<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{

    /**
     * @Route("/pdf", name="pdf")
     */
    public function pdfAction(Request $request)
    {
        $snappy = $this->get('knp_snappy.pdf');

        $html = $this->renderView('front/pdf.html.twig', array(
            'title' => 'Hello World !'
        ));

        $filename = 'myFirstSnappyPDF';

        return new Response(
            $snappy->getOutputFromHtml($html),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'inline; filename="'.$filename.'.pdf"'
            )
        );

//        return $this->render('front/index.html.twig' );
    }

    /**
     * @Route("/pdf2", name="pdf2")
     */
    public function pdf2Action(Request $request)
    {
        $snappy = $this->get('knp_snappy.pdf');

        $html = $this->renderView('front/OutDocument.html.twig');

        $filename = 'myFirstSnappyPDF';

        return new Response(
            $snappy->getOutputFromHtml($html),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'inline; filename="'.$filename.'.pdf"'
            )
        );

//        return $this->render('front/index.html.twig' );
    }



    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {

//        dump($this->getUser()->getRoles()[0]);
        if($this->getUser()==null){
            return $this->redirectToRoute('fos_user_security_login');
        }
        elseif ($this->getUser()->getRoles()[0]=="DOCTEUR"){
            return $this->redirectToRoute('afficher' );
        }
        else
            return $this->redirectToRoute('afficherVisiteAgee' );
    }

    /**
     * @Route("/back", name="back")
     */
    public function backAction(Request $request)
    {
        return $this->render('back/base.html.twig');
    }




    /**
     * @Route("/about", name="about")
     */
    public function aboutAction(Request $request)
    {
        return $this->render('front/about.html.twig');
    }


    /**
     * @Route("/send-notification", name="send_notification")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function sendNotification(Request $request)
    {
        $manager = $this->get('mgilet.notification');
        $notif = $manager->createNotification('Notification de rendez-vous');
        $notif->setMessage('Vous avez un rendez-vous médical le 25/04/2019');

        //$notif->setLink('http://symfony.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject','Some random text','http://google.fr');

        // you can add a notification to a list of entities
        // the third parameter ``$flush`` allows you to directly flush the entities
        $manager->addNotification(array($this->getUser()), $notif, true);

        return $this->redirectToRoute('homepage');
    }

}
