<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use ArrayAccess;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Mgilet\NotificationBundle\NotifiableInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Mgilet\NotificationBundle\Annotation\Notifiable;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 * @Notifiable(name="user")
 */


class User extends BaseUser implements ArrayAccess , NotifiableInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\OneToOne(targetEntity="GestionAgeeBundle\Entity\Planning_visite_medicale")
     * @ORM\JoinColumn(name="id", referencedColumnName="id_Agee")
     */
    protected $id;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }
    /**
     * @ORM\Column(type="string",length=255)
     * @Assert\NotBlank(message="le nom est obligatoire")
     */
    private $nom;
    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return User
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @ORM\Column(type="string",length=255)
     *
     */
    private $prenom;
    /**
     * Set prenom.
     *
     * @param string $prenom
     *
     * @return User
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom.
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="obligatoire")
     */
    private $cin;
    /**
     * Set cin.
     *
     * @param string $cin
     *
     * @return User
     */
    public function setCin($cin)
    {
        $this->cin = $cin;

        return $this;
    }

    /**
     * Get cin.
     *
     * @return integer
     */
    public function getCin()
    {
        return $this->cin;
    }

    /**
     * @ORM\Column(type="date")
     * @Assert\NotBlank(message="le nom est obligatoire")
     */
    private $datenaissance;
    /**
     * Set datenaissance.
     *
     * @param string $datenaissance
     *
     * @return User
     */
    public function setdatenaissance($datenaissance)
    {
        $this->datenaissance = $datenaissance;

        return $this;
    }

    /**
     * Get datenaissance.
     *
     * @return string
     */
    public function getDatenaissance()
    {
        return $this->datenaissance;
    }
    /**
     * @ORM\Column(type="string",length=255)
     * @Assert\NotBlank(message="le sexe est obligatoire")
     */
    private $sexe;
    /**
     * Set sexe.
     *
     * @param string $sexe
     *
     * @return User
     */
    public function setSexe($sexe)
    {
        $this->sexe = $sexe;

        return $this;
    }

    /**
     * Get sexe.
     * @Assert\Choice(
     *     choices = { "female", "male" },
     *     message = "Choose a valid genre."
     * )
     * @return string
     */
    public function getSexe()
    {
        return $this->sexe;
    }
    /**
     * @ORM\Column(type="string",length=255)
     * @Assert\NotBlank(message="la region obligatoire")
     */
    private $region;
    /**
     * Set region.
     *
     * @param string $region
     *
     * @return User
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region.
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }
    /**
     * @ORM\Column(type="string",length=255)
     * @Assert\NotBlank(message="l'adresse obligatoire")
     */
    private $adresse;
    /**
     * Set adresse.
     *
     * @param string $adresse
     *
     * @return User
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse.
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="telephone obligatoire")
     */
    private $telephone;
    /**
     * Set telephone.
     *
     * @param string $telephone
     *
     * @return User
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone.
     *
     * @return integer
     */
    public function getTelephone()
    {
        return $this->telephone;
    }


    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message=" rib obligatoire")
     */
    private $rib;
    /**
     * Set rib.
     *
     * @param string $rib
     *
     * @return User
     */
    public function setRib($rib)
    {
        $this->rib = $rib;

        return $this;
    }

    /**
     * Get rib.
     *
     * @return integer
     */
    public function getRib()
    {
        return $this->rib;
    }

    /**
     * @ORM\Column(type="integer" , nullable=true)
     *
     */
    private $bugetdon;
    /**
     * Set bugetdon.
     *
     * @param string $bugetdon
     *
     * @return User
     */
    public function setBugetdon($bugetdon)
    {
        $this->bugetdon = $bugetdon;

        return $this;
    }

    /**
     * Get bugetdon.
     *
     * @return integer
     */
    public function getBugetdon()
    {
        return $this->bugetdon;
    }
    /**
     * @ORM\Column(type="string",length=255, nullable=true)
     *
     */
    private  $photo;
    /**
     * Set photo.
     *
     * @param string $photo
     *
     * @return User
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo.
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }
    /**
     * @ORM\Column(type="float",nullable=true)
     *
     */
    private $salaire;
    /**
     * Set salaire.
     *
     * @param string $bugetdon
     *
     * @return User
     */
    public function setSalaire($salaire)
    {
        $this->salaire = $salaire;

        return $this;
    }

    /**
     * Get salaire.
     *
     * @return float
     */
    public function getSalaire()
    {
        return $this->salaire;
    }

    /**
     * Whether a offset exists
     * @link https://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     * @since 5.0.0
     */
    public function offsetExists($offset)
    {
        // TODO: Implement offsetExists() method.
    }

    /**
     * Offset to retrieve
     * @link https://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     * @since 5.0.0
     */
    public function offsetGet($offset)
    {
        // TODO: Implement offsetGet() method.
    }

    /**
     * Offset to set
     * @link https://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetSet($offset, $value)
    {
        // TODO: Implement offsetSet() method.
    }

    /**
     * Offset to unset
     * @link https://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetUnset($offset)
    {
        // TODO: Implement offsetUnset() method.
    }





}
