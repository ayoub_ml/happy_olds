<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, array('label' => 'Nom : ', 'attr' => array('class' => 'form-control','style' => 'margin-bottom: 5px;')))
            ->add('prenom', TextType::class, array('label' => 'Prénom : ', 'attr' => array('class' => 'form-control','style' => 'margin-bottom: 5px;')))
            ->add('cin', TextType::class, array('label' => 'CIN : ', 'attr' => array('class' => 'form-control','style' => 'margin-bottom: 5px;')))
            ->add('datenaissance', DateType::class, array('years' => range(1900, 2010), 'label' => 'Date de naissance : '))
            ->add('sexe', ChoiceType::class, array('label' => 'Sexe : ', 'choices' => [
                'Male' => 'male',
                'Female' => 'female',
            ], 'attr' => array('class' => 'form-control','style' => 'margin-bottom: 5px;')))
            ->add('region', TextType::class, array('label' => 'Région : ', 'attr' => array('class' => 'form-control','style' => 'margin-bottom: 5px;')))
            ->add('adresse', TextType::class, array('label' => 'Adresse : ', 'attr' => array('class' => 'form-control','style' => 'margin-bottom: 5px;')))
            ->add('telephone', TextType::class, array('label' => 'Téléphone : ', 'attr' => array('class' => 'form-control','style' => 'margin-bottom: 5px;')))
            ->add('rib', TextType::class, array('label' => 'Rib : ', 'attr' => array('class' => 'form-control','style' => 'margin-bottom: 5px;')))
            ->add('email', EmailType::class, array('label' => 'Email :', 'attr' => array('class' => 'formEmail fom-control', 'style' => 'margin-bottom: 5px;')))
            ->add('username', null, array('label' => 'Username :', 'translation_domain' => 'FOSUserBundle', 'attr' => array('class' => 'form-control','style' => 'margin-bottom: 5px;')))
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'options' => array(
                    'translation_domain' => 'FOSUserBundle',
                    'attr' => array(
                        'autocomplete' => 'new-password',
                        'class' => 'form-control','style' => 'margin-bottom: 5px;'
                    ),
                ),
                'first_options' => array('label' => 'Password :', 'attr' => array('class' => 'form-control', 'style' => 'margin-bottom: 5px;')),
                'second_options' => array('label' => 'Confirm password :', 'attr' => array('class' => 'form-control', 'style' => 'margin-bottom: 5px;')),
                'invalid_message' => 'fos_user.password.mismatch',
            ))
           ->add('roles', ChoiceType::class, array('label' => 'Type : ', 'choices' => array(
                'AGEE' => 'ROLE_AGEE',
                'BENEVOLE' => 'ROLE_BENEVOLE',
                'OLDSITTER' => 'ROLE_OLDSITTER',
                'DOCTEUR' => 'DOCTEUR',
            ), 'required' => true, 'multiple' => true, 'attr' => array('class' => 'userRole form-control')));
    }


    public function getBlockPrefix()
    {
        return 'user_registration';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User',
            'csrf_token_id' => 'registration',
        ));
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }
}