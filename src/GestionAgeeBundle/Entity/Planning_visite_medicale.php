<?php

namespace GestionAgeeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\User;

/**
 * Planning_visite_medicale
 *
 * @ORM\Table(name="planning_visite_medicale")
 * @ORM\Entity(repositoryClass="GestionAgeeBundle\Repository\Planning_visite_medicaleRepository")
 */
class Planning_visite_medicale
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_vm", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */

    private $id;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="id_Agee", referencedColumnName="id")
     */
    private $idAgee;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Date_Rdv", type="datetime")
     */
    private $dateRdv;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }







    /**
     * Set dateRdv
     *
     * @param \DateTime $dateRdv
     *
     * @return Planning_visite_medicale
     */
    public function setDateRdv($dateRdv)
    {

        $this->dateRdv = $dateRdv;

        return $this;
    }

    /**
     * Get dateRdv
     *
     * @return \DateTime
     */
    public function getDateRdv()
    {
        return $this->dateRdv;
    }

    /**
     * Set idAgee
     *
     * @param \AppBundle\Entity\User $idAgee
     *
     * @return Planning_visite_medicale
     */
    public function setIdAgee(\AppBundle\Entity\User $idAgee = null)
    {
        $this->idAgee = $idAgee;

        return $this;
    }

    /**
     * Get idAgee
     *
     * @return \AppBundle\Entity\User
     */
    public function getIdAgee()
    {
        return $this->idAgee;
    }
}
