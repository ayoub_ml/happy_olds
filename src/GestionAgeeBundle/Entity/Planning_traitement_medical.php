<?php

namespace GestionAgeeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Planning_traitement_medical
 *
 * @ORM\Table(name="planning_traitement_medical")
 * @ORM\Entity(repositoryClass="GestionAgeeBundle\Repository\Planning_traitement_medicalRepository")
 */
class Planning_traitement_medical
{

    const STATUS_AVANT = 'avant';
    const STATUS_APRES = 'apres';
    const STATUS_NON = 'non';

    /**
     * @var int
     *
     * @ORM\Column(name="id_ptm", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="id_Agee", referencedColumnName="id")
     *
     */
    private $idAgee;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_medicament", type="string", length=255)
     */
    private $nomMedicament;

    /**
     * @var string
     *
     * @ORM\Column(name="traitement_desc", type="string", length=255)
     */
    private $traitementDesc;

    /**
     * @var string
     *
     * @ORM\Column(name="duree_en_jour_de_traitement", type="decimal", precision=10, scale=0)
     */
    private $dureeEnJourDeTraitement;

    /**
     * @var string
     *
     * @ORM\Column(name="dejeuner", type="string", length=255, columnDefinition="ENUM('avant', 'apres' ,'non')")
     */
    private $dejeuner;

    /**
     * @var string
     *
     * @ORM\Column(name="petitDejeuner", type="string", length=255, columnDefinition="ENUM('avant', 'apres' ,'non')")
     */
    private $petitDejeuner;

    /**
     * @var string
     *
     * @ORM\Column(name="diner", type="string", length=255, columnDefinition="ENUM('avant', 'apres' ,'non')")
     */
    private $diner;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idAgee
     *
     * @param string $idAgee
     *
     * @return Planning_traitement_medical
     */
    public function setIdAgee($idAgee)
    {
        $this->idAgee = $idAgee;

        return $this;
    }

    /**
     * Get idAgee
     *
     * @return string
     */
    public function getIdAgee()
    {
        return $this->idAgee;
    }

    /**
     * Set nomMedicament
     *
     * @param string $nomMedicament
     *
     * @return Planning_traitement_medical
     */
    public function setNomMedicament($nomMedicament)
    {
        $this->nomMedicament = $nomMedicament;

        return $this;
    }

    /**
     * Get nomMedicament
     *
     * @return string
     */
    public function getNomMedicament()
    {
        return $this->nomMedicament;
    }

    /**
     * Set traitementDesc
     *
     * @param string $traitementDesc
     *
     * @return Planning_traitement_medical
     */
    public function setTraitementDesc($traitementDesc)
    {
        $this->traitementDesc = $traitementDesc;

        return $this;
    }

    /**
     * Get traitementDesc
     *
     * @return string
     */
    public function getTraitementDesc()
    {
        return $this->traitementDesc;
    }



    /**
     * Set dejeuner
     *
     * @param string $dejeuner
     *
     * @return Planning_traitement_medical
     */
    public function setDejeuner($dejeuner)
    {
        if (!in_array($dejeuner, array(self::STATUS_AVANT, self::STATUS_APRES, self::STATUS_NON))) {
            throw new \InvalidArgumentException("Invalid status enum avant apres non");
        }
        $this->dejeuner = $dejeuner;

        return $this;
    }

    /**
     * Get dejeuner
     *
     * @return string
     */
    public function getDejeuner()
    {
        return $this->dejeuner;
    }

    /**
     * Set petitDejeuner
     *
     * @param string $petitDejeuner
     *
     * @return Planning_traitement_medical
     */
    public function setPetitDejeuner($petitDejeuner)
    {
        if (!in_array($petitDejeuner, array(self::STATUS_AVANT, self::STATUS_APRES, self::STATUS_NON))) {
            throw new \InvalidArgumentException("Invalid status enum avant apres non");
        }
        $this->petitDejeuner = $petitDejeuner;

        return $this;
    }

    /**
     * Get petitDejeuner
     *
     * @return string
     */
    public function getPetitDejeuner()
    {
        return $this->petitDejeuner;
    }

    /**
     * Set diner
     *
     * @param string $diner
     *
     * @return Planning_traitement_medical
     */
    public function setDiner($diner)
    {
        if (!in_array($diner, array(self::STATUS_AVANT, self::STATUS_APRES, self::STATUS_NON))) {
            throw new \InvalidArgumentException("Invalid status enum avant apres non");
        }
        $this->diner = $diner;

        return $this;
    }

    /**
     * Get diner
     *
     * @return string
     */
    public function getDiner()
    {
        return $this->diner;
    }

    /**
     * Set dureeEnJourDeTraitement
     *
     * @param string $dureeEnJourDeTraitement
     *
     * @return Planning_traitement_medical
     */
    public function setDureeEnJourDeTraitement($dureeEnJourDeTraitement)
    {
        $this->dureeEnJourDeTraitement = $dureeEnJourDeTraitement;

        return $this;
    }

    /**
     * Get dureeEnJourDeTraitement
     *
     * @return string
     */
    public function getDureeEnJourDeTraitement()
    {
        return $this->dureeEnJourDeTraitement;
    }
}
