<?php

namespace GestionAgeeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MapLocation
 *
 * @ORM\Table(name="map_location")
 * @ORM\Entity(repositoryClass="GestionAgeeBundle\Repository\MapLocationRepository")
 */
class MapLocation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="id_Agee", referencedColumnName="id")
     *
     */
    private $idAgee;

    /**
     * @var int
     *
     * @ORM\Column(name="x", type="integer")
     */
    private $x;

    /**
     * @var int
     *
     * @ORM\Column(name="y", type="integer")
     */
    private $y;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * Set idAgee
     *
     * @param \AppBundle\Entity\User $idAgee
     *
     * @return MapLocation
     */
    public function setIdAgee(\AppBundle\Entity\User $idAgee = null)
    {
        $this->idAgee = $idAgee;

        return $this;
    }

    /**
     * Get idAgee
     *
     * @return \AppBundle\Entity\User
     */
    public function getIdAgee()
    {
        return $this->idAgee;
    }

    /**
     * Set x
     *
     * @param integer $x
     *
     * @return MapLocation
     */
    public function setX($x)
    {
        $this->x = $x;

        return $this;
    }

    /**
     * Get x
     *
     * @return integer
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * Set y
     *
     * @param integer $y
     *
     * @return MapLocation
     */
    public function setY($y)
    {
        $this->y = $y;

        return $this;
    }

    /**
     * Get y
     *
     * @return integer
     */
    public function getY()
    {
        return $this->y;
    }
}
