<?php

namespace GestionAgeeBundle\Entity;

use AppBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * Agee
 *
 * @ORM\Table(name="agee")
 * @ORM\Entity(repositoryClass="GestionAgeeBundle\Repository\AgeeRepository")
 */
class Agee extends User
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}

