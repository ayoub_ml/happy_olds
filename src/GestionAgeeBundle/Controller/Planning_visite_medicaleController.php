<?php

namespace GestionAgeeBundle\Controller;

use AppBundle\Entity\User;
use GestionAgeeBundle\Form\Planning_visite_medicaleType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use GestionAgeeBundle\Entity\Planning_visite_medicale;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class Planning_visite_medicaleController extends Controller
{
//    public function AjouterAction()
//    {
//        return $this->render('GestionAgeeBundle:Planning_visite_medicale:ajouter.html.twig', array(
//            // ...
//        ));
//    }


    /**
     * @Route("/pdf3/{id}", name="pdf3")
     */
    public function pdf3Action(Request $request, $id)
    {
        $snappy = $this->get('knp_snappy.pdf');

        $nomDocteur = $this->getUser()->getNom();
        $prenomDocteur = $this->getUser()->getPrenom();
        $em=$this->getDoctrine()->getManager();
        $planning = $em->getRepository(Planning_visite_medicale::class)->find($id);
        $dateRdv = $planning->getDateRdv()->format("d-m-Y");
        $agee = $planning->getIdAgee();
        $nomAgee = $agee->getNom();
        $prenomAgee = $agee->getPrenom();
        $naissanceAgee = $agee->getDatenaissance()->format("d-m-Y");
        $prenomAgee = $agee->getPrenom();
        $prenomAgee = $agee->getPrenom();
        $html = $this->renderView('@GestionAgee/Planning_visite_medicale/pdf3.html.twig', array(
            'nomDocteur' => $nomDocteur ,
            'prenomDocteur' => $prenomDocteur ,
            'nomAgee' => $nomAgee ,
            'prenomAgee' => $prenomAgee,
            'naissanceAgee' => $naissanceAgee,
            'dateRdv' => $dateRdv,

        ));
        $filename = 'myFirstSnappyPDF';

        return new Response(
            $snappy->getOutputFromHtml($html),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'inline; filename="'.$filename.'.pdf"'
            )
        );

//        return $this->render('front/index.html.twig' );
    }


    /**
     * @Route("/ajouter", name="ajouter")
     */
    public function AjouterAction(Request $request)
    {
        //create an object to store our data after the form submission
        $planning_visite_medicale=new Planning_visite_medicale();
        //prepare the form with the function: createForm()
        $form=$this->createForm(Planning_visite_medicaleType::class,$planning_visite_medicale);
        //extract the form answer from the received request
        $form=$form->handleRequest($request);
        //if this form is valid
        if($form->isValid()){
            $user = $planning_visite_medicale->getIdAgee();
            $date = $planning_visite_medicale->getDateRdv()->format("d-m-Y");
            $time = $planning_visite_medicale->getDateRdv()->format("H-i");

            $manager = $this->get('mgilet.notification');
            $notif = $manager->createNotification('--rendez-vous-- :');
            $notif->setMessage('Vous avez un rendez-vous médical le '.$date.' à '.$time.' soyez à l\'heure s\'il vous plait. Merci ! ');

            $manager->addNotification(array($user), $notif, true);

            //create an entity manager object
            $em=$this->getDoctrine()->getManager();
            //persist the object $modele in the ORM
            $em->persist($planning_visite_medicale);
            //update the data base with flush
            $em->flush();
            //redirect the route after the add
            return $this->redirectToRoute('afficher');
        }
        dump($form);

        return $this->render('@GestionAgee/Planning_visite_medicale/ajouter.html.twig',
            array(
                'form'=>$form->createView()
            ));
    }

//    public function pdfAction(Request $request, $id)
//    {   if ($this->getUser() == Null) {
//        return $this->render('@Evenement/Default/index.html.twig');
//    } else {
////        $em = $this->getDoctrine()->getManager();
//////        $articles = $em->getRepository("ArticleDonBundle:Article")->find($id);
////        $user = $em->getRepository("ArticleDonBundle:Article")->find($id);
//
//        $text = "je se"
//        $plannings = $this->getDoctrine()->getRepository(Planning_visite_medicale::class)->find($id);
//
//        $html = $this->renderView('@ArticleDon/Article/pdf.html.twig', array(
//            'text'  => $articles,
//
//            'titrearticle'=>$titrearticle,
//            'themearticle'=>$themearticle,
//            'Descriptionarticle'=>$Descriptionarticle,
//            'datearticle'=>$datearticle,
//            'devisname'=>$devisname,
//
//        ));
//
//        $titrearticle=$articles->getTitreArticle();
//        $themearticle=$articles->getThemeArticle();
//        $Descriptionarticle=$articles->getDescriptionArticle();
//        $datearticle=$articles->getDatearticle();
//        $devisname=$articles->getDevisName();
//
//        $em->flush();
//        $html = $this->renderView('@ArticleDon/Article/pdf.html.twig', array(
//            'articles'  => $articles,
//
//            'titrearticle'=>$titrearticle,
//            'themearticle'=>$themearticle,
//            'Descriptionarticle'=>$Descriptionarticle,
//            'datearticle'=>$datearticle,
//            'devisname'=>$devisname,
//
//        ));
//
//        return new PdfResponse(
//            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
//            'file.pdf'
//        );
//    }}

    /**
     * @Route("/modifier/{id}", name="modifier")
     */

    public function ModifierAction(Request $request, $id)
    {

        $em=$this->getDoctrine()->getManager();
        $planning_visite_medicale = $em->getRepository(Planning_visite_medicale::class)->find($id);
        $planning_visite_medicale->setIdAgee($planning_visite_medicale->getIdAgee());
        $planning_visite_medicale->setDateRdv($planning_visite_medicale->getDateRdv());
        $form=$this->createForm(Planning_visite_medicaleType::class,$planning_visite_medicale);

        $form->handleRequest($request);

        if($form->isValid()){

            $user = $planning_visite_medicale->getIdAgee();
            $date = $planning_visite_medicale->getDateRdv()->format("d-m-Y");
            $time = $planning_visite_medicale->getDateRdv()->format("H-i");

            $manager = $this->get('mgilet.notification');
            $notif = $manager->createNotification('--Mise à Jour RDV-- :');
            $notif->setMessage('Votre RDV a été reporté le '.$date.' à '.$time.' soyez à l\'heure s\'il vous plait. Merci ! ');

            $manager->addNotification(array($user), $notif, true);


            $agee=$form['idAgee']->getData();
            $dateRdv=$form['dateRdv']->getData();
            $planning_visite_medicale->setIdAgee($agee);
            $planning_visite_medicale->setDateRdv($dateRdv);

            $em->flush();

            return $this->redirectToRoute('afficher');
        }


        return $this->render('@GestionAgee/Planning_visite_medicale/modifier.html.twig', array(
            'planning_visite_medicale' => $planning_visite_medicale,
            'form' => $form->createView()
            // ...
        ));
    }


    /**
     * @Route("/supprimer/{id}", name="supprimer")
     */


    public function SupprimerAction($id)
    {
        //get the object to be removed given the submitted id
        $em = $this->getDoctrine()->getManager();
        $planning_visite_medicale= $em->getRepository(Planning_visite_medicale::class)->find($id);
        //remove from the ORM
        $em->remove($planning_visite_medicale);
        //update the data base
        $em->flush();
        return $this->redirectToRoute("afficher");
    }





    /**
     * @Route("/afficher", name="afficher")
     */


    public function AfficherAction()
    {
        $plannings = $this->getDoctrine()->getRepository(Planning_visite_medicale::class)->findAll();



        return $this->render('@GestionAgee/Planning_visite_medicale/afficher.html.twig', array("plannings" => $plannings
            // ...
        ));
    }

    /**
     * @Route("/afficherVisiteAgee", name="afficherVisiteAgee")
     */


    public function AfficherAgeeAction()
    {
        $userId = $this->getUser()->getId();
        $plannings = $this->getDoctrine()->getRepository(Planning_visite_medicale::class)->findBy(array('idAgee' => $userId));



        return $this->render('@GestionAgee/Agee/afficherVisiteAgee.html.twig', array("plannings" => $plannings
            // ...
        ));
    }

}
