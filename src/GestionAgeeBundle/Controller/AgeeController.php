<?php

namespace GestionAgeeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class AgeeController extends Controller
{

    /**
     * @Route("/afficher_agee", name="afficher_agee")
     */


    public function AfficherAction()
    {
        $plannings = $this->getDoctrine()->getRepository(Planning_visite_medicale::class)->findAll();



        return $this->render('@GestionAgee/Planning_visite_medicale/afficher_agee.html.twig', array("plannings" => $plannings
            // ...
        ));
    }




}
