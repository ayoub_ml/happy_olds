<?php

namespace GestionAgeeBundle\Controller;

use GestionAgeeBundle\Entity\Planning_traitement_medical;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


class MobileTraitementMedicalController extends Controller
{

    /**
     * @Route("/mobile/traitement/afficher", name="mobileTraitementAfficher")
     */
    public function allAction()
    {
        $tasks = $this->getDoctrine()->getManager()
            ->getRepository('GestionAgeeBundle:Planning_traitement_medical')
            ->findAll();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($tasks);
        return new JsonResponse($formatted);
    }


    /**
     * @Route("/mobile/traitement/recherche/{id}", name="mobileTraitementRecherche")
     */
    public function findAction($id)
    {
        $tasks = $this->getDoctrine()->getManager()
            ->getRepository('GestionAgeeBundle:Planning_traitement_medical')
            ->find($id);
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($tasks);
        return new JsonResponse($formatted);
    }

    /**
     * @Route("/mobile/traitement/recherche/idAgee/{id}", name="mobileTraitementRecherche")
     */
    public function findAgeeTraitementAction($id)
    {
        $tasks = $this->getDoctrine()->getManager()
            ->getRepository('GestionAgeeBundle:Planning_traitement_medical')
            ->findBy(['idAgee' => $id]);
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($tasks);
        return new JsonResponse($formatted);
    }


    /**
     * @Route("/mobile/Agee/afficher" , name="mobileAgeeAfficher")
     */

    public function AfficherAgee(){
        $agee = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:User')
            ->findAll();

        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($agee);
        return new JsonResponse($formatted);
    }






    /**
     * @Route("/mobile/traitement/ajouter", name="mobileTraitementAjouter")
     */
//http://localhost/phpmyadmin/sql.php?server=1&db=happy_olds&table=planning_traitement_medical&pos=0&token=1cd8b6ff32bd511dbdc37fe84623031a
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $planning_traitement_medical=new Planning_traitement_medical();
        $id = $this->getDoctrine()->getManager()->getRepository('AppBundle:User')->find($request->get('id'));
        $planning_traitement_medical->setIdAgee($id);
        $planning_traitement_medical->setNomMedicament($request->get('nomMedicament'));
        $planning_traitement_medical->setTraitementDesc($request->get('traitementDesc'));
        $planning_traitement_medical->setDureeEnJourDeTraitement($request->get('duree'));
        $planning_traitement_medical->setDejeuner($request->get('dejeuner'));
        $planning_traitement_medical->setPetitDejeuner($request->get('petitDejeuner'));
        $planning_traitement_medical->setDiner($request->get('diner'));
        $em->persist($planning_traitement_medical);
        $em->flush();

        //----------------notification-------------------------------
        $user = $planning_traitement_medical->getIdAgee();
        $medicament = $planning_traitement_medical->getNomMedicament();
        $nbJour = $planning_traitement_medical->getDureeEnJourDeTraitement();

        $manager = $this->get('mgilet.notification');
        $notif = $manager->createNotification('--Nouveau Traitement-- :');
        $notif->setMessage('Vous avez un nouveau Traitement médicamenteux à suivre dans une durée de '.$nbJour.' jours. --médicament: '.$medicament.'--');

        $manager->addNotification(array($user), $notif, true);
        //----------fin notif-------------------------------

        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($planning_traitement_medical);
        return new JsonResponse($formatted);
    }


    /**
     * @Route("/mobile/traitement/update", name="mobileTraitementModif")
     */
//http://localhost/phpmyadmin/sql.php?server=1&db=happy_olds&table=planning_traitement_medical&pos=0&token=1cd8b6ff32bd511dbdc37fe84623031a

    public function modifierTraitementAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $Planning_traitement_medical = $em->getRepository(Planning_traitement_medical::class)->find($request->get('idTraitement'));
        $agee = $this->getDoctrine()->getManager()->getRepository('AppBundle:User')->find($request->get('idAgee'));
        $Planning_traitement_medical->setIdAgee($agee);

//------------------notif_____________________

            $user = $Planning_traitement_medical->getIdAgee();
            $medicament = $Planning_traitement_medical->getNomMedicament();
            $nbJour = $Planning_traitement_medical->getDureeEnJourDeTraitement();

            $manager = $this->get('mgilet.notification');
            $notif = $manager->createNotification('--Mise à jour Traitement-- :');
            $notif->setMessage('Vous avez une mise à jour du Traitement médicamenteux à suivre dans une durée de '.$nbJour.' jours. --médicament: '.$medicament.'--');
            $manager->addNotification(array($user), $notif, true);

//------------------fin notif_____________________


            $nomMedicament = $request->get('nomMedicament');
            $traitementDesc = $request->get('traitementDesc');
            $dureeEnJourDuTraitement = $request->get('duree');
            $dejeuner = $request->get('dejeuner');
            $petitDejeuner = $request->get('petitDejeuner');
            $diner = $request->get('diner');


            $Planning_traitement_medical->setIdAgee($agee);
            $Planning_traitement_medical->setNomMedicament($nomMedicament);
            $Planning_traitement_medical->setTraitementDesc($traitementDesc);
            $Planning_traitement_medical->setDureeEnJourDeTraitement($dureeEnJourDuTraitement);
            $Planning_traitement_medical->setDejeuner($dejeuner);
            $Planning_traitement_medical->setPetitDejeuner($petitDejeuner);
            $Planning_traitement_medical->setDiner($diner);

            $em->flush();

            $serializer = new Serializer([new ObjectNormalizer()]);
            $formatted = $serializer->normalize($Planning_traitement_medical);
            return new JsonResponse($formatted);
        }


    public function ajouterTraitementAction(Request $request)
    {
        //create an object to store our data after the form submission
        $planning_traitement_medical=new Planning_traitement_medical();
        //prepare the form with the function: createForm()
        $form=$this->createForm(Planning_traitement_medicalType::class,$planning_traitement_medical);
        //extract the form answer from the received request
        $form=$form->handleRequest($request);
        //if this form is valid
        if($form->isValid()){
            $user = $planning_traitement_medical->getIdAgee();
            $medicament = $planning_traitement_medical->getNomMedicament();
            $nbJour = $planning_traitement_medical->getDureeEnJourDeTraitement();

            $manager = $this->get('mgilet.notification');
            $notif = $manager->createNotification('--Nouveau Traitement-- :');
            $notif->setMessage('Vous avez un nouveau Traitement médicamenteux à suivre dans une durée de '.$nbJour.' jours. --médicament: '.$medicament.'--');

            $manager->addNotification(array($user), $notif, true);

            //create an entity manager object
            $em=$this->getDoctrine()->getManager();
            //persist the object $modele in the ORM
            $em->persist($planning_traitement_medical);
            //update the data base with flush
            $em->flush();
            //redirect the route after the add
            return $this->redirectToRoute('afficherTraitement');
        }
        return $this->render('@GestionAgee/Planning_traitement_medical/ajouterTraitement.html.twig',
            array(
                'form'=>$form->createView()
            ));
    }


    /**
     * @Route("/mobile/supprimerTraitement", name="mobileSupprimerTraitement")
     */


    public function supprimerTraitementAction(Request $request)
    {
        //get the object to be removed given the submitted id
        $em = $this->getDoctrine()->getManager();
        $Planning_traitement_medical= $em->getRepository(Planning_traitement_medical::class)->find($request->get('idTraitement'));
        //remove from the ORM
        $em->remove($Planning_traitement_medical);
        //update the data base
        $em->flush();

        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($Planning_traitement_medical);
        return new JsonResponse($formatted);

    }








}
