<?php

namespace GestionAgeeBundle\Controller;

use GestionAgeeBundle\Entity\Planning_traitement_medical;
use GestionAgeeBundle\Form\Planning_traitement_medicalType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class Planning_traitement_medicalController extends Controller
{

    /**
     * @Route("/ajouterTraitement", name="ajouterTraitement")
     */
    public function ajouterTraitementAction(Request $request)
    {
        //create an object to store our data after the form submission
        $planning_traitement_medical=new Planning_traitement_medical();
        //prepare the form with the function: createForm()
        $form=$this->createForm(Planning_traitement_medicalType::class,$planning_traitement_medical);
        //extract the form answer from the received request
        $form=$form->handleRequest($request);
        //if this form is valid
        if($form->isValid()){
            $user = $planning_traitement_medical->getIdAgee();
            $medicament = $planning_traitement_medical->getNomMedicament();
            $nbJour = $planning_traitement_medical->getDureeEnJourDeTraitement();

            $manager = $this->get('mgilet.notification');
            $notif = $manager->createNotification('--Nouveau Traitement-- :');
            $notif->setMessage('Vous avez un nouveau Traitement médicamenteux à suivre dans une durée de '.$nbJour.' jours. --médicament: '.$medicament.'--');

            $manager->addNotification(array($user), $notif, true);

            //create an entity manager object
            $em=$this->getDoctrine()->getManager();
            //persist the object $modele in the ORM
            $em->persist($planning_traitement_medical);
            //update the data base with flush
            $em->flush();
            //redirect the route after the add
            return $this->redirectToRoute('afficherTraitement');
        }
        return $this->render('@GestionAgee/Planning_traitement_medical/ajouterTraitement.html.twig',
            array(
                'form'=>$form->createView()
            ));
    }

    /**
     * @Route("/modifierTraitement/{id}", name="modifierTraitement")
     */

    public function modifierTraitementAction(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();
        $Planning_traitement_medical = $em->getRepository(Planning_traitement_medical::class)->find($id);
        $Planning_traitement_medical->setIdAgee($Planning_traitement_medical->getIdAgee());
        $form = $this->createForm(Planning_traitement_medicalType::class, $Planning_traitement_medical);

        $form->handleRequest($request);

        if ($form->isValid()) {

            $user = $Planning_traitement_medical->getIdAgee();
            $medicament = $Planning_traitement_medical->getNomMedicament();
            $nbJour = $Planning_traitement_medical->getDureeEnJourDeTraitement();

            $manager = $this->get('mgilet.notification');
            $notif = $manager->createNotification('--Mise à jour Traitement-- :');
            $notif->setMessage('Vous avez une mise à jour du Traitement médicamenteux à suivre dans une durée de '.$nbJour.' jours. --médicament: '.$medicament.'--');
            $manager->addNotification(array($user), $notif, true);


            $agee = $form['idAgee']->getData();
            $nomMedicament = $form['nomMedicament']->getData();
            $traitementDesc = $form['traitementDesc']->getData();
            $dureeEnJourDuTraitement = $form['dureeEnJourDeTraitement']->getData();
            $dejeuner = $form['dejeuner']->getData();
            $petitDejeuner = $form['petitDejeuner']->getData();
            $diner = $form['diner']->getData();

            $Planning_traitement_medical->setIdAgee($agee);
            $Planning_traitement_medical->setNomMedicament($nomMedicament);
            $Planning_traitement_medical->setTraitementDesc($traitementDesc);
            $Planning_traitement_medical->setDureeEnJourDeTraitement($dureeEnJourDuTraitement);
            $Planning_traitement_medical->setDejeuner($dejeuner);
            $Planning_traitement_medical->setPetitDejeuner($petitDejeuner);
            $Planning_traitement_medical->setDiner($diner);

            $em->flush();

            return $this->redirectToRoute('afficherTraitement');
        }


        return $this->render('@GestionAgee/Planning_traitement_medical/modifierTraitement.html.twig', array(
            'planning_traitement_medical' => $Planning_traitement_medical,
            'form' => $form->createView()
            // ...
        ));
    }


    /**
     * @Route("/supprimerTraitement/{id}", name="supprimerTraitement")
     */


    public function supprimerTraitementAction($id)
    {
        //get the object to be removed given the submitted id
        $em = $this->getDoctrine()->getManager();
        $Planning_traitement_medical= $em->getRepository(Planning_traitement_medical::class)->find($id);
        //remove from the ORM
        $em->remove($Planning_traitement_medical);
        //update the data base
        $em->flush();
        return $this->redirectToRoute("afficherTraitement");
    }





    /**
     * @Route("/afficherTraitement", name="afficherTraitement")
     */


    public function afficherTraitementAction()
    {
        $plannings = $this->getDoctrine()->getRepository(Planning_traitement_medical::class)->findAll();



        return $this->render('@GestionAgee/Planning_traitement_medical/afficherTraitement.html.twig', array("plannings" => $plannings
            // ...
        ));
    }

    /**
     * @Route("/afficherTraitementAgee", name="afficherTraitementAgee")
     */


    public function afficherTraitementAgeection()
    {
        $userId = $this->getUser()->getId();
        $plannings = $this->getDoctrine()->getRepository(Planning_traitement_medical::class)->findBy(array('idAgee' => $userId));
        return $this->render('@GestionAgee/Agee/afficherTraitementAgee.html.twig', array("plannings" => $plannings
            // ...
        ));
    }

}
