<?php

namespace GestionAgeeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Planning_traitement_medicalType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nomMedicament')
            ->add('traitementDesc',TextareaType::class)
            ->add('dureeEnJourDeTraitement')
            ->add('dejeuner', ChoiceType::class, array("choices" => [ "" => "" ,"avant" => "avant", "apres" => "apres", "non" => "non"]))
            ->add('petitDejeuner',ChoiceType::class,array("choices"=>["" => "", "avant"=>"avant","apres"=>"apres","non"=>"non"]))
            ->add('diner', ChoiceType::class, array("choices" => ["" => "" , "avant" => "avant", "apres" => "apres", "non" => "non"]))
            ->add('idAgee')
            ->add('save', SubmitType::class, ['label' => 'Create Post']);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GestionAgeeBundle\Entity\Planning_traitement_medical'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'gestionageebundle_planning_traitement_medical';
    }


}
