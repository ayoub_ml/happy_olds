<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @FOSUser/Security/login_content.html.twig */
class __TwigTemplate_37ddb6086ae64a5e33cd98a9ec4b7f61c2ab58eec27fe4fba9ace0cec6480428 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@FOSUser/Security/login_content.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@FOSUser/Security/login_content.html.twig"));

        // line 2
        echo "<br><br><br><br>


";
        // line 6
        echo "    ";
        // line 7
        echo "        ";
        // line 8
        echo "    ";
        // line 9
        echo "
    ";
        // line 11
        echo "    ";
        // line 12
        echo "           ";
        // line 13
        echo "
    ";
        // line 15
        echo "    ";
        // line 16
        echo "
    ";
        // line 18
        echo "    ";
        // line 19
        echo "
    ";
        // line 22
        echo "

<br><br><br>
<form action=\"";
        // line 25
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_check");
        echo "\" method=\"post\" id=\"loginForm\">
    ";
        // line 26
        if (($context["csrf_token"] ?? $this->getContext($context, "csrf_token"))) {
            // line 27
            echo "        <input type=\"hidden\" name=\"_csrf_token\" value=\"";
            echo twig_escape_filter($this->env, ($context["csrf_token"] ?? $this->getContext($context, "csrf_token")), "html", null, true);
            echo "\"/>
    ";
        }
        // line 29
        echo "<div class=\"art-bothside\">

    <div class=\"sap_tabs\">
        <div id=\"horizontalTab\">
            <ul class=\"resp-tabs-list\">
                <li class=\"resp-tab-item\"><span>Login</span></li>
            </ul>
            <div class=\"clearfix\"></div>
            ";
        // line 37
        if (($context["error"] ?? $this->getContext($context, "error"))) {
            // line 38
            echo "                <div class=\"alert alert-danger text-center\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageKey", []), $this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageData", []), "security"), "html", null, true);
            echo "</div>
            ";
        }
        // line 40
        echo "            <div class=\"resp-tabs-container\">
                <div class=\"tab-1 resp-tab-content\">
                    <div class=\"swm-right-w3ls\">

                        <div class=\"form-left-to-w3l\">
                            <i class=\"fa fa-user icon\"></i> ";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.username", [], "FOSUserBundle"), "html", null, true);
        echo "<br>
                            <input class=\"form-control\" type=\"text\" id=\"username\" name=\"_username\" value=\"";
        // line 46
        echo twig_escape_filter($this->env, ($context["last_username"] ?? $this->getContext($context, "last_username")), "html", null, true);
        echo "\"
                                   required=\"required\" autocomplete=\"username\"/><br>
                            <div id=\"block-erreur-username\" class=\"hide\" style=\"color: red; margin-bottom: 15px; margin-top: -15px;\"></div>
                            ";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.password", [], "FOSUserBundle"), "html", null, true);
        echo "<br>
                            <input class=\"form-control\" type=\"password\" id=\"password\" name=\"_password\" required=\"required\"
                                   autocomplete=\"current-password\"/><br>
                            <div id=\"block-erreur-password\" class=\"hide\" style=\"color: red; margin-bottom: 15px; margin-top: -15px;\"></div>
                            ";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.remember_me", [], "FOSUserBundle"), "html", null, true);
        echo "<br>
                            <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\"/><br>
                        </div>

                        <div class=\"btnn\">

                        </div>
                        <input type=\"button\" class=\"submitLogin\" id=\"_submit\" name=\"_submit\" value=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.submit", [], "FOSUserBundle"), "html", null, true);
        echo "\"/>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
</form>

<br><br><br>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  136 => 60,  126 => 53,  119 => 49,  113 => 46,  109 => 45,  102 => 40,  96 => 38,  94 => 37,  84 => 29,  78 => 27,  76 => 26,  72 => 25,  67 => 22,  64 => 19,  62 => 18,  59 => 16,  57 => 15,  54 => 13,  52 => 12,  50 => 11,  47 => 9,  45 => 8,  43 => 7,  41 => 6,  36 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% trans_default_domain 'FOSUserBundle' %}
<br><br><br><br>


{#<form action=\"{{ path(\"fos_user_security_check\") }}\" method=\"post\">#}
    {#{% if csrf_token %}#}
        {#<input type=\"hidden\" name=\"_csrf_token\" value=\"{{ csrf_token }}\"/>#}
    {#{% endif %}#}

    {#<label for=\"username\">{{ 'security.login.username'|trans }}</label>#}
    {#<input type=\"text\" id=\"username\" name=\"_username\" value=\"{{ last_username }}\" required=\"required\"#}
           {#autocomplete=\"username\"/>#}

    {#<label for=\"password\">{{ 'security.login.password'|trans }}</label>#}
    {#<input type=\"password\" id=\"password\" name=\"_password\" required=\"required\" autocomplete=\"current-password\"/>#}

    {#<input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\"/>#}
    {#<label for=\"remember_me\">{{ 'security.login.remember_me'|trans }}</label>#}

    {#<input type=\"submit\" id=\"_submit\" name=\"_submit\" value=\"{{ 'security.login.submit'|trans }}\"/>#}
{#</form>#}


<br><br><br>
<form action=\"{{ path(\"fos_user_security_check\") }}\" method=\"post\" id=\"loginForm\">
    {% if csrf_token %}
        <input type=\"hidden\" name=\"_csrf_token\" value=\"{{ csrf_token }}\"/>
    {% endif %}
<div class=\"art-bothside\">

    <div class=\"sap_tabs\">
        <div id=\"horizontalTab\">
            <ul class=\"resp-tabs-list\">
                <li class=\"resp-tab-item\"><span>Login</span></li>
            </ul>
            <div class=\"clearfix\"></div>
            {% if error %}
                <div class=\"alert alert-danger text-center\">{{ error.messageKey|trans(error.messageData, 'security') }}</div>
            {% endif %}
            <div class=\"resp-tabs-container\">
                <div class=\"tab-1 resp-tab-content\">
                    <div class=\"swm-right-w3ls\">

                        <div class=\"form-left-to-w3l\">
                            <i class=\"fa fa-user icon\"></i> {{ 'security.login.username'|trans }}<br>
                            <input class=\"form-control\" type=\"text\" id=\"username\" name=\"_username\" value=\"{{ last_username }}\"
                                   required=\"required\" autocomplete=\"username\"/><br>
                            <div id=\"block-erreur-username\" class=\"hide\" style=\"color: red; margin-bottom: 15px; margin-top: -15px;\"></div>
                            {{ 'security.login.password'|trans }}<br>
                            <input class=\"form-control\" type=\"password\" id=\"password\" name=\"_password\" required=\"required\"
                                   autocomplete=\"current-password\"/><br>
                            <div id=\"block-erreur-password\" class=\"hide\" style=\"color: red; margin-bottom: 15px; margin-top: -15px;\"></div>
                            {{ 'security.login.remember_me'|trans }}<br>
                            <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\"/><br>
                        </div>

                        <div class=\"btnn\">

                        </div>
                        <input type=\"button\" class=\"submitLogin\" id=\"_submit\" name=\"_submit\" value=\"{{ 'security.login.submit'|trans }}\"/>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
</form>

<br><br><br>
", "@FOSUser/Security/login_content.html.twig", "/var/www/html/3A11/happy_olds/app/Resources/FOSUserBundle/views/Security/login_content.html.twig");
    }
}
