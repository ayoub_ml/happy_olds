<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* front/_footer.html.twig */
class __TwigTemplate_5c604dbd77c9f298fb226ef51f572d8ef5ba348f5f905076b47efb2ccd3548af extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "front/_footer.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "front/_footer.html.twig"));

        // line 1
        echo "<div class=\"footer\" id=\"contact\">
\t\t<div class=\"footer_inner_info_wthree_agileits\">
\t\t\t<!--/tabs-->
\t\t\t<div class=\"responsive_tabs\">
\t\t\t\t<div id=\"horizontalTab\">
\t\t\t\t\t<ul class=\"resp-tabs-list\">
\t\t\t\t\t\t<li>  </li>
\t\t\t\t\t\t<li> </li>
\t\t\t\t\t\t<li> </li>
\t\t\t\t\t</ul>
\t\t\t\t\t<div class=\"resp-tabs-container\">
\t\t\t\t\t\t<!--/tab_one-->
\t\t\t\t\t\t<div class=\"tab1\">
\t\t\t\t\t\t\t<div class=\"tab-info\">

\t\t\t\t\t\t\t\t<div class=\"address\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-4 address-grid\">
\t\t\t\t\t\t\t\t\t\t<div class=\"address-left\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"dodecagon f1\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"dodecagon-in f1\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"dodecagon-bg f1\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fas fa-phone-volume\" aria-hidden=\"true\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"address-right\">
\t\t\t\t\t\t\t\t\t\t\t<h6>Numéro de téléphone</h6>
\t\t\t\t\t\t\t\t\t\t\t<p>+216 99 000 100,+216 71 000 100</p>

\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"> </div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-4 address-grid\">
\t\t\t\t\t\t\t\t\t\t<div class=\"address-left\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"dodecagon f1\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"dodecagon-in f1\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"dodecagon-bg f1\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"far fa-envelope\" aria-hidden=\"true\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"address-right\">
\t\t\t\t\t\t\t\t\t\t\t<h6>Adresse Email</h6>
\t\t\t\t\t\t\t\t\t\t\t<p>Email :
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"mailto:example@email.com\"> HappyOlds@happy.tn</a>
\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"> </div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-4 address-grid\">
\t\t\t\t\t\t\t\t\t\t<div class=\"address-left\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"dodecagon f1\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"dodecagon-in f1\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"dodecagon-bg f1\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fas fa-map-marker-alt\" aria-hidden=\"true\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"address-right\">
\t\t\t\t\t\t\t\t\t\t\t<h6>Localisation</h6>
\t\t\t\t\t\t\t\t\t\t\t<p>Cité ElGhazela Tunis.

\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"> </div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"> </div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!--//tab_one-->
\t\t\t\t\t\t<div class=\"tab2\">

\t\t\t\t\t\t\t<div class=\"tab-info\">
\t\t\t\t\t\t\t\t<div class=\"contact_grid_right\">
\t\t\t\t\t\t\t\t\t<h6>Please fill this form to contact with us.</h6>
\t\t\t\t\t\t\t\t\t<form action=\"#\" method=\"post\">
\t\t\t\t\t\t\t\t\t\t<div class=\"contact_left_grid\">
\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"Name\" placeholder=\"Name\" required=\"\">
\t\t\t\t\t\t\t\t\t\t\t<input type=\"email\" name=\"Email\" placeholder=\"Email\" required=\"\">

\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"Telephone\" placeholder=\"Telephone\" required=\"\">
\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"Subject\" placeholder=\"Subject\" required=\"\">
\t\t\t\t\t\t\t\t\t\t\t<textarea name=\"Message\" onfocus=\"this.value = '';\" onblur=\"if (this.value == '') {this.value = 'Message...';}\" required=\"\">Message...</textarea>
\t\t\t\t\t\t\t\t\t\t\t<input type=\"submit\" value=\"Submit\">
\t\t\t\t\t\t\t\t\t\t\t<input type=\"reset\" value=\"Clear\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"> </div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!--//tab_two-->
\t\t\t\t\t\t<div class=\"tab3\">

\t\t\t\t\t\t\t<div class=\"tab-info\">
\t\t\t\t\t\t\t\t<div class=\"contact-map\">

\t\t\t\t\t\t\t\t\t<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d100949.24429313939!2d-122.44206553967531!3d37.75102885910819!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80859a6d00690021%3A0x4a501367f076adff!2sSan+Francisco%2C+CA%2C+USA!5e0!3m2!1sen!2sin!4v1472190196783\"
\t\t\t\t\t\t\t\t\t    class=\"map\" style=\"border:0\" allowfullscreen=\"\"></iframe>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"clearfix\"> </div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<!--//tabs-->
\t\t\t<div class=\"clearfix\"> </div>
\t\t\t<ul class=\"social-nav model-3d-0 footer-social social two\">
\t\t\t\t<li>
\t\t\t\t\t<a href=\"#\" class=\"facebook\">
\t\t\t\t\t\t<div class=\"front\">
\t\t\t\t\t\t\t<i class=\"fab fa-facebook-f\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t</div>

\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<li>
\t\t\t\t\t<a href=\"#\" class=\"twitter\">
\t\t\t\t\t\t<div class=\"front\">
\t\t\t\t\t\t\t<i class=\"fab fa-twitter\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t</div>

\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<li>
\t\t\t\t\t<a href=\"#\" class=\"instagram\">
\t\t\t\t\t\t<div class=\"front\">
\t\t\t\t\t\t\t<i class=\"fab fa-instagram\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t</div>

\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<li>
\t\t\t\t\t<a href=\"#\" class=\"pinterest\">
\t\t\t\t\t\t<div class=\"front\">
\t\t\t\t\t\t\t<i class=\"fab fa-linkedin\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t</div>

\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t</ul>
\t\t\t<p class=\"copy-right\">2019 © Happy olds. All rights reserved
\t\t\t</p>
\t\t</div>
\t</div>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "front/_footer.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  36 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"footer\" id=\"contact\">
\t\t<div class=\"footer_inner_info_wthree_agileits\">
\t\t\t<!--/tabs-->
\t\t\t<div class=\"responsive_tabs\">
\t\t\t\t<div id=\"horizontalTab\">
\t\t\t\t\t<ul class=\"resp-tabs-list\">
\t\t\t\t\t\t<li>  </li>
\t\t\t\t\t\t<li> </li>
\t\t\t\t\t\t<li> </li>
\t\t\t\t\t</ul>
\t\t\t\t\t<div class=\"resp-tabs-container\">
\t\t\t\t\t\t<!--/tab_one-->
\t\t\t\t\t\t<div class=\"tab1\">
\t\t\t\t\t\t\t<div class=\"tab-info\">

\t\t\t\t\t\t\t\t<div class=\"address\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-4 address-grid\">
\t\t\t\t\t\t\t\t\t\t<div class=\"address-left\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"dodecagon f1\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"dodecagon-in f1\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"dodecagon-bg f1\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fas fa-phone-volume\" aria-hidden=\"true\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"address-right\">
\t\t\t\t\t\t\t\t\t\t\t<h6>Numéro de téléphone</h6>
\t\t\t\t\t\t\t\t\t\t\t<p>+216 99 000 100,+216 71 000 100</p>

\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"> </div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-4 address-grid\">
\t\t\t\t\t\t\t\t\t\t<div class=\"address-left\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"dodecagon f1\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"dodecagon-in f1\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"dodecagon-bg f1\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"far fa-envelope\" aria-hidden=\"true\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"address-right\">
\t\t\t\t\t\t\t\t\t\t\t<h6>Adresse Email</h6>
\t\t\t\t\t\t\t\t\t\t\t<p>Email :
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"mailto:example@email.com\"> HappyOlds@happy.tn</a>
\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"> </div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-4 address-grid\">
\t\t\t\t\t\t\t\t\t\t<div class=\"address-left\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"dodecagon f1\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"dodecagon-in f1\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"dodecagon-bg f1\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fas fa-map-marker-alt\" aria-hidden=\"true\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"address-right\">
\t\t\t\t\t\t\t\t\t\t\t<h6>Localisation</h6>
\t\t\t\t\t\t\t\t\t\t\t<p>Cité ElGhazela Tunis.

\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"> </div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"> </div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!--//tab_one-->
\t\t\t\t\t\t<div class=\"tab2\">

\t\t\t\t\t\t\t<div class=\"tab-info\">
\t\t\t\t\t\t\t\t<div class=\"contact_grid_right\">
\t\t\t\t\t\t\t\t\t<h6>Please fill this form to contact with us.</h6>
\t\t\t\t\t\t\t\t\t<form action=\"#\" method=\"post\">
\t\t\t\t\t\t\t\t\t\t<div class=\"contact_left_grid\">
\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"Name\" placeholder=\"Name\" required=\"\">
\t\t\t\t\t\t\t\t\t\t\t<input type=\"email\" name=\"Email\" placeholder=\"Email\" required=\"\">

\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"Telephone\" placeholder=\"Telephone\" required=\"\">
\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"Subject\" placeholder=\"Subject\" required=\"\">
\t\t\t\t\t\t\t\t\t\t\t<textarea name=\"Message\" onfocus=\"this.value = '';\" onblur=\"if (this.value == '') {this.value = 'Message...';}\" required=\"\">Message...</textarea>
\t\t\t\t\t\t\t\t\t\t\t<input type=\"submit\" value=\"Submit\">
\t\t\t\t\t\t\t\t\t\t\t<input type=\"reset\" value=\"Clear\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"> </div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!--//tab_two-->
\t\t\t\t\t\t<div class=\"tab3\">

\t\t\t\t\t\t\t<div class=\"tab-info\">
\t\t\t\t\t\t\t\t<div class=\"contact-map\">

\t\t\t\t\t\t\t\t\t<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d100949.24429313939!2d-122.44206553967531!3d37.75102885910819!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80859a6d00690021%3A0x4a501367f076adff!2sSan+Francisco%2C+CA%2C+USA!5e0!3m2!1sen!2sin!4v1472190196783\"
\t\t\t\t\t\t\t\t\t    class=\"map\" style=\"border:0\" allowfullscreen=\"\"></iframe>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"clearfix\"> </div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<!--//tabs-->
\t\t\t<div class=\"clearfix\"> </div>
\t\t\t<ul class=\"social-nav model-3d-0 footer-social social two\">
\t\t\t\t<li>
\t\t\t\t\t<a href=\"#\" class=\"facebook\">
\t\t\t\t\t\t<div class=\"front\">
\t\t\t\t\t\t\t<i class=\"fab fa-facebook-f\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t</div>

\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<li>
\t\t\t\t\t<a href=\"#\" class=\"twitter\">
\t\t\t\t\t\t<div class=\"front\">
\t\t\t\t\t\t\t<i class=\"fab fa-twitter\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t</div>

\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<li>
\t\t\t\t\t<a href=\"#\" class=\"instagram\">
\t\t\t\t\t\t<div class=\"front\">
\t\t\t\t\t\t\t<i class=\"fab fa-instagram\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t</div>

\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<li>
\t\t\t\t\t<a href=\"#\" class=\"pinterest\">
\t\t\t\t\t\t<div class=\"front\">
\t\t\t\t\t\t\t<i class=\"fab fa-linkedin\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t</div>

\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t</ul>
\t\t\t<p class=\"copy-right\">2019 © Happy olds. All rights reserved
\t\t\t</p>
\t\t</div>
\t</div>", "front/_footer.html.twig", "/var/www/html/3A11/happy_olds/app/Resources/views/front/_footer.html.twig");
    }
}
