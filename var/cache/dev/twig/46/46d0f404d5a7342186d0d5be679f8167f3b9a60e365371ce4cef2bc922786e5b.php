<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* :front:_head.html.twig */
class __TwigTemplate_384146b9b847989578821f238419b5305e321c25e287f4ea33cb5f5cfd2fb410 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", ":front:_head.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", ":front:_head.html.twig"));

        // line 1
        echo "<head>
\t<title>Relief a Society Category Bootstrap Responsive Web Template | Home :: w3layouts</title>

\t<!--/tags -->
\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
\t<meta charset=\"utf-8\">
\t<meta name=\"keywords\" content=\"Relief Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design\" />
\t<script type=\"application/x-javascript\">
\t\taddEventListener(\"load\", function () {
\t\t\tsetTimeout(hideURLbar, 0);
\t\t}, false);

\t\tfunction hideURLbar() {
\t\t\twindow.scrollTo(0, 1);
\t\t}
\t</script>
\t<!--//tags -->
\t<link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
\t<link href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
\t<link href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/prettyPhoto.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />
\t<link href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/easy-responsive-tabs.css"), "html", null, true);
        echo "\" rel='stylesheet' type='text/css' />
\t<link href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/fontawesome-all.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t<link href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/Login1.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t<link href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/loginstyle.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t<!-- //for bootstrap working -->
\t<link href=\"//fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,600,600i,700\" rel=\"stylesheet\">
\t<link href=\"//fonts.googleapis.com/css?family=Poppins:200,200i,300,300i,400,400i,500,500i,600,600i,700\" rel=\"stylesheet\">
\t<link rel=\"stylesheet\" href=\"//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css\"/>

</head>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return ":front:_head.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 25,  76 => 24,  72 => 23,  68 => 22,  64 => 21,  60 => 20,  56 => 19,  36 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<head>
\t<title>Relief a Society Category Bootstrap Responsive Web Template | Home :: w3layouts</title>

\t<!--/tags -->
\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
\t<meta charset=\"utf-8\">
\t<meta name=\"keywords\" content=\"Relief Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design\" />
\t<script type=\"application/x-javascript\">
\t\taddEventListener(\"load\", function () {
\t\t\tsetTimeout(hideURLbar, 0);
\t\t}, false);

\t\tfunction hideURLbar() {
\t\t\twindow.scrollTo(0, 1);
\t\t}
\t</script>
\t<!--//tags -->
\t<link href=\"{{ asset('css/bootstrap.css') }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
\t<link href=\"{{ asset('css/style.css') }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
\t<link href=\"{{ asset('css/prettyPhoto.css') }}\" rel=\"stylesheet\" type=\"text/css\" />
\t<link href=\"{{ asset('css/easy-responsive-tabs.css') }}\" rel='stylesheet' type='text/css' />
\t<link href=\"{{ asset('css/fontawesome-all.css') }}\" rel=\"stylesheet\">
\t<link href=\"{{ asset('css/Login1.css') }}\" rel=\"stylesheet\">
\t<link href=\"{{ asset('css/loginstyle.css') }}\" rel=\"stylesheet\">
\t<!-- //for bootstrap working -->
\t<link href=\"//fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,600,600i,700\" rel=\"stylesheet\">
\t<link href=\"//fonts.googleapis.com/css?family=Poppins:200,200i,300,300i,400,400i,500,500i,600,600i,700\" rel=\"stylesheet\">
\t<link rel=\"stylesheet\" href=\"//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css\"/>

</head>", ":front:_head.html.twig", "/var/www/html/3A11/happy_olds/app/Resources/views/front/_head.html.twig");
    }
}
