<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* front/_scripts.html.twig */
class __TwigTemplate_aaf4088702bc467cf03ef9056f09148fbb87a1133aa51617a31731c35783446f extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "front/_scripts.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "front/_scripts.html.twig"));

        // line 1
        echo "<script type=\"text/javascript\" charset=\"utf8\" src=\"https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js\"></script>

<script src=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/rechercheTable/table.js"), "html", null, true);
        echo "\"></script>

<script type=\"text/javascript\" src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery-2.2.3.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap.js"), "html", null, true);
        echo "\"></script>
<!-- script for responsive tabs -->
<script src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/easy-responsive-tabs.js"), "html", null, true);
        echo "\"></script>
<script src=";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery-2.2.3.minLogin.js"), "html", null, true);
        echo "></script>
<script src=";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/easyResponsiveTabsLogin.js"), "html", null, true);
        echo "></script>
<script>
    \$(document).ready(function () {
        \$('#horizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            activate: function (event) { // Callback function if tab is switched
                var \$tab = \$(this);
                var \$info = \$('#tabInfo');
                var \$name = \$('span', \$info);
                \$name.text(\$tab.text());
                \$info.show();
            }
        });
        \$('#verticalTab').easyResponsiveTabs({
            type: 'vertical',
            width: 'auto',
            fit: true
        });
    });
</script>
<!--// script for responsive tabs -->
<script src=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/responsiveslides.min.js"), "html", null, true);
        echo "\"></script>
<script>
    // You can also use \"\$(window).load(function() {\"
    \$(function () {
        // Slideshow 4
        \$(\"#slider4\").responsiveSlides({
            auto: true,
            pager: true,
            nav: false,
            speed: 500,
            namespace: \"callbacks\",
            timeout: 2000,
            before: function () {
                \$('.events').append(\"<li>before event fired.</li>\");
            },
            after: function () {
                \$('.events').append(\"<li>after event fired.</li>\");
            }
        });

    });
</script>
<script type=\"text/javascript\" src=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/all.js"), "html", null, true);
        echo "\"></script>
<script>
    \$('ul.dropdown-menu li').hover(function () {
        \$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    }, function () {
        \$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
    });
</script>

<!-- js -->
<link href=\"";
        // line 66
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/simplyCountdown.css"), "html", null, true);
        echo "\" rel='stylesheet' type='text/css'/>

<!--js-->
";
        // line 71
        echo "    ";
        // line 72
        echo "        ";
        // line 73
        echo "            ";
        // line 74
        echo "        ";
        // line 75
        echo "    ";
        // line 76
        echo "
    ";
        // line 78
        echo "    ";
        // line 79
        echo "    ";
        // line 80
        echo "        ";
        // line 81
        echo "        ";
        // line 82
        echo "    ";
        // line 83
        echo "
    ";
        // line 85
        echo "    ";
        // line 88
        echo "<!-- Smooth-Scrolling-JavaScript -->
<script type=\"text/javascript\" src=\"";
        // line 89
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/easing.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 90
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/move-top.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\">
    jQuery(document).ready(function (\$) {
        \$(\".scroll, .navbar li a, .footer li a\").click(function (event) {
            if(\$(this).hasClass('scroll')) {
                \$('html,body').animate({
                    scrollTop: \$(this.hash).offset().top
                }, 1000);
            }
        });
    });
</script>
<!-- //Smooth-Scrolling-JavaScript -->
<script type=\"text/javascript\">
    \$(document).ready(function () {
        /*
                                var defaults = {
                                      containerID: 'toTop', // fading element id
                                    containerHoverID: 'toTopHover', // fading element hover id
                                    scrollSpeed: 1200,
                                    easingType: 'linear'
                                 };
                                */

        \$().UItoTop({
            easingType: 'easeOutQuart'
        });

    });
</script>


<a href=\"#home\" class=\"scroll\" id=\"toTop\" style=\"display: block;\">
    <span id=\"toTopHover\" style=\"opacity: 1;\"> </span>
</a>

<!-- jQuery-Photo-filter-lightbox-Gallery-plugin -->
<script type=\"text/javascript\" src=\"";
        // line 127
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery-1.7.2.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 128
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery.quicksand.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"";
        // line 129
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/script.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"";
        // line 130
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery.prettyPhoto.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<!-- //jQuery-Photo-filter-lightbox-Gallery-plugin -->

<script>
    \$(document).ready(function () {
        \$('.userRole').removeAttr('multiple');
    });
</script>



";
        // line 142
        echo "
";
        // line 147
        echo "
";
        // line 149
        echo "

<script>
    \$('.dropdown-menu').on(\"click.bs.dropdown\", function (e) { e.stopPropagation(); e.preventDefault(); });
</script>
<script src=\"";
        // line 154
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/controle-saisie/register.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 155
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/controle-saisie/login.js"), "html", null, true);
        echo "\"></script>


";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "front/_scripts.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  251 => 155,  247 => 154,  240 => 149,  237 => 147,  234 => 142,  220 => 130,  216 => 129,  212 => 128,  208 => 127,  168 => 90,  164 => 89,  161 => 88,  159 => 85,  156 => 83,  154 => 82,  152 => 81,  150 => 80,  148 => 79,  146 => 78,  143 => 76,  141 => 75,  139 => 74,  137 => 73,  135 => 72,  133 => 71,  127 => 66,  114 => 56,  89 => 34,  62 => 10,  58 => 9,  54 => 8,  49 => 6,  45 => 5,  40 => 3,  36 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<script type=\"text/javascript\" charset=\"utf8\" src=\"https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js\"></script>

<script src=\"{{ asset('js/rechercheTable/table.js') }}\"></script>

<script type=\"text/javascript\" src=\"{{ asset('js/jquery-2.2.3.min.js') }}\"></script>
<script type=\"text/javascript\" src=\"{{ asset('js/bootstrap.js') }}\"></script>
<!-- script for responsive tabs -->
<script src=\"{{ asset('js/easy-responsive-tabs.js') }}\"></script>
<script src={{ asset('js/jquery-2.2.3.minLogin.js') }}></script>
<script src={{ asset('js/easyResponsiveTabsLogin.js') }}></script>
<script>
    \$(document).ready(function () {
        \$('#horizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            activate: function (event) { // Callback function if tab is switched
                var \$tab = \$(this);
                var \$info = \$('#tabInfo');
                var \$name = \$('span', \$info);
                \$name.text(\$tab.text());
                \$info.show();
            }
        });
        \$('#verticalTab').easyResponsiveTabs({
            type: 'vertical',
            width: 'auto',
            fit: true
        });
    });
</script>
<!--// script for responsive tabs -->
<script src=\"{{ asset('js/responsiveslides.min.js') }}\"></script>
<script>
    // You can also use \"\$(window).load(function() {\"
    \$(function () {
        // Slideshow 4
        \$(\"#slider4\").responsiveSlides({
            auto: true,
            pager: true,
            nav: false,
            speed: 500,
            namespace: \"callbacks\",
            timeout: 2000,
            before: function () {
                \$('.events').append(\"<li>before event fired.</li>\");
            },
            after: function () {
                \$('.events').append(\"<li>after event fired.</li>\");
            }
        });

    });
</script>
<script type=\"text/javascript\" src=\"{{ asset('js/all.js') }}\"></script>
<script>
    \$('ul.dropdown-menu li').hover(function () {
        \$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    }, function () {
        \$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
    });
</script>

<!-- js -->
<link href=\"{{ asset('css/simplyCountdown.css') }}\" rel='stylesheet' type='text/css'/>

<!--js-->
{#<!--/tooltip -->#}
{#<script>#}
    {#\$(function () {#}
        {#\$('[data-toggle=\"tooltip\"]').tooltip({#}
            {#trigger: 'manual'#}
        {#}).tooltip('show');#}
    {#});#}

    {#// \$( window ).scroll(function() {#}
    {#// if(\$( window ).scrollTop() > 10){  // scroll down abit and get the action#}
    {#\$(\".progress-bar\").each(function () {#}
        {#each_bar_width = \$(this).attr('aria-valuenow');#}
        {#\$(this).width(each_bar_width + '%');#}
    {#});#}

    {#//  }#}
    {#// });#}
{#</script>#}
{#<!--//tooltip -->#}
<!-- Smooth-Scrolling-JavaScript -->
<script type=\"text/javascript\" src=\"{{ asset('js/easing.js') }}\"></script>
<script type=\"text/javascript\" src=\"{{ asset('js/move-top.js') }}\"></script>
<script type=\"text/javascript\">
    jQuery(document).ready(function (\$) {
        \$(\".scroll, .navbar li a, .footer li a\").click(function (event) {
            if(\$(this).hasClass('scroll')) {
                \$('html,body').animate({
                    scrollTop: \$(this.hash).offset().top
                }, 1000);
            }
        });
    });
</script>
<!-- //Smooth-Scrolling-JavaScript -->
<script type=\"text/javascript\">
    \$(document).ready(function () {
        /*
                                var defaults = {
                                      containerID: 'toTop', // fading element id
                                    containerHoverID: 'toTopHover', // fading element hover id
                                    scrollSpeed: 1200,
                                    easingType: 'linear'
                                 };
                                */

        \$().UItoTop({
            easingType: 'easeOutQuart'
        });

    });
</script>


<a href=\"#home\" class=\"scroll\" id=\"toTop\" style=\"display: block;\">
    <span id=\"toTopHover\" style=\"opacity: 1;\"> </span>
</a>

<!-- jQuery-Photo-filter-lightbox-Gallery-plugin -->
<script type=\"text/javascript\" src=\"{{ asset('js/jquery-1.7.2.js') }}\"></script>
<script src=\"{{ asset('js/jquery.quicksand.js') }}\" type=\"text/javascript\"></script>
<script src=\"{{ asset('js/script.js') }}\" type=\"text/javascript\"></script>
<script src=\"{{ asset('js/jquery.prettyPhoto.js') }}\" type=\"text/javascript\"></script>
<!-- //jQuery-Photo-filter-lightbox-Gallery-plugin -->

<script>
    \$(document).ready(function () {
        \$('.userRole').removeAttr('multiple');
    });
</script>



{#script calendar begin#}

{#<script type=\"text/javascript\" src=\"{{ asset('bundles/fullcalendar/js/fullcalendar/lib/jquery.min.js') }}\"></script>#}
{#<script type=\"text/javascript\" src=\"{{ asset('bundles/fullcalendar/js/fullcalendar/lib/moment.min.js') }}\"></script>#}
{#<script type=\"text/javascript\" src=\"{{ asset('bundles/fullcalendar/js/fullcalendar/fullcalendar.min.js') }}\"></script>#}
{#<script type=\"text/javascript\" src=\"{{ asset('bundles/fullcalendar/js/fullcalendar/fullcalendar.default-settings.js') }}\"></script>#}

{#script calendar end#}


<script>
    \$('.dropdown-menu').on(\"click.bs.dropdown\", function (e) { e.stopPropagation(); e.preventDefault(); });
</script>
<script src=\"{{ asset('js/controle-saisie/register.js') }}\"></script>
<script src=\"{{ asset('js/controle-saisie/login.js') }}\"></script>


", "front/_scripts.html.twig", "/var/www/html/3A11/happy_olds/app/Resources/views/front/_scripts.html.twig");
    }
}
