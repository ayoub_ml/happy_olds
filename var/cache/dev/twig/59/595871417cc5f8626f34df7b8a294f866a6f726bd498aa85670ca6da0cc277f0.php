<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* :front:about.html.twig */
class __TwigTemplate_7639e13db1668ba18b7a4bd436ad299362e93049bb4889ab9d11ab5fe4d9ee34 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "front/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", ":front:about.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", ":front:about.html.twig"));

        $this->parent = $this->loadTemplate("front/base.html.twig", ":front:about.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "    <div class=\"banner_bottom\" id=\"about\">
        <div class=\"container\">
            <h3 class=\"tittle_w3_agileinfo\">About Our Relief</h3>
            <div class=\"inner_sec_info_w3layouts\">
                <div class=\"help_full\">
                    <ul class=\"rslides\" id=\"slider4\">
                        <li>
                            <div class=\"respon_info_img\">
                                <img src=\"images/banner3.jpg\" class=\"img-responsive\" alt=\"Relief\">
                            </div>
                            <div class=\"banner_bottom_left\">
                                <h4>Feed For Hungry Child</h4>

                                <p>Maecenas quis neque libero. Class aptent taciti.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Pellentesque
                                    convallis diam consequat magna vulputate malesuada. Cras a ornare elit. Nulla viverra pharetra sem, eget pulvinar
                                    neque pharetra ac.</p>
                                <p>Lorem Ipsum convallis diam consequat magna vulputate malesuada. Cras a ornare elit. Nulla viverra pharetra sem, eget
                                    pulvinar neque pharetra ac.</p>
                                <div class=\"ab_button\">
                                    <a class=\"btn btn-primary btn-lg hvr-underline-from-left\" href=\"single.html\" role=\"button\">Read More </a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class=\"respon_info_img\">
                                <img src=\"images/banner2.jpg\" class=\"img-responsive\" alt=\"Relief\">
                            </div>
                            <div class=\"banner_bottom_left\">
                                <h4>Education to Every Child</h4>

                                <p>Maecenas quis neque libero. Class aptent taciti.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Pellentesque
                                    convallis diam consequat magna vulputate malesuada. Cras a ornare elit. Nulla viverra pharetra sem, eget pulvinar
                                    neque pharetra ac.</p>
                                <p>Lorem Ipsum convallis diam consequat magna vulputate malesuada. Cras a ornare elit. Nulla viverra pharetra sem, eget
                                    pulvinar neque pharetra ac.</p>
                                <div class=\"ab_button\">
                                    <a class=\"btn btn-primary btn-lg hvr-underline-from-left\" href=\"single.html\" role=\"button\">Read More </a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class=\"respon_info_img\">
                                <img src=\"images/banner1.jpg\" class=\"img-responsive\" alt=\"Relief\">
                            </div>
                            <div class=\"banner_bottom_left\">
                                <h4>Patient and Family Support</h4>
                                <p>Maecenas quis neque libero. Class aptent taciti.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Pellentesque
                                    convallis diam consequat magna vulputate malesuada. Cras a ornare elit. Nulla viverra pharetra sem, eget pulvinar
                                    neque pharetra ac.</p>
                                <p>Lorem Ipsum convallis diam consequat magna vulputate malesuada. Cras a ornare elit. Nulla viverra pharetra sem, eget
                                    pulvinar neque pharetra ac.</p>
                                <div class=\"ab_button\">
                                    <a class=\"btn btn-primary btn-lg hvr-underline-from-left\" href=\"single.html\" role=\"button\">Read More </a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class=\"respon_info_img\">
                                <img src=\"images/banner4.jpg\" class=\"img-responsive\" alt=\"Relief\">
                            </div>
                            <div class=\"banner_bottom_left\">
                                <h4>Provide Treatment</h4>
                                <p>Maecenas quis neque libero. Class aptent taciti.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Pellentesque
                                    convallis diam consequat magna vulputate malesuada. Cras a ornare elit. Nulla viverra pharetra sem, eget pulvinar
                                    neque pharetra ac.</p>
                                <p>Lorem Ipsum convallis diam consequat magna vulputate malesuada. Cras a ornare elit. Nulla viverra pharetra sem, eget
                                    pulvinar neque pharetra ac.</p>
                                <div class=\"ab_button\">
                                    <a class=\"btn btn-primary btn-lg hvr-underline-from-left\" href=\"single.html\" role=\"button\">Read More </a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class=\"news-main\">
                <div class=\"col-md-6 banner_bottom_left\">
                    <div class=\"banner_bottom_pos\">
                        <div class=\"banner_bottom_pos_grid\">
                            <div class=\"col-xs-3 banner_bottom_grid_left\">
                                <div class=\"banner_bottom_grid_left_grid\">
                                    <div class=\"dodecagon b1\">
                                        <div class=\"dodecagon-in b1\">
                                            <div class=\"dodecagon-bg b1\">
                                                <span class=\"fas fa-camera-retro\" aria-hidden=\"true\"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class=\"col-xs-9 banner_bottom_grid_right\">
                                <h4>Health and Medication</h4>
                                <p>Lorem Ipsum is simply text the printing and typesetting standard industry.</p>
                                <div class=\"barWrapper\">
\t\t\t\t\t\t\t\t\t<span class=\"progressText\">
\t\t\t\t\t\t\t\t\t\t<b>Donators</b>
\t\t\t\t\t\t\t\t\t</span>
                                    <div class=\"progress\">
                                        <div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"85\" aria-valuemin=\"0\" aria-valuemax=\"100\">
                                            <span class=\"popOver\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"85%\"> </span>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class=\"clearfix\"> </div>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-6 banner_bottom_left\">
                    <div class=\"banner_bottom_pos\">
                        <div class=\"banner_bottom_pos_grid\">
                            <div class=\"col-xs-3 banner_bottom_grid_left\">
                                <div class=\"banner_bottom_grid_left_grid\">
                                    <div class=\"dodecagon b1\">
                                        <div class=\"dodecagon-in b1\">
                                            <div class=\"dodecagon-bg b1\">
                                                <span class=\"far fa-thumbs-up\" aria-hidden=\"true\"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class=\"col-xs-9 banner_bottom_grid_right\">
                                <h4>Wildlife and Ecosystems</h4>
                                <p>Lorem Ipsum is simply text the printing and typesetting standard industry.</p>
                                <div class=\"barWrapper\">
\t\t\t\t\t\t\t\t\t<span class=\"progressText\">
\t\t\t\t\t\t\t\t\t\t<b>Donators</b>
\t\t\t\t\t\t\t\t\t</span>
                                    <div class=\"progress\">
                                        <div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"75\" aria-valuemin=\"0\" aria-valuemax=\"100\">
                                            <span class=\"popOver\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"75%\"> </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class=\"clearfix\"> </div>
                        </div>
                    </div>
                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return ":front:about.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 4,  51 => 3,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'front/base.html.twig'%}

{% block content %}
    <div class=\"banner_bottom\" id=\"about\">
        <div class=\"container\">
            <h3 class=\"tittle_w3_agileinfo\">About Our Relief</h3>
            <div class=\"inner_sec_info_w3layouts\">
                <div class=\"help_full\">
                    <ul class=\"rslides\" id=\"slider4\">
                        <li>
                            <div class=\"respon_info_img\">
                                <img src=\"images/banner3.jpg\" class=\"img-responsive\" alt=\"Relief\">
                            </div>
                            <div class=\"banner_bottom_left\">
                                <h4>Feed For Hungry Child</h4>

                                <p>Maecenas quis neque libero. Class aptent taciti.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Pellentesque
                                    convallis diam consequat magna vulputate malesuada. Cras a ornare elit. Nulla viverra pharetra sem, eget pulvinar
                                    neque pharetra ac.</p>
                                <p>Lorem Ipsum convallis diam consequat magna vulputate malesuada. Cras a ornare elit. Nulla viverra pharetra sem, eget
                                    pulvinar neque pharetra ac.</p>
                                <div class=\"ab_button\">
                                    <a class=\"btn btn-primary btn-lg hvr-underline-from-left\" href=\"single.html\" role=\"button\">Read More </a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class=\"respon_info_img\">
                                <img src=\"images/banner2.jpg\" class=\"img-responsive\" alt=\"Relief\">
                            </div>
                            <div class=\"banner_bottom_left\">
                                <h4>Education to Every Child</h4>

                                <p>Maecenas quis neque libero. Class aptent taciti.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Pellentesque
                                    convallis diam consequat magna vulputate malesuada. Cras a ornare elit. Nulla viverra pharetra sem, eget pulvinar
                                    neque pharetra ac.</p>
                                <p>Lorem Ipsum convallis diam consequat magna vulputate malesuada. Cras a ornare elit. Nulla viverra pharetra sem, eget
                                    pulvinar neque pharetra ac.</p>
                                <div class=\"ab_button\">
                                    <a class=\"btn btn-primary btn-lg hvr-underline-from-left\" href=\"single.html\" role=\"button\">Read More </a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class=\"respon_info_img\">
                                <img src=\"images/banner1.jpg\" class=\"img-responsive\" alt=\"Relief\">
                            </div>
                            <div class=\"banner_bottom_left\">
                                <h4>Patient and Family Support</h4>
                                <p>Maecenas quis neque libero. Class aptent taciti.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Pellentesque
                                    convallis diam consequat magna vulputate malesuada. Cras a ornare elit. Nulla viverra pharetra sem, eget pulvinar
                                    neque pharetra ac.</p>
                                <p>Lorem Ipsum convallis diam consequat magna vulputate malesuada. Cras a ornare elit. Nulla viverra pharetra sem, eget
                                    pulvinar neque pharetra ac.</p>
                                <div class=\"ab_button\">
                                    <a class=\"btn btn-primary btn-lg hvr-underline-from-left\" href=\"single.html\" role=\"button\">Read More </a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class=\"respon_info_img\">
                                <img src=\"images/banner4.jpg\" class=\"img-responsive\" alt=\"Relief\">
                            </div>
                            <div class=\"banner_bottom_left\">
                                <h4>Provide Treatment</h4>
                                <p>Maecenas quis neque libero. Class aptent taciti.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Pellentesque
                                    convallis diam consequat magna vulputate malesuada. Cras a ornare elit. Nulla viverra pharetra sem, eget pulvinar
                                    neque pharetra ac.</p>
                                <p>Lorem Ipsum convallis diam consequat magna vulputate malesuada. Cras a ornare elit. Nulla viverra pharetra sem, eget
                                    pulvinar neque pharetra ac.</p>
                                <div class=\"ab_button\">
                                    <a class=\"btn btn-primary btn-lg hvr-underline-from-left\" href=\"single.html\" role=\"button\">Read More </a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class=\"news-main\">
                <div class=\"col-md-6 banner_bottom_left\">
                    <div class=\"banner_bottom_pos\">
                        <div class=\"banner_bottom_pos_grid\">
                            <div class=\"col-xs-3 banner_bottom_grid_left\">
                                <div class=\"banner_bottom_grid_left_grid\">
                                    <div class=\"dodecagon b1\">
                                        <div class=\"dodecagon-in b1\">
                                            <div class=\"dodecagon-bg b1\">
                                                <span class=\"fas fa-camera-retro\" aria-hidden=\"true\"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class=\"col-xs-9 banner_bottom_grid_right\">
                                <h4>Health and Medication</h4>
                                <p>Lorem Ipsum is simply text the printing and typesetting standard industry.</p>
                                <div class=\"barWrapper\">
\t\t\t\t\t\t\t\t\t<span class=\"progressText\">
\t\t\t\t\t\t\t\t\t\t<b>Donators</b>
\t\t\t\t\t\t\t\t\t</span>
                                    <div class=\"progress\">
                                        <div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"85\" aria-valuemin=\"0\" aria-valuemax=\"100\">
                                            <span class=\"popOver\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"85%\"> </span>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class=\"clearfix\"> </div>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-6 banner_bottom_left\">
                    <div class=\"banner_bottom_pos\">
                        <div class=\"banner_bottom_pos_grid\">
                            <div class=\"col-xs-3 banner_bottom_grid_left\">
                                <div class=\"banner_bottom_grid_left_grid\">
                                    <div class=\"dodecagon b1\">
                                        <div class=\"dodecagon-in b1\">
                                            <div class=\"dodecagon-bg b1\">
                                                <span class=\"far fa-thumbs-up\" aria-hidden=\"true\"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class=\"col-xs-9 banner_bottom_grid_right\">
                                <h4>Wildlife and Ecosystems</h4>
                                <p>Lorem Ipsum is simply text the printing and typesetting standard industry.</p>
                                <div class=\"barWrapper\">
\t\t\t\t\t\t\t\t\t<span class=\"progressText\">
\t\t\t\t\t\t\t\t\t\t<b>Donators</b>
\t\t\t\t\t\t\t\t\t</span>
                                    <div class=\"progress\">
                                        <div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"75\" aria-valuemin=\"0\" aria-valuemax=\"100\">
                                            <span class=\"popOver\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"75%\"> </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class=\"clearfix\"> </div>
                        </div>
                    </div>
                </div>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </div>
{% endblock %}", ":front:about.html.twig", "/var/www/html/3A11/happy_olds/app/Resources/views/front/about.html.twig");
    }
}
