<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @GestionAgee/Agee/afficherTraitementAgee.html.twig */
class __TwigTemplate_b95f1062c5c47bf0df07346a65d5e42903c9090158cb527907460cbb1647e326 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "front/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@GestionAgee/Agee/afficherTraitementAgee.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@GestionAgee/Agee/afficherTraitementAgee.html.twig"));

        $this->parent = $this->loadTemplate("front/base.html.twig", "@GestionAgee/Agee/afficherTraitementAgee.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "    <center>Planning des visites medicales</center>";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_content($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 7
        echo "










    <!-- banner -->
    <div class=\"banner_top\">
        <div id=\"myCarousel\" class=\"carousel slide\" data-ride=\"carousel\">
            <!-- Indicators -->
            <ol class=\"carousel-indicators\">
                <li data-target=\"#myCarousel\" data-slide-to=\"0\" class=\"active\"></li>
                <li data-target=\"#myCarousel\" data-slide-to=\"1\" class=\"\"></li>
                <li data-target=\"#myCarousel\" data-slide-to=\"2\" class=\"\"></li>
                <li data-target=\"#myCarousel\" data-slide-to=\"3\" class=\"\"></li>
            </ol>
            <div class=\"carousel-inner\" role=\"listbox\">
                <div class=\"item active\">
                    <div class=\"container\">
                        <div class=\"carousel-caption\">
                            <h3>Change Begins With You.</h3>


                        </div>
                    </div>
                </div>
                <div class=\"item item2\">
                    <div class=\"container\">
                        <div class=\"carousel-caption\">
                            <h3>Make good things happen</h3>


                        </div>
                    </div>
                </div>
                <div class=\"item item3\">
                    <div class=\"container\">
                        <div class=\"carousel-caption\">
                            <h3>Change Begins With You.</h3>


                        </div>
                    </div>
                </div>
                <div class=\"item item4\">
                    <div class=\"container\">
                        <div class=\"carousel-caption\">


                        </div>
                    </div>
                </div>
            </div>
            <a class=\"left carousel-control\" href=\"#myCarousel\" role=\"button\" data-slide=\"prev\">
                <span class=\"fa fa-chevron-left\" aria-hidden=\"true\"></span>
                <span class=\"sr-only\">Previous</span>
            </a>
            <a class=\"right carousel-control\" href=\"#myCarousel\" role=\"button\" data-slide=\"next\">
                <span class=\"fa fa-chevron-right\" aria-hidden=\"true\"></span>
                <span class=\"sr-only\">Next</span>
            </a>
            <!-- The Modal -->
        </div>
    </div>











    <style>

        body {
            background-color: #F1F1F1;
        }

        #planning {
            background-color: #FAFAFA;
            width: 90%;
            margin-left: 5%;
            padding: 15px;
        }

        .table {
            border: 2px solid #629aa9 !important;

        }

        .table th {
            border: 2px solid #629aa9 !important;
        }

        .table td {
            border: 2px solid #629aa9 !important;
        }

    </style>









    <link rel=\"stylesheet\" type=\"text/css\" href=\"https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css\">


    <br><br><br><br><br><br><br><br><br><br><br>

    <br>
    <br>

    <div id=\"planning\" >
            <h1 style=\"text-align: center ; font-size: 30px\"  >Planning des Traitements médicaux</h1>
        <br><br>
        <table class=\"table table-bordered\" id=\"myTable\">

            <th>Nom Agee</th>
            <th>Prenom Agee</th>
            <th>CIN</th>
            <th>Nom Du Medicament</th>
            <th>Description Du traitement</th>
            <th>Durée du traitment (jour)</th>
            <th>Dejeuner</th>
            <th>Petit-Dejeuner</th>
            <th>Diner</th>


            ";
        // line 147
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["plannings"] ?? $this->getContext($context, "plannings")));
        foreach ($context['_seq'] as $context["_key"] => $context["planning"]) {
            // line 148
            echo "                <tr>
                    ";
            // line 150
            echo "                    <td align=center>  ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["planning"], "idAgee", []), "nom", []), "html", null, true);
            echo " </td>
                    <td align=center>  ";
            // line 151
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["planning"], "idAgee", []), "prenom", []), "html", null, true);
            echo " </td>
                    <td align=center>  ";
            // line 152
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["planning"], "idAgee", []), "cin", []), "html", null, true);
            echo " </td>
                    <td align=center>  ";
            // line 153
            echo twig_escape_filter($this->env, $this->getAttribute($context["planning"], "nomMedicament", []), "html", null, true);
            echo " </td>
                    <td align=center>  ";
            // line 154
            echo twig_escape_filter($this->env, $this->getAttribute($context["planning"], "traitementDesc", []), "html", null, true);
            echo " </td>
                    <td align=center>  ";
            // line 155
            echo twig_escape_filter($this->env, $this->getAttribute($context["planning"], "dureeEnJourDeTraitement", []), "html", null, true);
            echo " </td>
                    <td align=center>  ";
            // line 156
            echo twig_escape_filter($this->env, $this->getAttribute($context["planning"], "dejeuner", []), "html", null, true);
            echo " </td>
                    <td align=center>  ";
            // line 157
            echo twig_escape_filter($this->env, $this->getAttribute($context["planning"], "petitDejeuner", []), "html", null, true);
            echo " </td>
                    <td align=center>  ";
            // line 158
            echo twig_escape_filter($this->env, $this->getAttribute($context["planning"], "diner", []), "html", null, true);
            echo " </td>


                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['planning'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 163
        echo "        </table>
    </div>

    <br><br><br><br><br><br><br><br><br><br><br><br><br>



";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "@GestionAgee/Agee/afficherTraitementAgee.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  273 => 163,  262 => 158,  258 => 157,  254 => 156,  250 => 155,  246 => 154,  242 => 153,  238 => 152,  234 => 151,  229 => 150,  226 => 148,  222 => 147,  80 => 7,  71 => 6,  61 => 4,  52 => 3,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"front/base.html.twig\" %}

{% block title %}
    <center>Planning des visites medicales</center>{% endblock %}

{% block content %}











    <!-- banner -->
    <div class=\"banner_top\">
        <div id=\"myCarousel\" class=\"carousel slide\" data-ride=\"carousel\">
            <!-- Indicators -->
            <ol class=\"carousel-indicators\">
                <li data-target=\"#myCarousel\" data-slide-to=\"0\" class=\"active\"></li>
                <li data-target=\"#myCarousel\" data-slide-to=\"1\" class=\"\"></li>
                <li data-target=\"#myCarousel\" data-slide-to=\"2\" class=\"\"></li>
                <li data-target=\"#myCarousel\" data-slide-to=\"3\" class=\"\"></li>
            </ol>
            <div class=\"carousel-inner\" role=\"listbox\">
                <div class=\"item active\">
                    <div class=\"container\">
                        <div class=\"carousel-caption\">
                            <h3>Change Begins With You.</h3>


                        </div>
                    </div>
                </div>
                <div class=\"item item2\">
                    <div class=\"container\">
                        <div class=\"carousel-caption\">
                            <h3>Make good things happen</h3>


                        </div>
                    </div>
                </div>
                <div class=\"item item3\">
                    <div class=\"container\">
                        <div class=\"carousel-caption\">
                            <h3>Change Begins With You.</h3>


                        </div>
                    </div>
                </div>
                <div class=\"item item4\">
                    <div class=\"container\">
                        <div class=\"carousel-caption\">


                        </div>
                    </div>
                </div>
            </div>
            <a class=\"left carousel-control\" href=\"#myCarousel\" role=\"button\" data-slide=\"prev\">
                <span class=\"fa fa-chevron-left\" aria-hidden=\"true\"></span>
                <span class=\"sr-only\">Previous</span>
            </a>
            <a class=\"right carousel-control\" href=\"#myCarousel\" role=\"button\" data-slide=\"next\">
                <span class=\"fa fa-chevron-right\" aria-hidden=\"true\"></span>
                <span class=\"sr-only\">Next</span>
            </a>
            <!-- The Modal -->
        </div>
    </div>











    <style>

        body {
            background-color: #F1F1F1;
        }

        #planning {
            background-color: #FAFAFA;
            width: 90%;
            margin-left: 5%;
            padding: 15px;
        }

        .table {
            border: 2px solid #629aa9 !important;

        }

        .table th {
            border: 2px solid #629aa9 !important;
        }

        .table td {
            border: 2px solid #629aa9 !important;
        }

    </style>









    <link rel=\"stylesheet\" type=\"text/css\" href=\"https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css\">


    <br><br><br><br><br><br><br><br><br><br><br>

    <br>
    <br>

    <div id=\"planning\" >
            <h1 style=\"text-align: center ; font-size: 30px\"  >Planning des Traitements médicaux</h1>
        <br><br>
        <table class=\"table table-bordered\" id=\"myTable\">

            <th>Nom Agee</th>
            <th>Prenom Agee</th>
            <th>CIN</th>
            <th>Nom Du Medicament</th>
            <th>Description Du traitement</th>
            <th>Durée du traitment (jour)</th>
            <th>Dejeuner</th>
            <th>Petit-Dejeuner</th>
            <th>Diner</th>


            {% for planning in plannings %}
                <tr>
                    {#<td align=center>  {{ planning.id }} </td>#}
                    <td align=center>  {{ planning.idAgee.nom }} </td>
                    <td align=center>  {{ planning.idAgee.prenom }} </td>
                    <td align=center>  {{ planning.idAgee.cin }} </td>
                    <td align=center>  {{ planning.nomMedicament }} </td>
                    <td align=center>  {{ planning.traitementDesc }} </td>
                    <td align=center>  {{ planning.dureeEnJourDeTraitement }} </td>
                    <td align=center>  {{ planning.dejeuner }} </td>
                    <td align=center>  {{ planning.petitDejeuner }} </td>
                    <td align=center>  {{ planning.diner }} </td>


                </tr>
            {% endfor %}
        </table>
    </div>

    <br><br><br><br><br><br><br><br><br><br><br><br><br>



{% endblock %}
", "@GestionAgee/Agee/afficherTraitementAgee.html.twig", "/var/www/html/3A11/happy_olds/src/GestionAgeeBundle/Resources/views/Agee/afficherTraitementAgee.html.twig");
    }
}
