<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @FOSUser/Registration/register_content.html.twig */
class __TwigTemplate_d0a2195c7edf9ff0fea6dd7a03c6e0b62418836349fe8f11aac1e8f3bf77087d extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register_content.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register_content.html.twig"));

        // line 2
        echo "<br><br><br><br><br><br>
<div class=\"art-bothside\">
    <div class=\"sap_tabs\">
        <div id=\"horizontalTab\">
            <ul class=\"resp-tabs-list\">
                <li class=\"resp-tab-item\"><span>Register</span></li>
            </ul>
            <div class=\"clearfix\"></div>
            <div class=\"resp-tabs-container\">
                <div class=\"tab-1 resp-tab-content\">
                    <div class=\"swm-right-w3ls\">
                        ";
        // line 13
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start', ["attr" => ["id" => "registerForm", "autocomplete" => "off"]]);
        echo "
                        <div class=\"form-left-to-w3l\">
                            <i class=\"fa fa-user icon\"></i> ";
        // line 15
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "nom", []), 'row');
        echo "
                            <div id=\"block-erreur-nom\" class=\"hide\" style=\"color: red; margin-bottom: 15px;\"></div>
                            <i class=\"fa fa-user icon\"></i>";
        // line 17
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "prenom", []), 'row');
        echo "
                            <div id=\"block-erreur-prenom\" class=\"hide\" style=\"color: red; margin-bottom: 15px;\"></div>
                            <i class=\"fa fa-envelope icon\"></i>";
        // line 19
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "email", []), 'row');
        echo "<br>
                            <div id=\"block-erreur-email\" class=\"hide\" style=\"color: red; margin-bottom: 15px; margin-top: -15px;\"></div>
                            ";
        // line 21
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "username", []), 'row');
        echo "<br>
                            <div id=\"block-erreur-username\" class=\"hide\" style=\"color: red; margin-bottom: 15px; margin-top: -15px;\"></div>
                            ";
        // line 23
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "cin", []), 'row');
        echo "<br>
                            <div id=\"block-erreur-cin\" class=\"hide\" style=\"color: red; margin-bottom: 15px; margin-top: -15px;\"></div>
                            ";
        // line 25
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "datenaissance", []), 'row');
        echo "<br>
                            <div id=\"block-erreur-date\" class=\"hide\" style=\"color: red; margin-bottom: 15px;\"></div>
                            ";
        // line 27
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "sexe", []), 'row');
        echo "<br>
                            <div id=\"block-erreur-sexe\" class=\"hide\" style=\"color: red; margin-bottom: 15px;\"></div>
                            ";
        // line 29
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "region", []), 'row');
        echo "<br>
                            <div id=\"block-erreur-region\" class=\"hide\" style=\"color: red; margin-bottom: 15px;\"></div>
                            ";
        // line 31
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "adresse", []), 'row');
        echo "<br>
                            <div id=\"block-erreur-adresse\" class=\"hide\" style=\"color: red; margin-bottom: 15px;\"></div>
                            ";
        // line 33
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "telephone", []), 'row');
        echo "<br>
                            <div id=\"block-erreur-telephone\" class=\"hide\" style=\"color: red; margin-bottom: 15px;\"></div>
                            ";
        // line 35
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "rib", []), 'row');
        echo "<br>
                            <div id=\"block-erreur-rib\" class=\"hide\" style=\"color: red; margin-bottom: 15px;\"></div>
                            ";
        // line 37
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "roles", []), 'row');
        echo "<br>
                            <div id=\"block-erreur-role\" class=\"hide\" style=\"color: red; margin-bottom: 15px;\"></div>
                            ";
        // line 39
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "plainPassword", []), 'row');
        echo "
                            <div id=\"block-erreur-password\" class=\"hide\" style=\"color: red; margin-bottom: 15px;\"></div>
                            <div id=\"block-erreur-password-confirm\" class=\"hide\" style=\"color: red; margin-bottom: 15px;\"></div>
                        </div>
                        <div class=\"btnn\">
                            <button id=\"submitRegister\" style=\"width: 36%; margin-left: 32%; margin-bottom: 10px;\"
                                    class=\"btn btn-danger btn-block\" type=\"button\"
                                    value=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.submit", [], "FOSUserBundle"), "html", null, true);
        echo "\">Register
                            </button>
                        </div>
                        ";
        // line 49
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


<br><br><br><br><br><br><br>

";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/register_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  130 => 49,  124 => 46,  114 => 39,  109 => 37,  104 => 35,  99 => 33,  94 => 31,  89 => 29,  84 => 27,  79 => 25,  74 => 23,  69 => 21,  64 => 19,  59 => 17,  54 => 15,  49 => 13,  36 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% trans_default_domain 'FOSUserBundle' %}
<br><br><br><br><br><br>
<div class=\"art-bothside\">
    <div class=\"sap_tabs\">
        <div id=\"horizontalTab\">
            <ul class=\"resp-tabs-list\">
                <li class=\"resp-tab-item\"><span>Register</span></li>
            </ul>
            <div class=\"clearfix\"></div>
            <div class=\"resp-tabs-container\">
                <div class=\"tab-1 resp-tab-content\">
                    <div class=\"swm-right-w3ls\">
                        {{ form_start(form, {'attr': {'id': 'registerForm', 'autocomplete' : 'off'}}) }}
                        <div class=\"form-left-to-w3l\">
                            <i class=\"fa fa-user icon\"></i> {{ form_row(form.nom) }}
                            <div id=\"block-erreur-nom\" class=\"hide\" style=\"color: red; margin-bottom: 15px;\"></div>
                            <i class=\"fa fa-user icon\"></i>{{ form_row(form.prenom) }}
                            <div id=\"block-erreur-prenom\" class=\"hide\" style=\"color: red; margin-bottom: 15px;\"></div>
                            <i class=\"fa fa-envelope icon\"></i>{{ form_row(form.email) }}<br>
                            <div id=\"block-erreur-email\" class=\"hide\" style=\"color: red; margin-bottom: 15px; margin-top: -15px;\"></div>
                            {{ form_row(form.username) }}<br>
                            <div id=\"block-erreur-username\" class=\"hide\" style=\"color: red; margin-bottom: 15px; margin-top: -15px;\"></div>
                            {{ form_row(form.cin) }}<br>
                            <div id=\"block-erreur-cin\" class=\"hide\" style=\"color: red; margin-bottom: 15px; margin-top: -15px;\"></div>
                            {{ form_row(form.datenaissance) }}<br>
                            <div id=\"block-erreur-date\" class=\"hide\" style=\"color: red; margin-bottom: 15px;\"></div>
                            {{ form_row(form.sexe) }}<br>
                            <div id=\"block-erreur-sexe\" class=\"hide\" style=\"color: red; margin-bottom: 15px;\"></div>
                            {{ form_row(form.region) }}<br>
                            <div id=\"block-erreur-region\" class=\"hide\" style=\"color: red; margin-bottom: 15px;\"></div>
                            {{ form_row(form.adresse) }}<br>
                            <div id=\"block-erreur-adresse\" class=\"hide\" style=\"color: red; margin-bottom: 15px;\"></div>
                            {{ form_row(form.telephone) }}<br>
                            <div id=\"block-erreur-telephone\" class=\"hide\" style=\"color: red; margin-bottom: 15px;\"></div>
                            {{ form_row(form.rib) }}<br>
                            <div id=\"block-erreur-rib\" class=\"hide\" style=\"color: red; margin-bottom: 15px;\"></div>
                            {{ form_row(form.roles) }}<br>
                            <div id=\"block-erreur-role\" class=\"hide\" style=\"color: red; margin-bottom: 15px;\"></div>
                            {{ form_row(form.plainPassword) }}
                            <div id=\"block-erreur-password\" class=\"hide\" style=\"color: red; margin-bottom: 15px;\"></div>
                            <div id=\"block-erreur-password-confirm\" class=\"hide\" style=\"color: red; margin-bottom: 15px;\"></div>
                        </div>
                        <div class=\"btnn\">
                            <button id=\"submitRegister\" style=\"width: 36%; margin-left: 32%; margin-bottom: 10px;\"
                                    class=\"btn btn-danger btn-block\" type=\"button\"
                                    value=\"{{ 'registration.submit'|trans }}\">Register
                            </button>
                        </div>
                        {{ form_end(form) }}
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


<br><br><br><br><br><br><br>

", "@FOSUser/Registration/register_content.html.twig", "/var/www/html/3A11/happy_olds/app/Resources/FOSUserBundle/views/Registration/register_content.html.twig");
    }
}
