<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* back/_scripts.html.twig */
class __TwigTemplate_77b2656539e22fe69e4664b68cb19e8afa68776b704a0bee65cd41ef147c10ab extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "back/_scripts.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "back/_scripts.html.twig"));

        // line 1
        echo "<!-- new added graphs chart js-->

<script src=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("jsB/Chart.bundle.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("jsB/utils.js"), "html", null, true);
        echo "\"></script>

<script>
\tvar MONTHS = [\"January\", \"February\", \"March\", \"April\", \"May\", \"June\", \"July\", \"August\", \"September\", \"October\", \"November\", \"December\"];
\tvar color = Chart.helpers.color;
\tvar barChartData = {
\t\tlabels: [\"January\", \"February\", \"March\", \"April\", \"May\", \"June\", \"July\"],
\t\tdatasets: [{
\t\t\tlabel: 'Dataset 1',
\t\t\tbackgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
\t\t\tborderColor: window.chartColors.red,
\t\t\tborderWidth: 1,
\t\t\tdata: [
\t\t\t\trandomScalingFactor(),
\t\t\t\trandomScalingFactor(),
\t\t\t\trandomScalingFactor(),
\t\t\t\trandomScalingFactor(),
\t\t\t\trandomScalingFactor(),
\t\t\t\trandomScalingFactor(),
\t\t\t\trandomScalingFactor()
\t\t\t]
\t\t}, {
\t\t\tlabel: 'Dataset 2',
\t\t\tbackgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
\t\t\tborderColor: window.chartColors.blue,
\t\t\tborderWidth: 1,
\t\t\tdata: [
\t\t\t\trandomScalingFactor(),
\t\t\t\trandomScalingFactor(),
\t\t\t\trandomScalingFactor(),
\t\t\t\trandomScalingFactor(),
\t\t\t\trandomScalingFactor(),
\t\t\t\trandomScalingFactor(),
\t\t\t\trandomScalingFactor()
\t\t\t]
\t\t}]

\t};

\twindow.onload = function() {
\t\tvar ctx = document.getElementById(\"canvas\").getContext(\"2d\");
\t\twindow.myBar = new Chart(ctx, {
\t\t\ttype: 'bar',
\t\t\tdata: barChartData,
\t\t\toptions: {
\t\t\t\tresponsive: true,
\t\t\t\tlegend: {
\t\t\t\t\tposition: 'top',
\t\t\t\t},
\t\t\t\ttitle: {
\t\t\t\t\tdisplay: true,
\t\t\t\t\ttext: 'Chart.js Bar Chart'
\t\t\t\t}
\t\t\t}
\t\t});

\t};

\tdocument.getElementById('randomizeData').addEventListener('click', function() {
\t\tvar zero = Math.random() < 0.2 ? true : false;
\t\tbarChartData.datasets.forEach(function(dataset) {
\t\t\tdataset.data = dataset.data.map(function() {
\t\t\t\treturn zero ? 0.0 : randomScalingFactor();
\t\t\t});

\t\t});
\t\twindow.myBar.update();
\t});

\tvar colorNames = Object.keys(window.chartColors);
\tdocument.getElementById('addDataset').addEventListener('click', function() {
\t\tvar colorName = colorNames[barChartData.datasets.length % colorNames.length];;
\t\tvar dsColor = window.chartColors[colorName];
\t\tvar newDataset = {
\t\t\tlabel: 'Dataset ' + barChartData.datasets.length,
\t\t\tbackgroundColor: color(dsColor).alpha(0.5).rgbString(),
\t\t\tborderColor: dsColor,
\t\t\tborderWidth: 1,
\t\t\tdata: []
\t\t};

\t\tfor (var index = 0; index < barChartData.labels.length; ++index) {
\t\t\tnewDataset.data.push(randomScalingFactor());
\t\t}

\t\tbarChartData.datasets.push(newDataset);
\t\twindow.myBar.update();
\t});

\tdocument.getElementById('addData').addEventListener('click', function() {
\t\tif (barChartData.datasets.length > 0) {
\t\t\tvar month = MONTHS[barChartData.labels.length % MONTHS.length];
\t\t\tbarChartData.labels.push(month);

\t\t\tfor (var index = 0; index < barChartData.datasets.length; ++index) {
\t\t\t\t//window.myBar.addData(randomScalingFactor(), index);
\t\t\t\tbarChartData.datasets[index].data.push(randomScalingFactor());
\t\t\t}

\t\t\twindow.myBar.update();
\t\t}
\t});

\tdocument.getElementById('removeDataset').addEventListener('click', function() {
\t\tbarChartData.datasets.splice(0, 1);
\t\twindow.myBar.update();
\t});

\tdocument.getElementById('removeData').addEventListener('click', function() {
\t\tbarChartData.labels.splice(-1, 1); // remove the label first

\t\tbarChartData.datasets.forEach(function(dataset, datasetIndex) {
\t\t\tdataset.data.pop();
\t\t});

\t\twindow.myBar.update();
\t});
</script>
<!-- new added graphs chart js-->

<!-- Classie --><!-- for toggle left push menu script -->
<script src=\"";
        // line 125
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("jsB/classie.js"), "html", null, true);
        echo "\"></script>
<script>
\tvar menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
\t\t\tshowLeftPush = document.getElementById( 'showLeftPush' ),
\t\t\tbody = document.body;

\tshowLeftPush.onclick = function() {
\t\tclassie.toggle( this, 'active' );
\t\tclassie.toggle( body, 'cbp-spmenu-push-toright' );
\t\tclassie.toggle( menuLeft, 'cbp-spmenu-open' );
\t\tdisableOther( 'showLeftPush' );
\t};


\tfunction disableOther( button ) {
\t\tif( button !== 'showLeftPush' ) {
\t\t\tclassie.toggle( showLeftPush, 'disabled' );
\t\t}
\t}
</script>
<!-- //Classie --><!-- //for toggle left push menu script -->

<!--scrolling js-->
<script src=\"";
        // line 148
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("jsB/jquery.nicescroll.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 149
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("jsB/scripts.js"), "html", null, true);
        echo "\"></script>
<!--//scrolling js-->

<!-- side nav js -->
<script src=\"";
        // line 153
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("jsB/SidebarNav.min.js"), "html", null, true);
        echo "\" type='text/javascript'></script>
<script>
\t\$('.sidebar-menu').SidebarNav()
</script>
<!-- //side nav js -->

<!-- for index page weekly sales java script -->
<script src=\"";
        // line 160
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("jsB/SimpleChart.js"), "html", null, true);
        echo "\"></script>
<script>
\tvar graphdata1 = {
\t\tlinecolor: \"#CCA300\",
\t\ttitle: \"Monday\",
\t\tvalues: [
\t\t\t{ X: \"6:00\", Y: 10.00 },
\t\t\t{ X: \"7:00\", Y: 20.00 },
\t\t\t{ X: \"8:00\", Y: 40.00 },
\t\t\t{ X: \"9:00\", Y: 34.00 },
\t\t\t{ X: \"10:00\", Y: 40.25 },
\t\t\t{ X: \"11:00\", Y: 28.56 },
\t\t\t{ X: \"12:00\", Y: 18.57 },
\t\t\t{ X: \"13:00\", Y: 34.00 },
\t\t\t{ X: \"14:00\", Y: 40.89 },
\t\t\t{ X: \"15:00\", Y: 12.57 },
\t\t\t{ X: \"16:00\", Y: 28.24 },
\t\t\t{ X: \"17:00\", Y: 18.00 },
\t\t\t{ X: \"18:00\", Y: 34.24 },
\t\t\t{ X: \"19:00\", Y: 40.58 },
\t\t\t{ X: \"20:00\", Y: 12.54 },
\t\t\t{ X: \"21:00\", Y: 28.00 },
\t\t\t{ X: \"22:00\", Y: 18.00 },
\t\t\t{ X: \"23:00\", Y: 34.89 },
\t\t\t{ X: \"0:00\", Y: 40.26 },
\t\t\t{ X: \"1:00\", Y: 28.89 },
\t\t\t{ X: \"2:00\", Y: 18.87 },
\t\t\t{ X: \"3:00\", Y: 34.00 },
\t\t\t{ X: \"4:00\", Y: 40.00 }
\t\t]
\t};
\tvar graphdata2 = {
\t\tlinecolor: \"#00CC66\",
\t\ttitle: \"Tuesday\",
\t\tvalues: [
\t\t\t{ X: \"6:00\", Y: 100.00 },
\t\t\t{ X: \"7:00\", Y: 120.00 },
\t\t\t{ X: \"8:00\", Y: 140.00 },
\t\t\t{ X: \"9:00\", Y: 134.00 },
\t\t\t{ X: \"10:00\", Y: 140.25 },
\t\t\t{ X: \"11:00\", Y: 128.56 },
\t\t\t{ X: \"12:00\", Y: 118.57 },
\t\t\t{ X: \"13:00\", Y: 134.00 },
\t\t\t{ X: \"14:00\", Y: 140.89 },
\t\t\t{ X: \"15:00\", Y: 112.57 },
\t\t\t{ X: \"16:00\", Y: 128.24 },
\t\t\t{ X: \"17:00\", Y: 118.00 },
\t\t\t{ X: \"18:00\", Y: 134.24 },
\t\t\t{ X: \"19:00\", Y: 140.58 },
\t\t\t{ X: \"20:00\", Y: 112.54 },
\t\t\t{ X: \"21:00\", Y: 128.00 },
\t\t\t{ X: \"22:00\", Y: 118.00 },
\t\t\t{ X: \"23:00\", Y: 134.89 },
\t\t\t{ X: \"0:00\", Y: 140.26 },
\t\t\t{ X: \"1:00\", Y: 128.89 },
\t\t\t{ X: \"2:00\", Y: 118.87 },
\t\t\t{ X: \"3:00\", Y: 134.00 },
\t\t\t{ X: \"4:00\", Y: 180.00 }
\t\t]
\t};
\tvar graphdata3 = {
\t\tlinecolor: \"#FF99CC\",
\t\ttitle: \"Wednesday\",
\t\tvalues: [
\t\t\t{ X: \"6:00\", Y: 230.00 },
\t\t\t{ X: \"7:00\", Y: 210.00 },
\t\t\t{ X: \"8:00\", Y: 214.00 },
\t\t\t{ X: \"9:00\", Y: 234.00 },
\t\t\t{ X: \"10:00\", Y: 247.25 },
\t\t\t{ X: \"11:00\", Y: 218.56 },
\t\t\t{ X: \"12:00\", Y: 268.57 },
\t\t\t{ X: \"13:00\", Y: 274.00 },
\t\t\t{ X: \"14:00\", Y: 280.89 },
\t\t\t{ X: \"15:00\", Y: 242.57 },
\t\t\t{ X: \"16:00\", Y: 298.24 },
\t\t\t{ X: \"17:00\", Y: 208.00 },
\t\t\t{ X: \"18:00\", Y: 214.24 },
\t\t\t{ X: \"19:00\", Y: 214.58 },
\t\t\t{ X: \"20:00\", Y: 211.54 },
\t\t\t{ X: \"21:00\", Y: 248.00 },
\t\t\t{ X: \"22:00\", Y: 258.00 },
\t\t\t{ X: \"23:00\", Y: 234.89 },
\t\t\t{ X: \"0:00\", Y: 210.26 },
\t\t\t{ X: \"1:00\", Y: 248.89 },
\t\t\t{ X: \"2:00\", Y: 238.87 },
\t\t\t{ X: \"3:00\", Y: 264.00 },
\t\t\t{ X: \"4:00\", Y: 270.00 }
\t\t]
\t};
\tvar graphdata4 = {
\t\tlinecolor: \"Random\",
\t\ttitle: \"Thursday\",
\t\tvalues: [
\t\t\t{ X: \"6:00\", Y: 300.00 },
\t\t\t{ X: \"7:00\", Y: 410.98 },
\t\t\t{ X: \"8:00\", Y: 310.00 },
\t\t\t{ X: \"9:00\", Y: 314.00 },
\t\t\t{ X: \"10:00\", Y: 310.25 },
\t\t\t{ X: \"11:00\", Y: 318.56 },
\t\t\t{ X: \"12:00\", Y: 318.57 },
\t\t\t{ X: \"13:00\", Y: 314.00 },
\t\t\t{ X: \"14:00\", Y: 310.89 },
\t\t\t{ X: \"15:00\", Y: 512.57 },
\t\t\t{ X: \"16:00\", Y: 318.24 },
\t\t\t{ X: \"17:00\", Y: 318.00 },
\t\t\t{ X: \"18:00\", Y: 314.24 },
\t\t\t{ X: \"19:00\", Y: 310.58 },
\t\t\t{ X: \"20:00\", Y: 312.54 },
\t\t\t{ X: \"21:00\", Y: 318.00 },
\t\t\t{ X: \"22:00\", Y: 318.00 },
\t\t\t{ X: \"23:00\", Y: 314.89 },
\t\t\t{ X: \"0:00\", Y: 310.26 },
\t\t\t{ X: \"1:00\", Y: 318.89 },
\t\t\t{ X: \"2:00\", Y: 518.87 },
\t\t\t{ X: \"3:00\", Y: 314.00 },
\t\t\t{ X: \"4:00\", Y: 310.00 }
\t\t]
\t};
\tvar Piedata = {
\t\tlinecolor: \"Random\",
\t\ttitle: \"Profit\",
\t\tvalues: [
\t\t\t{ X: \"Monday\", Y: 50.00 },
\t\t\t{ X: \"Tuesday\", Y: 110.98 },
\t\t\t{ X: \"Wednesday\", Y: 70.00 },
\t\t\t{ X: \"Thursday\", Y: 204.00 },
\t\t\t{ X: \"Friday\", Y: 80.25 },
\t\t\t{ X: \"Saturday\", Y: 38.56 },
\t\t\t{ X: \"Sunday\", Y: 98.57 }
\t\t]
\t};
\t\$(function () {
\t\t\$(\"#Bargraph\").SimpleChart({
\t\t\tChartType: \"Bar\",
\t\t\ttoolwidth: \"50\",
\t\t\ttoolheight: \"25\",
\t\t\taxiscolor: \"#E6E6E6\",
\t\t\ttextcolor: \"#6E6E6E\",
\t\t\tshowlegends: true,
\t\t\tdata: [graphdata4, graphdata3, graphdata2, graphdata1],
\t\t\tlegendsize: \"140\",
\t\t\tlegendposition: 'bottom',
\t\t\txaxislabel: 'Hours',
\t\t\ttitle: 'Weekly Profit',
\t\t\tyaxislabel: 'Profit in \$'
\t\t});
\t\t\$(\"#sltchartype\").on('change', function () {
\t\t\t\$(\"#Bargraph\").SimpleChart('ChartType', \$(this).val());
\t\t\t\$(\"#Bargraph\").SimpleChart('reload', 'true');
\t\t});
\t\t\$(\"#Hybridgraph\").SimpleChart({
\t\t\tChartType: \"Hybrid\",
\t\t\ttoolwidth: \"50\",
\t\t\ttoolheight: \"25\",
\t\t\taxiscolor: \"#E6E6E6\",
\t\t\ttextcolor: \"#6E6E6E\",
\t\t\tshowlegends: true,
\t\t\tdata: [graphdata4],
\t\t\tlegendsize: \"140\",
\t\t\tlegendposition: 'bottom',
\t\t\txaxislabel: 'Hours',
\t\t\ttitle: 'Weekly Profit',
\t\t\tyaxislabel: 'Profit in \$'
\t\t});
\t\t\$(\"#Linegraph\").SimpleChart({
\t\t\tChartType: \"Line\",
\t\t\ttoolwidth: \"50\",
\t\t\ttoolheight: \"25\",
\t\t\taxiscolor: \"#E6E6E6\",
\t\t\ttextcolor: \"#6E6E6E\",
\t\t\tshowlegends: false,
\t\t\tdata: [graphdata4, graphdata3, graphdata2, graphdata1],
\t\t\tlegendsize: \"140\",
\t\t\tlegendposition: 'bottom',
\t\t\txaxislabel: 'Hours',
\t\t\ttitle: 'Weekly Profit',
\t\t\tyaxislabel: 'Profit in \$'
\t\t});
\t\t\$(\"#Areagraph\").SimpleChart({
\t\t\tChartType: \"Area\",
\t\t\ttoolwidth: \"50\",
\t\t\ttoolheight: \"25\",
\t\t\taxiscolor: \"#E6E6E6\",
\t\t\ttextcolor: \"#6E6E6E\",
\t\t\tshowlegends: true,
\t\t\tdata: [graphdata4, graphdata3, graphdata2, graphdata1],
\t\t\tlegendsize: \"140\",
\t\t\tlegendposition: 'bottom',
\t\t\txaxislabel: 'Hours',
\t\t\ttitle: 'Weekly Profit',
\t\t\tyaxislabel: 'Profit in \$'
\t\t});
\t\t\$(\"#Scatterredgraph\").SimpleChart({
\t\t\tChartType: \"Scattered\",
\t\t\ttoolwidth: \"50\",
\t\t\ttoolheight: \"25\",
\t\t\taxiscolor: \"#E6E6E6\",
\t\t\ttextcolor: \"#6E6E6E\",
\t\t\tshowlegends: true,
\t\t\tdata: [graphdata4, graphdata3, graphdata2, graphdata1],
\t\t\tlegendsize: \"140\",
\t\t\tlegendposition: 'bottom',
\t\t\txaxislabel: 'Hours',
\t\t\ttitle: 'Weekly Profit',
\t\t\tyaxislabel: 'Profit in \$'
\t\t});
\t\t\$(\"#Piegraph\").SimpleChart({
\t\t\tChartType: \"Pie\",
\t\t\ttoolwidth: \"50\",
\t\t\ttoolheight: \"25\",
\t\t\taxiscolor: \"#E6E6E6\",
\t\t\ttextcolor: \"#6E6E6E\",
\t\t\tshowlegends: true,
\t\t\tshowpielables: true,
\t\t\tdata: [Piedata],
\t\t\tlegendsize: \"250\",
\t\t\tlegendposition: 'right',
\t\t\txaxislabel: 'Hours',
\t\t\ttitle: 'Weekly Profit',
\t\t\tyaxislabel: 'Profit in \$'
\t\t});

\t\t\$(\"#Stackedbargraph\").SimpleChart({
\t\t\tChartType: \"Stacked\",
\t\t\ttoolwidth: \"50\",
\t\t\ttoolheight: \"25\",
\t\t\taxiscolor: \"#E6E6E6\",
\t\t\ttextcolor: \"#6E6E6E\",
\t\t\tshowlegends: true,
\t\t\tdata: [graphdata3, graphdata2, graphdata1],
\t\t\tlegendsize: \"140\",
\t\t\tlegendposition: 'bottom',
\t\t\txaxislabel: 'Hours',
\t\t\ttitle: 'Weekly Profit',
\t\t\tyaxislabel: 'Profit in \$'
\t\t});

\t\t\$(\"#StackedHybridbargraph\").SimpleChart({
\t\t\tChartType: \"StackedHybrid\",
\t\t\ttoolwidth: \"50\",
\t\t\ttoolheight: \"25\",
\t\t\taxiscolor: \"#E6E6E6\",
\t\t\ttextcolor: \"#6E6E6E\",
\t\t\tshowlegends: true,
\t\t\tdata: [graphdata3, graphdata2, graphdata1],
\t\t\tlegendsize: \"140\",
\t\t\tlegendposition: 'bottom',
\t\t\txaxislabel: 'Hours',
\t\t\ttitle: 'Weekly Profit',
\t\t\tyaxislabel: 'Profit in \$'
\t\t});
\t});

</script>
<!-- //for index page weekly sales java script -->


<!-- Bootstrap Core JavaScript -->
<script src=\"";
        // line 418
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("jsB/bootstrap.js"), "html", null, true);
        echo "\"> </script>
<!-- //Bootstrap Core JavaScript -->



";
        // line 424
        echo "
<script type=\"text/javascript\" src=\"";
        // line 425
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/fullcalendar/js/fullcalendar/lib/jquery.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 426
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/fullcalendar/js/fullcalendar/lib/moment.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 427
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/fullcalendar/js/fullcalendar/fullcalendar.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 428
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/fullcalendar/js/fullcalendar/fullcalendar.default-settings.js"), "html", null, true);
        echo "\"></script>

";
        // line 431
        echo "


<script type=\"text/javascript\" charset=\"utf8\" src=\"https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js\"></script>

<script src=\"";
        // line 436
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/rechercheTable/table.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "back/_scripts.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  511 => 436,  504 => 431,  499 => 428,  495 => 427,  491 => 426,  487 => 425,  484 => 424,  476 => 418,  215 => 160,  205 => 153,  198 => 149,  194 => 148,  168 => 125,  44 => 4,  40 => 3,  36 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<!-- new added graphs chart js-->

<script src=\"{{ asset('jsB/Chart.bundle.js') }}\"></script>
<script src=\"{{ asset('jsB/utils.js') }}\"></script>

<script>
\tvar MONTHS = [\"January\", \"February\", \"March\", \"April\", \"May\", \"June\", \"July\", \"August\", \"September\", \"October\", \"November\", \"December\"];
\tvar color = Chart.helpers.color;
\tvar barChartData = {
\t\tlabels: [\"January\", \"February\", \"March\", \"April\", \"May\", \"June\", \"July\"],
\t\tdatasets: [{
\t\t\tlabel: 'Dataset 1',
\t\t\tbackgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
\t\t\tborderColor: window.chartColors.red,
\t\t\tborderWidth: 1,
\t\t\tdata: [
\t\t\t\trandomScalingFactor(),
\t\t\t\trandomScalingFactor(),
\t\t\t\trandomScalingFactor(),
\t\t\t\trandomScalingFactor(),
\t\t\t\trandomScalingFactor(),
\t\t\t\trandomScalingFactor(),
\t\t\t\trandomScalingFactor()
\t\t\t]
\t\t}, {
\t\t\tlabel: 'Dataset 2',
\t\t\tbackgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
\t\t\tborderColor: window.chartColors.blue,
\t\t\tborderWidth: 1,
\t\t\tdata: [
\t\t\t\trandomScalingFactor(),
\t\t\t\trandomScalingFactor(),
\t\t\t\trandomScalingFactor(),
\t\t\t\trandomScalingFactor(),
\t\t\t\trandomScalingFactor(),
\t\t\t\trandomScalingFactor(),
\t\t\t\trandomScalingFactor()
\t\t\t]
\t\t}]

\t};

\twindow.onload = function() {
\t\tvar ctx = document.getElementById(\"canvas\").getContext(\"2d\");
\t\twindow.myBar = new Chart(ctx, {
\t\t\ttype: 'bar',
\t\t\tdata: barChartData,
\t\t\toptions: {
\t\t\t\tresponsive: true,
\t\t\t\tlegend: {
\t\t\t\t\tposition: 'top',
\t\t\t\t},
\t\t\t\ttitle: {
\t\t\t\t\tdisplay: true,
\t\t\t\t\ttext: 'Chart.js Bar Chart'
\t\t\t\t}
\t\t\t}
\t\t});

\t};

\tdocument.getElementById('randomizeData').addEventListener('click', function() {
\t\tvar zero = Math.random() < 0.2 ? true : false;
\t\tbarChartData.datasets.forEach(function(dataset) {
\t\t\tdataset.data = dataset.data.map(function() {
\t\t\t\treturn zero ? 0.0 : randomScalingFactor();
\t\t\t});

\t\t});
\t\twindow.myBar.update();
\t});

\tvar colorNames = Object.keys(window.chartColors);
\tdocument.getElementById('addDataset').addEventListener('click', function() {
\t\tvar colorName = colorNames[barChartData.datasets.length % colorNames.length];;
\t\tvar dsColor = window.chartColors[colorName];
\t\tvar newDataset = {
\t\t\tlabel: 'Dataset ' + barChartData.datasets.length,
\t\t\tbackgroundColor: color(dsColor).alpha(0.5).rgbString(),
\t\t\tborderColor: dsColor,
\t\t\tborderWidth: 1,
\t\t\tdata: []
\t\t};

\t\tfor (var index = 0; index < barChartData.labels.length; ++index) {
\t\t\tnewDataset.data.push(randomScalingFactor());
\t\t}

\t\tbarChartData.datasets.push(newDataset);
\t\twindow.myBar.update();
\t});

\tdocument.getElementById('addData').addEventListener('click', function() {
\t\tif (barChartData.datasets.length > 0) {
\t\t\tvar month = MONTHS[barChartData.labels.length % MONTHS.length];
\t\t\tbarChartData.labels.push(month);

\t\t\tfor (var index = 0; index < barChartData.datasets.length; ++index) {
\t\t\t\t//window.myBar.addData(randomScalingFactor(), index);
\t\t\t\tbarChartData.datasets[index].data.push(randomScalingFactor());
\t\t\t}

\t\t\twindow.myBar.update();
\t\t}
\t});

\tdocument.getElementById('removeDataset').addEventListener('click', function() {
\t\tbarChartData.datasets.splice(0, 1);
\t\twindow.myBar.update();
\t});

\tdocument.getElementById('removeData').addEventListener('click', function() {
\t\tbarChartData.labels.splice(-1, 1); // remove the label first

\t\tbarChartData.datasets.forEach(function(dataset, datasetIndex) {
\t\t\tdataset.data.pop();
\t\t});

\t\twindow.myBar.update();
\t});
</script>
<!-- new added graphs chart js-->

<!-- Classie --><!-- for toggle left push menu script -->
<script src=\"{{ asset('jsB/classie.js') }}\"></script>
<script>
\tvar menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
\t\t\tshowLeftPush = document.getElementById( 'showLeftPush' ),
\t\t\tbody = document.body;

\tshowLeftPush.onclick = function() {
\t\tclassie.toggle( this, 'active' );
\t\tclassie.toggle( body, 'cbp-spmenu-push-toright' );
\t\tclassie.toggle( menuLeft, 'cbp-spmenu-open' );
\t\tdisableOther( 'showLeftPush' );
\t};


\tfunction disableOther( button ) {
\t\tif( button !== 'showLeftPush' ) {
\t\t\tclassie.toggle( showLeftPush, 'disabled' );
\t\t}
\t}
</script>
<!-- //Classie --><!-- //for toggle left push menu script -->

<!--scrolling js-->
<script src=\"{{ asset('jsB/jquery.nicescroll.js') }}\"></script>
<script src=\"{{ asset('jsB/scripts.js') }}\"></script>
<!--//scrolling js-->

<!-- side nav js -->
<script src=\"{{ asset('jsB/SidebarNav.min.js') }}\" type='text/javascript'></script>
<script>
\t\$('.sidebar-menu').SidebarNav()
</script>
<!-- //side nav js -->

<!-- for index page weekly sales java script -->
<script src=\"{{ asset('jsB/SimpleChart.js') }}\"></script>
<script>
\tvar graphdata1 = {
\t\tlinecolor: \"#CCA300\",
\t\ttitle: \"Monday\",
\t\tvalues: [
\t\t\t{ X: \"6:00\", Y: 10.00 },
\t\t\t{ X: \"7:00\", Y: 20.00 },
\t\t\t{ X: \"8:00\", Y: 40.00 },
\t\t\t{ X: \"9:00\", Y: 34.00 },
\t\t\t{ X: \"10:00\", Y: 40.25 },
\t\t\t{ X: \"11:00\", Y: 28.56 },
\t\t\t{ X: \"12:00\", Y: 18.57 },
\t\t\t{ X: \"13:00\", Y: 34.00 },
\t\t\t{ X: \"14:00\", Y: 40.89 },
\t\t\t{ X: \"15:00\", Y: 12.57 },
\t\t\t{ X: \"16:00\", Y: 28.24 },
\t\t\t{ X: \"17:00\", Y: 18.00 },
\t\t\t{ X: \"18:00\", Y: 34.24 },
\t\t\t{ X: \"19:00\", Y: 40.58 },
\t\t\t{ X: \"20:00\", Y: 12.54 },
\t\t\t{ X: \"21:00\", Y: 28.00 },
\t\t\t{ X: \"22:00\", Y: 18.00 },
\t\t\t{ X: \"23:00\", Y: 34.89 },
\t\t\t{ X: \"0:00\", Y: 40.26 },
\t\t\t{ X: \"1:00\", Y: 28.89 },
\t\t\t{ X: \"2:00\", Y: 18.87 },
\t\t\t{ X: \"3:00\", Y: 34.00 },
\t\t\t{ X: \"4:00\", Y: 40.00 }
\t\t]
\t};
\tvar graphdata2 = {
\t\tlinecolor: \"#00CC66\",
\t\ttitle: \"Tuesday\",
\t\tvalues: [
\t\t\t{ X: \"6:00\", Y: 100.00 },
\t\t\t{ X: \"7:00\", Y: 120.00 },
\t\t\t{ X: \"8:00\", Y: 140.00 },
\t\t\t{ X: \"9:00\", Y: 134.00 },
\t\t\t{ X: \"10:00\", Y: 140.25 },
\t\t\t{ X: \"11:00\", Y: 128.56 },
\t\t\t{ X: \"12:00\", Y: 118.57 },
\t\t\t{ X: \"13:00\", Y: 134.00 },
\t\t\t{ X: \"14:00\", Y: 140.89 },
\t\t\t{ X: \"15:00\", Y: 112.57 },
\t\t\t{ X: \"16:00\", Y: 128.24 },
\t\t\t{ X: \"17:00\", Y: 118.00 },
\t\t\t{ X: \"18:00\", Y: 134.24 },
\t\t\t{ X: \"19:00\", Y: 140.58 },
\t\t\t{ X: \"20:00\", Y: 112.54 },
\t\t\t{ X: \"21:00\", Y: 128.00 },
\t\t\t{ X: \"22:00\", Y: 118.00 },
\t\t\t{ X: \"23:00\", Y: 134.89 },
\t\t\t{ X: \"0:00\", Y: 140.26 },
\t\t\t{ X: \"1:00\", Y: 128.89 },
\t\t\t{ X: \"2:00\", Y: 118.87 },
\t\t\t{ X: \"3:00\", Y: 134.00 },
\t\t\t{ X: \"4:00\", Y: 180.00 }
\t\t]
\t};
\tvar graphdata3 = {
\t\tlinecolor: \"#FF99CC\",
\t\ttitle: \"Wednesday\",
\t\tvalues: [
\t\t\t{ X: \"6:00\", Y: 230.00 },
\t\t\t{ X: \"7:00\", Y: 210.00 },
\t\t\t{ X: \"8:00\", Y: 214.00 },
\t\t\t{ X: \"9:00\", Y: 234.00 },
\t\t\t{ X: \"10:00\", Y: 247.25 },
\t\t\t{ X: \"11:00\", Y: 218.56 },
\t\t\t{ X: \"12:00\", Y: 268.57 },
\t\t\t{ X: \"13:00\", Y: 274.00 },
\t\t\t{ X: \"14:00\", Y: 280.89 },
\t\t\t{ X: \"15:00\", Y: 242.57 },
\t\t\t{ X: \"16:00\", Y: 298.24 },
\t\t\t{ X: \"17:00\", Y: 208.00 },
\t\t\t{ X: \"18:00\", Y: 214.24 },
\t\t\t{ X: \"19:00\", Y: 214.58 },
\t\t\t{ X: \"20:00\", Y: 211.54 },
\t\t\t{ X: \"21:00\", Y: 248.00 },
\t\t\t{ X: \"22:00\", Y: 258.00 },
\t\t\t{ X: \"23:00\", Y: 234.89 },
\t\t\t{ X: \"0:00\", Y: 210.26 },
\t\t\t{ X: \"1:00\", Y: 248.89 },
\t\t\t{ X: \"2:00\", Y: 238.87 },
\t\t\t{ X: \"3:00\", Y: 264.00 },
\t\t\t{ X: \"4:00\", Y: 270.00 }
\t\t]
\t};
\tvar graphdata4 = {
\t\tlinecolor: \"Random\",
\t\ttitle: \"Thursday\",
\t\tvalues: [
\t\t\t{ X: \"6:00\", Y: 300.00 },
\t\t\t{ X: \"7:00\", Y: 410.98 },
\t\t\t{ X: \"8:00\", Y: 310.00 },
\t\t\t{ X: \"9:00\", Y: 314.00 },
\t\t\t{ X: \"10:00\", Y: 310.25 },
\t\t\t{ X: \"11:00\", Y: 318.56 },
\t\t\t{ X: \"12:00\", Y: 318.57 },
\t\t\t{ X: \"13:00\", Y: 314.00 },
\t\t\t{ X: \"14:00\", Y: 310.89 },
\t\t\t{ X: \"15:00\", Y: 512.57 },
\t\t\t{ X: \"16:00\", Y: 318.24 },
\t\t\t{ X: \"17:00\", Y: 318.00 },
\t\t\t{ X: \"18:00\", Y: 314.24 },
\t\t\t{ X: \"19:00\", Y: 310.58 },
\t\t\t{ X: \"20:00\", Y: 312.54 },
\t\t\t{ X: \"21:00\", Y: 318.00 },
\t\t\t{ X: \"22:00\", Y: 318.00 },
\t\t\t{ X: \"23:00\", Y: 314.89 },
\t\t\t{ X: \"0:00\", Y: 310.26 },
\t\t\t{ X: \"1:00\", Y: 318.89 },
\t\t\t{ X: \"2:00\", Y: 518.87 },
\t\t\t{ X: \"3:00\", Y: 314.00 },
\t\t\t{ X: \"4:00\", Y: 310.00 }
\t\t]
\t};
\tvar Piedata = {
\t\tlinecolor: \"Random\",
\t\ttitle: \"Profit\",
\t\tvalues: [
\t\t\t{ X: \"Monday\", Y: 50.00 },
\t\t\t{ X: \"Tuesday\", Y: 110.98 },
\t\t\t{ X: \"Wednesday\", Y: 70.00 },
\t\t\t{ X: \"Thursday\", Y: 204.00 },
\t\t\t{ X: \"Friday\", Y: 80.25 },
\t\t\t{ X: \"Saturday\", Y: 38.56 },
\t\t\t{ X: \"Sunday\", Y: 98.57 }
\t\t]
\t};
\t\$(function () {
\t\t\$(\"#Bargraph\").SimpleChart({
\t\t\tChartType: \"Bar\",
\t\t\ttoolwidth: \"50\",
\t\t\ttoolheight: \"25\",
\t\t\taxiscolor: \"#E6E6E6\",
\t\t\ttextcolor: \"#6E6E6E\",
\t\t\tshowlegends: true,
\t\t\tdata: [graphdata4, graphdata3, graphdata2, graphdata1],
\t\t\tlegendsize: \"140\",
\t\t\tlegendposition: 'bottom',
\t\t\txaxislabel: 'Hours',
\t\t\ttitle: 'Weekly Profit',
\t\t\tyaxislabel: 'Profit in \$'
\t\t});
\t\t\$(\"#sltchartype\").on('change', function () {
\t\t\t\$(\"#Bargraph\").SimpleChart('ChartType', \$(this).val());
\t\t\t\$(\"#Bargraph\").SimpleChart('reload', 'true');
\t\t});
\t\t\$(\"#Hybridgraph\").SimpleChart({
\t\t\tChartType: \"Hybrid\",
\t\t\ttoolwidth: \"50\",
\t\t\ttoolheight: \"25\",
\t\t\taxiscolor: \"#E6E6E6\",
\t\t\ttextcolor: \"#6E6E6E\",
\t\t\tshowlegends: true,
\t\t\tdata: [graphdata4],
\t\t\tlegendsize: \"140\",
\t\t\tlegendposition: 'bottom',
\t\t\txaxislabel: 'Hours',
\t\t\ttitle: 'Weekly Profit',
\t\t\tyaxislabel: 'Profit in \$'
\t\t});
\t\t\$(\"#Linegraph\").SimpleChart({
\t\t\tChartType: \"Line\",
\t\t\ttoolwidth: \"50\",
\t\t\ttoolheight: \"25\",
\t\t\taxiscolor: \"#E6E6E6\",
\t\t\ttextcolor: \"#6E6E6E\",
\t\t\tshowlegends: false,
\t\t\tdata: [graphdata4, graphdata3, graphdata2, graphdata1],
\t\t\tlegendsize: \"140\",
\t\t\tlegendposition: 'bottom',
\t\t\txaxislabel: 'Hours',
\t\t\ttitle: 'Weekly Profit',
\t\t\tyaxislabel: 'Profit in \$'
\t\t});
\t\t\$(\"#Areagraph\").SimpleChart({
\t\t\tChartType: \"Area\",
\t\t\ttoolwidth: \"50\",
\t\t\ttoolheight: \"25\",
\t\t\taxiscolor: \"#E6E6E6\",
\t\t\ttextcolor: \"#6E6E6E\",
\t\t\tshowlegends: true,
\t\t\tdata: [graphdata4, graphdata3, graphdata2, graphdata1],
\t\t\tlegendsize: \"140\",
\t\t\tlegendposition: 'bottom',
\t\t\txaxislabel: 'Hours',
\t\t\ttitle: 'Weekly Profit',
\t\t\tyaxislabel: 'Profit in \$'
\t\t});
\t\t\$(\"#Scatterredgraph\").SimpleChart({
\t\t\tChartType: \"Scattered\",
\t\t\ttoolwidth: \"50\",
\t\t\ttoolheight: \"25\",
\t\t\taxiscolor: \"#E6E6E6\",
\t\t\ttextcolor: \"#6E6E6E\",
\t\t\tshowlegends: true,
\t\t\tdata: [graphdata4, graphdata3, graphdata2, graphdata1],
\t\t\tlegendsize: \"140\",
\t\t\tlegendposition: 'bottom',
\t\t\txaxislabel: 'Hours',
\t\t\ttitle: 'Weekly Profit',
\t\t\tyaxislabel: 'Profit in \$'
\t\t});
\t\t\$(\"#Piegraph\").SimpleChart({
\t\t\tChartType: \"Pie\",
\t\t\ttoolwidth: \"50\",
\t\t\ttoolheight: \"25\",
\t\t\taxiscolor: \"#E6E6E6\",
\t\t\ttextcolor: \"#6E6E6E\",
\t\t\tshowlegends: true,
\t\t\tshowpielables: true,
\t\t\tdata: [Piedata],
\t\t\tlegendsize: \"250\",
\t\t\tlegendposition: 'right',
\t\t\txaxislabel: 'Hours',
\t\t\ttitle: 'Weekly Profit',
\t\t\tyaxislabel: 'Profit in \$'
\t\t});

\t\t\$(\"#Stackedbargraph\").SimpleChart({
\t\t\tChartType: \"Stacked\",
\t\t\ttoolwidth: \"50\",
\t\t\ttoolheight: \"25\",
\t\t\taxiscolor: \"#E6E6E6\",
\t\t\ttextcolor: \"#6E6E6E\",
\t\t\tshowlegends: true,
\t\t\tdata: [graphdata3, graphdata2, graphdata1],
\t\t\tlegendsize: \"140\",
\t\t\tlegendposition: 'bottom',
\t\t\txaxislabel: 'Hours',
\t\t\ttitle: 'Weekly Profit',
\t\t\tyaxislabel: 'Profit in \$'
\t\t});

\t\t\$(\"#StackedHybridbargraph\").SimpleChart({
\t\t\tChartType: \"StackedHybrid\",
\t\t\ttoolwidth: \"50\",
\t\t\ttoolheight: \"25\",
\t\t\taxiscolor: \"#E6E6E6\",
\t\t\ttextcolor: \"#6E6E6E\",
\t\t\tshowlegends: true,
\t\t\tdata: [graphdata3, graphdata2, graphdata1],
\t\t\tlegendsize: \"140\",
\t\t\tlegendposition: 'bottom',
\t\t\txaxislabel: 'Hours',
\t\t\ttitle: 'Weekly Profit',
\t\t\tyaxislabel: 'Profit in \$'
\t\t});
\t});

</script>
<!-- //for index page weekly sales java script -->


<!-- Bootstrap Core JavaScript -->
<script src=\"{{ asset('jsB/bootstrap.js') }}\"> </script>
<!-- //Bootstrap Core JavaScript -->



{#script calendar begin#}

<script type=\"text/javascript\" src=\"{{ asset('bundles/fullcalendar/js/fullcalendar/lib/jquery.min.js') }}\"></script>
<script type=\"text/javascript\" src=\"{{ asset('bundles/fullcalendar/js/fullcalendar/lib/moment.min.js') }}\"></script>
<script type=\"text/javascript\" src=\"{{ asset('bundles/fullcalendar/js/fullcalendar/fullcalendar.min.js') }}\"></script>
<script type=\"text/javascript\" src=\"{{ asset('bundles/fullcalendar/js/fullcalendar/fullcalendar.default-settings.js') }}\"></script>

{#script calendar end#}



<script type=\"text/javascript\" charset=\"utf8\" src=\"https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js\"></script>

<script src=\"{{ asset('js/rechercheTable/table.js') }}\"></script>
", "back/_scripts.html.twig", "/var/www/html/3A11/happy_olds/app/Resources/views/back/_scripts.html.twig");
    }
}
