<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @GestionAgee/Planning_traitement_medical/afficherTraitement.html.twig */
class __TwigTemplate_4daa2b07690287b8c4bff2eca8e629186c52d444985b71dfc564e7de0fe92203 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "back/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@GestionAgee/Planning_traitement_medical/afficherTraitement.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@GestionAgee/Planning_traitement_medical/afficherTraitement.html.twig"));

        $this->parent = $this->loadTemplate("back/base.html.twig", "@GestionAgee/Planning_traitement_medical/afficherTraitement.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "GestionAgeeBundle:Planning_visite_medicale:Afficher";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 6
        echo "
    <link rel=\"stylesheet\" type=\"text/css\" href=\"https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css\">

    <style>

        .table {
            border: 2px solid #FFFFFF !important;

        }

        .table th {
            border: 2px solid #FFFFFF !important;
        }

        .table td {
            border: 2px solid #FFFFFF !important;
        }


        .table tbody > tr:hover {
            background-color: #2dde98;
        }

    </style>


    <br>
    <h1 style=\"text-align: center ; font-size: 30px\">Planning des Traitements Médicaux</h1><br>

    <table class=\"table table-bordered\" id=\"myTable\">

        <thead style=\"background-color: #629aa9\">
            <tr>
                <th>Nom Agee</th>
                <th>Prenom Agee</th>
                <th>CIN</th>
                <th>Nom Du Medicament</th>
                <th>Description Du traitement</th>
                <th>Durée du traitement (jour)</th>
                <th>Dejeuner</th>
                <th>Petit-Dejeuner</th>
                <th>Diner</th>
                <th>Modifier</th>
                <th>Supprimer</th>
            </tr>
        </thead>

        ";
        // line 53
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["plannings"] ?? $this->getContext($context, "plannings")));
        foreach ($context['_seq'] as $context["_key"] => $context["planning"]) {
            // line 54
            echo "            <tr>
                ";
            // line 56
            echo "                <td align=center>  ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["planning"], "idAgee", []), "nom", []), "html", null, true);
            echo " </td>
                <td align=center>  ";
            // line 57
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["planning"], "idAgee", []), "prenom", []), "html", null, true);
            echo " </td>
                <td align=center>  ";
            // line 58
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["planning"], "idAgee", []), "cin", []), "html", null, true);
            echo " </td>
                <td align=center>  ";
            // line 59
            echo twig_escape_filter($this->env, $this->getAttribute($context["planning"], "nomMedicament", []), "html", null, true);
            echo " </td>
                <td align=center>  ";
            // line 60
            echo twig_escape_filter($this->env, $this->getAttribute($context["planning"], "traitementDesc", []), "html", null, true);
            echo " </td>
                <td align=center>  ";
            // line 61
            echo twig_escape_filter($this->env, $this->getAttribute($context["planning"], "dureeEnJourDeTraitement", []), "html", null, true);
            echo " </td>
                <td align=center>  ";
            // line 62
            echo twig_escape_filter($this->env, $this->getAttribute($context["planning"], "dejeuner", []), "html", null, true);
            echo " </td>
                <td align=center>  ";
            // line 63
            echo twig_escape_filter($this->env, $this->getAttribute($context["planning"], "petitDejeuner", []), "html", null, true);
            echo " </td>
                <td align=center>  ";
            // line 64
            echo twig_escape_filter($this->env, $this->getAttribute($context["planning"], "diner", []), "html", null, true);
            echo " </td>
                ";
            // line 66
            echo "                ";
            // line 67
            echo "                       ";
            // line 68
            echo "                <td>
                    <a href=\"";
            // line 69
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("modifierTraitement", ["id" => $this->getAttribute($context["planning"], "id", [])]), "html", null, true);
            echo "\">
                        <button class=\"btn btn-warning\">
                            <span class=\"fa fa-edit\"></span>
                        </button>
                    </a>
                </td>
                <td>
                    <a href=\"";
            // line 76
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("supprimerTraitement", ["id" => $this->getAttribute($context["planning"], "id", [])]), "html", null, true);
            echo "\"
                       onclick=\"return confirm('Etes-vous sur de vouloir supprimer ?');\">
                        <button class=\"btn btn-danger\">
                            <span class=\"fa fa-trash\"></span>
                        </button>
                    </a>
                </td>

            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['planning'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 86
        echo "    </table>

    <br><br><br><br><br>



";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "@GestionAgee/Planning_traitement_medical/afficherTraitement.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  205 => 86,  189 => 76,  179 => 69,  176 => 68,  174 => 67,  172 => 66,  168 => 64,  164 => 63,  160 => 62,  156 => 61,  152 => 60,  148 => 59,  144 => 58,  140 => 57,  135 => 56,  132 => 54,  128 => 53,  79 => 6,  70 => 5,  52 => 3,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"back/base.html.twig\" %}

{% block title %}GestionAgeeBundle:Planning_visite_medicale:Afficher{% endblock %}

{% block content %}

    <link rel=\"stylesheet\" type=\"text/css\" href=\"https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css\">

    <style>

        .table {
            border: 2px solid #FFFFFF !important;

        }

        .table th {
            border: 2px solid #FFFFFF !important;
        }

        .table td {
            border: 2px solid #FFFFFF !important;
        }


        .table tbody > tr:hover {
            background-color: #2dde98;
        }

    </style>


    <br>
    <h1 style=\"text-align: center ; font-size: 30px\">Planning des Traitements Médicaux</h1><br>

    <table class=\"table table-bordered\" id=\"myTable\">

        <thead style=\"background-color: #629aa9\">
            <tr>
                <th>Nom Agee</th>
                <th>Prenom Agee</th>
                <th>CIN</th>
                <th>Nom Du Medicament</th>
                <th>Description Du traitement</th>
                <th>Durée du traitement (jour)</th>
                <th>Dejeuner</th>
                <th>Petit-Dejeuner</th>
                <th>Diner</th>
                <th>Modifier</th>
                <th>Supprimer</th>
            </tr>
        </thead>

        {% for planning in plannings %}
            <tr>
                {#<td align=center>  {{ planning.id }} </td>#}
                <td align=center>  {{ planning.idAgee.nom }} </td>
                <td align=center>  {{ planning.idAgee.prenom }} </td>
                <td align=center>  {{ planning.idAgee.cin }} </td>
                <td align=center>  {{ planning.nomMedicament }} </td>
                <td align=center>  {{ planning.traitementDesc }} </td>
                <td align=center>  {{ planning.dureeEnJourDeTraitement }} </td>
                <td align=center>  {{ planning.dejeuner }} </td>
                <td align=center>  {{ planning.petitDejeuner }} </td>
                <td align=center>  {{ planning.diner }} </td>
                {#<td><a href=\"{{ path('modifierTraitement',{'id':planning.id}) }}\">Modifier</a></td>#}
                {#<td><a href=\"{{ path('supprimerTraitement',{'id':planning.id}) }}\"#}
                       {#onclick=\"return confirm('Etes-vous sur de vouloir supprimer ?');\">Supprimer</a></td>#}
                <td>
                    <a href=\"{{ path('modifierTraitement',{'id':planning.id}) }}\">
                        <button class=\"btn btn-warning\">
                            <span class=\"fa fa-edit\"></span>
                        </button>
                    </a>
                </td>
                <td>
                    <a href=\"{{ path('supprimerTraitement',{'id':planning.id}) }}\"
                       onclick=\"return confirm('Etes-vous sur de vouloir supprimer ?');\">
                        <button class=\"btn btn-danger\">
                            <span class=\"fa fa-trash\"></span>
                        </button>
                    </a>
                </td>

            </tr>
        {% endfor %}
    </table>

    <br><br><br><br><br>



{% endblock %}
", "@GestionAgee/Planning_traitement_medical/afficherTraitement.html.twig", "/var/www/html/3A11/happy_olds/src/GestionAgeeBundle/Resources/views/Planning_traitement_medical/afficherTraitement.html.twig");
    }
}
