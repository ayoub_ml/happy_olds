<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* back/base.html.twig */
class __TwigTemplate_9016c85ffe51fe456b5055a72e4a1ac0c636c12e4fe41cb76f1046a696d26763 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "back/base.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "back/base.html.twig"));

        // line 1
        echo "<!DOCTYPE HTML>
<html>
";
        // line 3
        $this->loadTemplate("back/_head.html.twig", "back/base.html.twig", 3)->display($context);
        // line 4
        echo "<body class=\"cbp-spmenu-push\">
<div class=\"main-content\">
    ";
        // line 6
        $this->loadTemplate("back/_left_navigation.html.twig", "back/base.html.twig", 6)->display($context);
        // line 7
        echo "    ";
        $this->loadTemplate("back/_top_header.html.twig", "back/base.html.twig", 7)->display($context);
        // line 8
        echo "    <div id=\"page-wrapper\">
        <div class=\"main-page\">
            ";
        // line 10
        $this->displayBlock('content', $context, $blocks);
        // line 11
        echo "        </div>
    </div>
    ";
        // line 13
        $this->loadTemplate("back/_footer.html.twig", "back/base.html.twig", 13)->display($context);
        // line 14
        echo "    ";
        $this->loadTemplate("back/_stylesheets.html.twig", "back/base.html.twig", 14)->display($context);
        // line 15
        echo "</div>
";
        // line 16
        $this->loadTemplate("back/_scripts.html.twig", "back/base.html.twig", 16)->display($context);
        // line 17
        echo "</body>
</html>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 10
    public function block_content($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "back/base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 10,  72 => 17,  70 => 16,  67 => 15,  64 => 14,  62 => 13,  58 => 11,  56 => 10,  52 => 8,  49 => 7,  47 => 6,  43 => 4,  41 => 3,  37 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE HTML>
<html>
{% include 'back/_head.html.twig' %}
<body class=\"cbp-spmenu-push\">
<div class=\"main-content\">
    {% include 'back/_left_navigation.html.twig' %}
    {% include 'back/_top_header.html.twig' %}
    <div id=\"page-wrapper\">
        <div class=\"main-page\">
            {% block content %}{% endblock %}
        </div>
    </div>
    {% include 'back/_footer.html.twig' %}
    {% include 'back/_stylesheets.html.twig' %}
</div>
{% include 'back/_scripts.html.twig' %}
</body>
</html>", "back/base.html.twig", "/var/www/html/3A11/happy_olds/app/Resources/views/back/base.html.twig");
    }
}
