<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @GestionAgee/Planning_traitement_medical/ajouterTraitement.html.twig */
class __TwigTemplate_579aa4daec2359845cd52723990a88115d4be27a1a633ee9df33d5dde9f86bc5 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "back/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@GestionAgee/Planning_traitement_medical/ajouterTraitement.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@GestionAgee/Planning_traitement_medical/ajouterTraitement.html.twig"));

        $this->parent = $this->loadTemplate("back/base.html.twig", "@GestionAgee/Planning_traitement_medical/ajouterTraitement.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "GestionAgeeBundle:Planning_visite_medicale:Ajouter";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 6
        echo "    <br>

    <style>
        .error{
            color: #ff6c5f;
            border-color: #ff6c5f;
        }
    </style>

    <h1 style=\"text-align: center ; font-size: 30px\">Ajouter Traitement Médical</h1><br>


        <div style=\"text-align: center ; width: 50% ; margin-left: 25%\">
            <fieldset>


                ";
        // line 22
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start', ["attr" => ["id" => "traitementForm"]]);
        echo "
                ";
        // line 23
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "idAgee", []), 'row', ["label" => "Âgée :", "attr" => ["class" => "form-control"]]);
        echo "</br>
                ";
        // line 24
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "nomMedicament", []), 'row', ["label" => "Nom du Médicament", "attr" => ["class" => "form-control"]]);
        echo "</br>
                ";
        // line 25
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "traitementDesc", []), 'row', ["label" => "Description du Traitement", "attr" => ["class" => "form-control", "style" => "resize: vertical; height: 80px;"]]);
        echo "</br>
                ";
        // line 26
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "dureeEnJourDeTraitement", []), 'row', ["label" => "Duree En Jour De Traitement", "attr" => ["class" => "form-control"]]);
        echo "</br>
                ";
        // line 27
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "dejeuner", []), 'row', ["label" => "Dejeuner", "attr" => ["class" => "form-control"]]);
        echo "</br>
                ";
        // line 28
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "petitDejeuner", []), 'row', ["label" => "Petit Dejeuner", "attr" => ["class" => "form-control"]]);
        echo "</br>
                ";
        // line 29
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "diner", []), 'row', ["label" => "Diner", "attr" => ["class" => "form-control"]]);
        echo "</br>
                ";
        // line 30
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "save", []), 'widget', ["label" => "save", "attr" => ["class" => "btn btn-success "]]);
        echo "</br>
                ";
        // line 31
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
            </fieldset>
        </div>





    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js\"></script>
    <script>
        \$('#traitementForm').validate({
            rules: {
                \"gestionageebundle_planning_traitement_medical[idAgee]\":{
                    required: true
                },
                \"gestionageebundle_planning_traitement_medical[nomMedicament]\":{
                    required: true
                },
                \"gestionageebundle_planning_traitement_medical[traitementDesc]\":{
                    required: true
                },
                \"gestionageebundle_planning_traitement_medical[dureeEnJourDeTraitement]\":{
                    required: true,
                    digits: true,
                    min: 1
                },
                \"gestionageebundle_planning_traitement_medical[dejeuner]\":{
                    required: true
                },
                \"gestionageebundle_planning_traitement_medical[petitDejeuner]\":{
                    required: true
                },
                \"gestionageebundle_planning_traitement_medical[diner]\":{
                    required: true
                }

            },
            messages: {
                \"gestionageebundle_planning_traitement_medical[idAgee]\":{
                    required: \"Veuillez choisir l'Agée\"
                },
                \"gestionageebundle_planning_traitement_medical[nomMedicament]\":{
                    required: \"Veuillez saisir un nom du médicament\"
                },
                \"gestionageebundle_planning_traitement_medical[traitementDesc]\":{
                    required: \"Veuillez saisir une description\"
                },
                \"gestionageebundle_planning_traitement_medical[dureeEnJourDeTraitement]\":{
                    required: \"Veuillez indiquer le nombre des jours\",
                    digits: \"Veuiller saisir un nombre valide\",
                    min: \"Veuiller saisir un nombre supérieur à 0\"
                },
                \"gestionageebundle_planning_traitement_medical[dejeuner]\":{
                    required: \"Veuillez selectionner un choix\"
                },
                \"gestionageebundle_planning_traitement_medical[petitDejeuner]\":{
                    required: \"Veuillez selectionner un choix\"
                },
                \"gestionageebundle_planning_traitement_medical[diner]\":{
                    required: \"Veuillez selectionner un choix\"
                }
            }
        });
    </script>



";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "@GestionAgee/Planning_traitement_medical/ajouterTraitement.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  133 => 31,  129 => 30,  125 => 29,  121 => 28,  117 => 27,  113 => 26,  109 => 25,  105 => 24,  101 => 23,  97 => 22,  79 => 6,  70 => 5,  52 => 3,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"back/base.html.twig\" %}

{% block title %}GestionAgeeBundle:Planning_visite_medicale:Ajouter{% endblock %}

{% block content %}
    <br>

    <style>
        .error{
            color: #ff6c5f;
            border-color: #ff6c5f;
        }
    </style>

    <h1 style=\"text-align: center ; font-size: 30px\">Ajouter Traitement Médical</h1><br>


        <div style=\"text-align: center ; width: 50% ; margin-left: 25%\">
            <fieldset>


                {{ form_start(form, {'attr':{'id':'traitementForm'}}) }}
                {{ form_row(form.idAgee,{'label':'Âgée :','attr':{'class':'form-control'}}) }}</br>
                {{ form_row(form.nomMedicament, {'label':'Nom du Médicament','attr':{'class':'form-control'}}) }}</br>
                {{ form_row(form.traitementDesc, {'label':'Description du Traitement','attr':{'class':'form-control','style':'resize: vertical; height: 80px;'}}) }}</br>
                {{ form_row(form.dureeEnJourDeTraitement, {'label':'Duree En Jour De Traitement','attr':{'class':'form-control'}}) }}</br>
                {{ form_row(form.dejeuner, {'label':'Dejeuner','attr':{'class':'form-control'}}) }}</br>
                {{ form_row(form.petitDejeuner, {'label':'Petit Dejeuner','attr':{'class':'form-control'}}) }}</br>
                {{ form_row(form.diner, {'label':'Diner','attr':{'class':'form-control'}}) }}</br>
                {{ form_widget(form.save, { 'label': 'save' ,'attr':{'class':'btn btn-success '}}) }}</br>
                {{ form_end(form) }}
            </fieldset>
        </div>





    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js\"></script>
    <script>
        \$('#traitementForm').validate({
            rules: {
                \"gestionageebundle_planning_traitement_medical[idAgee]\":{
                    required: true
                },
                \"gestionageebundle_planning_traitement_medical[nomMedicament]\":{
                    required: true
                },
                \"gestionageebundle_planning_traitement_medical[traitementDesc]\":{
                    required: true
                },
                \"gestionageebundle_planning_traitement_medical[dureeEnJourDeTraitement]\":{
                    required: true,
                    digits: true,
                    min: 1
                },
                \"gestionageebundle_planning_traitement_medical[dejeuner]\":{
                    required: true
                },
                \"gestionageebundle_planning_traitement_medical[petitDejeuner]\":{
                    required: true
                },
                \"gestionageebundle_planning_traitement_medical[diner]\":{
                    required: true
                }

            },
            messages: {
                \"gestionageebundle_planning_traitement_medical[idAgee]\":{
                    required: \"Veuillez choisir l'Agée\"
                },
                \"gestionageebundle_planning_traitement_medical[nomMedicament]\":{
                    required: \"Veuillez saisir un nom du médicament\"
                },
                \"gestionageebundle_planning_traitement_medical[traitementDesc]\":{
                    required: \"Veuillez saisir une description\"
                },
                \"gestionageebundle_planning_traitement_medical[dureeEnJourDeTraitement]\":{
                    required: \"Veuillez indiquer le nombre des jours\",
                    digits: \"Veuiller saisir un nombre valide\",
                    min: \"Veuiller saisir un nombre supérieur à 0\"
                },
                \"gestionageebundle_planning_traitement_medical[dejeuner]\":{
                    required: \"Veuillez selectionner un choix\"
                },
                \"gestionageebundle_planning_traitement_medical[petitDejeuner]\":{
                    required: \"Veuillez selectionner un choix\"
                },
                \"gestionageebundle_planning_traitement_medical[diner]\":{
                    required: \"Veuillez selectionner un choix\"
                }
            }
        });
    </script>



{% endblock %}
", "@GestionAgee/Planning_traitement_medical/ajouterTraitement.html.twig", "/var/www/html/3A11/happy_olds/src/GestionAgeeBundle/Resources/views/Planning_traitement_medical/ajouterTraitement.html.twig");
    }
}
