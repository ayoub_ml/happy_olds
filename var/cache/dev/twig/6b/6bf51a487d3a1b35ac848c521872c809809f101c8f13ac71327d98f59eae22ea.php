<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* back/_left_navigation.html.twig */
class __TwigTemplate_82d266b3c13ade47496b59e2157b4d9b6b393fc280f25cce67b0387c9ae28537 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "back/_left_navigation.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "back/_left_navigation.html.twig"));

        // line 1
        echo "<div class=\"cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left\" id=\"cbp-spmenu-s1\">
    <!--left-fixed -navigation-->
    <aside class=\"sidebar-left\">
        <nav class=\"navbar navbar-inverse\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\".collapse\" aria-expanded=\"false\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <h1><a class=\"navbar-brand\" href=\"";
        // line 12
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("back");
        echo "\"><span class=\"fa fa-area-chart\"></span> Happy<span class=\"dashboard_text\">Olds</span></a></h1>
            </div>
            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
                <ul class=\"sidebar-menu\">
                    <li class=\"header\"></li>
                    <li class=\"treeview\">
                        <a href=\"";
        // line 18
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ajouter");
        echo "\">
                            <i class=\"fa fa-calendar-plus-o\"></i> <span>Ajouter un RDV</span>
                        </a>
                    </li>
                    <li class=\"treeview\">
                        <a href=\"";
        // line 23
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("afficher");
        echo "\">
                            <i class=\"fa fa-calendar\"></i> <span>Liste des RDV</span>
                        </a>
                    </li>
                    <li class=\"treeview\">
                        <a href=\"";
        // line 28
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ajouterTraitement");
        echo "\">
                            <i class=\"fa fa-medkit\"></i> <span>Ajouter Traitement</span>
                        </a>
                    </li>
                    <li class=\"treeview\">
                        <a href=\"";
        // line 33
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("afficherTraitement");
        echo "\">
                            <i class=\"fa fa-list\"></i> <span>Liste Des Traitements</span>
                        </a>
                    </li>


                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
    </aside>
</div>
<!--left-fixed -navigation-->";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "back/_left_navigation.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 33,  74 => 28,  66 => 23,  58 => 18,  49 => 12,  36 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left\" id=\"cbp-spmenu-s1\">
    <!--left-fixed -navigation-->
    <aside class=\"sidebar-left\">
        <nav class=\"navbar navbar-inverse\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\".collapse\" aria-expanded=\"false\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <h1><a class=\"navbar-brand\" href=\"{{ path('back') }}\"><span class=\"fa fa-area-chart\"></span> Happy<span class=\"dashboard_text\">Olds</span></a></h1>
            </div>
            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
                <ul class=\"sidebar-menu\">
                    <li class=\"header\"></li>
                    <li class=\"treeview\">
                        <a href=\"{{ path('ajouter') }}\">
                            <i class=\"fa fa-calendar-plus-o\"></i> <span>Ajouter un RDV</span>
                        </a>
                    </li>
                    <li class=\"treeview\">
                        <a href=\"{{ path('afficher') }}\">
                            <i class=\"fa fa-calendar\"></i> <span>Liste des RDV</span>
                        </a>
                    </li>
                    <li class=\"treeview\">
                        <a href=\"{{ path('ajouterTraitement') }}\">
                            <i class=\"fa fa-medkit\"></i> <span>Ajouter Traitement</span>
                        </a>
                    </li>
                    <li class=\"treeview\">
                        <a href=\"{{ path('afficherTraitement') }}\">
                            <i class=\"fa fa-list\"></i> <span>Liste Des Traitements</span>
                        </a>
                    </li>


                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
    </aside>
</div>
<!--left-fixed -navigation-->", "back/_left_navigation.html.twig", "/var/www/html/3A11/happy_olds/app/Resources/views/back/_left_navigation.html.twig");
    }
}
