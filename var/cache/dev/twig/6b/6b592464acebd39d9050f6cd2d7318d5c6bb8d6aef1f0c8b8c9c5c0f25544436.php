<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* front/_top_header.html.twig */
class __TwigTemplate_e1011569e2251b5482b998a08a8241e8bde7f879c55c237f985ab46191d71193 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "front/_top_header.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "front/_top_header.html.twig"));

        // line 1
        echo "<style>

    .customButton:hover {
        background-color: #ff4c00 !important;
        color: white;
    }

    .notification-div > li {
        border-bottom: 1px solid #ccc;
        padding: 20px 10px 20px 10px;
    }

    .notification-div > li.active {
        background-color: #ffd8a875;
        font-weight: bold;
    }
</style>

<div class=\"top_header\" id=\"home\">
    <!-- Fixed navbar -->
    <nav class=\"navbar navbar-default navbar-fixed-top\">
        <div class=\"nav_top_fx_w3layouts_agileits\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\"
                        aria-expanded=\"false\"
                        aria-controls=\"navbar\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <div class=\"logo_wthree_agile\">
                    <h1>
                        <a style=\"font-size: 15px;\" class=\"navbar-brand\" href=\"";
        // line 34
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">
                            <i class=\"fas fa-heart\" aria-hidden=\"true\"></i> Happy Olds
                            <span style=\"font-size: 0.6em;\" class=\"desc\">Give a little. Help a lot.</span>
                        </a>
                    </h1>
                </div>
            </div>
            <div id=\"navbar\" class=\"navbar-collapse collapse\">
                <div class=\"nav_right_top\">
                    <ul class=\"nav navbar-nav\">

                        ";
        // line 45
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("IS_AUTHENTICATED_FULLY")) {
            // line 46
            echo "                            <li id=\"notifications-dropdown\" class=\"dropdown\">
                                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\"
                                   aria-haspopup=\"true\" aria-expanded=\"false\"
                                   style=\"padding-bottom: 32px; padding-left: 25px;\">
                                    <span style=\"font-size: 19px;\" class=\"fa fa-bell\"></span>
                                    <span class=\"caret\"></span>
                                    <div id=\"notification-number-wrapper\">
                                        <div id=\"notification-number\">
                                            ";
            // line 54
            if (($this->env->getExtension('Mgilet\NotificationBundle\Twig\NotificationExtension')->countUnseenNotifications($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", [])) != 0)) {
                // line 55
                echo "                                                <span style=\"border-radius: 50%; background-color: red; color: white; padding: 2px 5px 1px 7px; position: absolute; top: 10px; left: 5px;\">";
                echo $this->env->getExtension('Mgilet\NotificationBundle\Twig\NotificationExtension')->countUnseenNotifications($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", []));
                echo "</span>
                                            ";
            }
            // line 57
            echo "                                        </div>
                                    </div>
                                </a>
                                ";
            // line 60
            if (($this->getAttribute(($context["app"] ?? null), "user", [], "any", true, true) && ($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", []) != null))) {
                // line 61
                echo "                                    ";
                echo $this->env->getExtension('Mgilet\NotificationBundle\Twig\NotificationExtension')->render($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", []), ["template" => "front/_notification.html.twig"]);
                echo "
                                ";
            }
            // line 63
            echo "                            </li>
                            <li class=\"";
            // line 64
            if (($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", []), "pathinfo", []) == "/afficherVisiteAgee")) {
                echo " active ";
            }
            echo "\">
                                <a href=\"";
            // line 65
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("afficherVisiteAgee");
            echo "\">RDV</a>
                            </li>
                            <li class=\"";
            // line 67
            if (($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", []), "pathinfo", []) == "/afficherTraitementAgee")) {
                echo " active ";
            }
            echo "\">
                                <a href=\"";
            // line 68
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("afficherTraitementAgee");
            echo "\">Traitement</a>
                            </li>
                        ";
        }
        // line 71
        echo "

                        ";
        // line 74
        echo "                            ";
        // line 75
        echo "                        ";
        // line 76
        echo "                        ";
        if ( !$this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("IS_AUTHENTICATED_FULLY")) {
            // line 77
            echo "                            <li class=\"";
            if (($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", []), "pathinfo", []) == "/login")) {
                echo " active ";
            }
            echo "\">
                                <a href=\"";
            // line 78
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_login");
            echo "\">Login</a>
                            </li>

                            <li class=\"";
            // line 81
            if (($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", []), "pathinfo", []) == "/register/")) {
                echo " active ";
            }
            echo "\">
                                <a href=\"";
            // line 82
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_registration_register");
            echo "\">Register</a>
                            </li>
                        ";
        }
        // line 85
        echo "                        <li class=\"";
        if (($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", []), "pathinfo", []) == "/about")) {
            echo " active ";
        }
        echo "\">
                            <a href=\"";
        // line 86
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("about");
        echo "\">About</a>
                        </li>
";
        // line 91
        echo "
                        ";
        // line 92
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("IS_AUTHENTICATED_FULLY")) {
            // line 93
            echo "                            <li>
                                <a href=\"";
            // line 94
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_logout");
            echo "\">Logout</a>
                            </li>
                        ";
        }
        // line 97
        echo "

                    </ul>
                </div>
            </div>
            <!--/.nav-collapse -->
        </div>
    </nav>
    <div class=\"clearfix\"></div>


</div>


";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "front/_top_header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  206 => 97,  200 => 94,  197 => 93,  195 => 92,  192 => 91,  187 => 86,  180 => 85,  174 => 82,  168 => 81,  162 => 78,  155 => 77,  152 => 76,  150 => 75,  148 => 74,  144 => 71,  138 => 68,  132 => 67,  127 => 65,  121 => 64,  118 => 63,  112 => 61,  110 => 60,  105 => 57,  99 => 55,  97 => 54,  87 => 46,  85 => 45,  71 => 34,  36 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<style>

    .customButton:hover {
        background-color: #ff4c00 !important;
        color: white;
    }

    .notification-div > li {
        border-bottom: 1px solid #ccc;
        padding: 20px 10px 20px 10px;
    }

    .notification-div > li.active {
        background-color: #ffd8a875;
        font-weight: bold;
    }
</style>

<div class=\"top_header\" id=\"home\">
    <!-- Fixed navbar -->
    <nav class=\"navbar navbar-default navbar-fixed-top\">
        <div class=\"nav_top_fx_w3layouts_agileits\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\"
                        aria-expanded=\"false\"
                        aria-controls=\"navbar\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <div class=\"logo_wthree_agile\">
                    <h1>
                        <a style=\"font-size: 15px;\" class=\"navbar-brand\" href=\"{{ path ('homepage') }}\">
                            <i class=\"fas fa-heart\" aria-hidden=\"true\"></i> Happy Olds
                            <span style=\"font-size: 0.6em;\" class=\"desc\">Give a little. Help a lot.</span>
                        </a>
                    </h1>
                </div>
            </div>
            <div id=\"navbar\" class=\"navbar-collapse collapse\">
                <div class=\"nav_right_top\">
                    <ul class=\"nav navbar-nav\">

                        {% if is_granted('IS_AUTHENTICATED_FULLY') %}
                            <li id=\"notifications-dropdown\" class=\"dropdown\">
                                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\"
                                   aria-haspopup=\"true\" aria-expanded=\"false\"
                                   style=\"padding-bottom: 32px; padding-left: 25px;\">
                                    <span style=\"font-size: 19px;\" class=\"fa fa-bell\"></span>
                                    <span class=\"caret\"></span>
                                    <div id=\"notification-number-wrapper\">
                                        <div id=\"notification-number\">
                                            {% if  mgilet_notification_unseen_count(app.user)  != 0 %}
                                                <span style=\"border-radius: 50%; background-color: red; color: white; padding: 2px 5px 1px 7px; position: absolute; top: 10px; left: 5px;\">{{ mgilet_notification_unseen_count(app.user) }}</span>
                                            {% endif %}
                                        </div>
                                    </div>
                                </a>
                                {% if app.user is defined and app.user!=null %}
                                    {{ mgilet_notification_render(app.user,{ 'template': 'front/_notification.html.twig'}) }}
                                {% endif %}
                            </li>
                            <li class=\"{% if app.request.pathinfo == '/afficherVisiteAgee' %} active {% endif %}\">
                                <a href=\"{{ path ('afficherVisiteAgee') }}\">RDV</a>
                            </li>
                            <li class=\"{% if app.request.pathinfo == '/afficherTraitementAgee' %} active {% endif %}\">
                                <a href=\"{{ path ('afficherTraitementAgee') }}\">Traitement</a>
                            </li>
                        {% endif %}


                        {#<li class=\"{% if app.request.pathinfo == '/' %} active {% endif %}\">#}
                            {#<a href=\"{{ path ('homepage') }}\">Home</a>#}
                        {#</li>#}
                        {% if not is_granted('IS_AUTHENTICATED_FULLY') %}
                            <li class=\"{% if app.request.pathinfo == '/login' %} active {% endif %}\">
                                <a href=\"{{ path ('fos_user_security_login') }}\">Login</a>
                            </li>

                            <li class=\"{% if app.request.pathinfo == '/register/' %} active {% endif %}\">
                                <a href=\"{{ path ('fos_user_registration_register') }}\">Register</a>
                            </li>
                        {% endif %}
                        <li class=\"{% if app.request.pathinfo == '/about' %} active {% endif %}\">
                            <a href=\"{{ path ('about') }}\">About</a>
                        </li>
{#                        <li>#}
{#                            <a class=\"scroll\" href=\"#contact\">Contact</a>#}
{#                        </li>#}

                        {% if is_granted('IS_AUTHENTICATED_FULLY') %}
                            <li>
                                <a href=\"{{ path('fos_user_security_logout') }}\">Logout</a>
                            </li>
                        {% endif %}


                    </ul>
                </div>
            </div>
            <!--/.nav-collapse -->
        </div>
    </nav>
    <div class=\"clearfix\"></div>


</div>


", "front/_top_header.html.twig", "/var/www/html/3A11/happy_olds/app/Resources/views/front/_top_header.html.twig");
    }
}
