<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @GestionAgee/Agee/afficherVisiteAgee.html.twig */
class __TwigTemplate_c9003bbf64507063f50e1ba709608c2ab054097e461e087077f7a24c2cf690d3 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "front/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@GestionAgee/Agee/afficherVisiteAgee.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@GestionAgee/Agee/afficherVisiteAgee.html.twig"));

        $this->parent = $this->loadTemplate("front/base.html.twig", "@GestionAgee/Agee/afficherVisiteAgee.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "GestionAgeeBundle:Planning_visite_medicale:Afficher";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 6
        echo "






    <!-- banner -->
    <div class=\"banner_top\">
        <div id=\"myCarousel\" class=\"carousel slide\" data-ride=\"carousel\">
            <!-- Indicators -->
            <ol class=\"carousel-indicators\">
                <li data-target=\"#myCarousel\" data-slide-to=\"0\" class=\"active\"></li>
                <li data-target=\"#myCarousel\" data-slide-to=\"1\" class=\"\"></li>
                <li data-target=\"#myCarousel\" data-slide-to=\"2\" class=\"\"></li>
                <li data-target=\"#myCarousel\" data-slide-to=\"3\" class=\"\"></li>
            </ol>
            <div class=\"carousel-inner\" role=\"listbox\">
                <div class=\"item active\">
                    <div class=\"container\">
                        <div class=\"carousel-caption\">
                            <h3>Change Begins With You.</h3>


                        </div>
                    </div>
                </div>
                <div class=\"item item2\">
                    <div class=\"container\">
                        <div class=\"carousel-caption\">
                            <h3>Make good things happen</h3>


                        </div>
                    </div>
                </div>
                <div class=\"item item3\">
                    <div class=\"container\">
                        <div class=\"carousel-caption\">
                            <h3>Change Begins With You.</h3>


                        </div>
                    </div>
                </div>
                <div class=\"item item4\">
                    <div class=\"container\">
                        <div class=\"carousel-caption\">


                        </div>
                    </div>
                </div>
            </div>
            <a class=\"left carousel-control\" href=\"#myCarousel\" role=\"button\" data-slide=\"prev\">
                <span class=\"fa fa-chevron-left\" aria-hidden=\"true\"></span>
                <span class=\"sr-only\">Previous</span>
            </a>
            <a class=\"right carousel-control\" href=\"#myCarousel\" role=\"button\" data-slide=\"next\">
                <span class=\"fa fa-chevron-right\" aria-hidden=\"true\"></span>
                <span class=\"sr-only\">Next</span>
            </a>
            <!-- The Modal -->
        </div>
    </div>












    <style>

        body {
            background-color: #F1F1F1;
        }

        #planning {
            background-color: #FAFAFA;
            width: 90%;
            margin-left: 5%;
            padding: 15px;
        }

        .table {
            border: 2px solid #629aa9 !important;

        }

        .table th {
            border: 2px solid #629aa9 !important;
        }

        .table td {
            border: 2px solid #629aa9 !important;
        }

    </style>


    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <div id=\"planning\">
        <br>
        <h1 style=\"text-align: center ; font-size: 30px\"  >Planning des visites medicales</h1>
        <br>
        <br>
        <table class=\"table table-bordered\" >

            <thead>
            <th>Nom Agee</th>
            <th>Prenom Agee</th>
            <th>CIN</th>
            <th>Date du RDV</th>
            <th>Heure du RDV</th>
            </thead>


            <tbody>
            ";
        // line 130
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["plannings"] ?? $this->getContext($context, "plannings")));
        foreach ($context['_seq'] as $context["_key"] => $context["planning"]) {
            // line 131
            echo "                <tr>
                    ";
            // line 133
            echo "                    <td align=center>  ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["planning"], "idAgee", []), "nom", []), "html", null, true);
            echo " </td>
                    <td align=center>  ";
            // line 134
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["planning"], "idAgee", []), "prenom", []), "html", null, true);
            echo " </td>
                    <td align=center>  ";
            // line 135
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["planning"], "idAgee", []), "cin", []), "html", null, true);
            echo " </td>
                    <td align=center>  ";
            // line 136
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["planning"], "dateRdv", []), "d-m-Y"), "html", null, true);
            echo " </td>
                    <td align=center>  ";
            // line 137
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["planning"], "dateRdv", []), "H:i:s"), "html", null, true);
            echo " </td>



                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['planning'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 143
        echo "            </tbody>
        </table>
    </div>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>


    ";
        // line 149
        $this->loadTemplate("@FullCalendar/Calendar/calendar.html.twig", "@GestionAgee/Agee/afficherVisiteAgee.html.twig", 149)->display($context);
        // line 150
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "@GestionAgee/Agee/afficherVisiteAgee.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  251 => 150,  249 => 149,  241 => 143,  229 => 137,  225 => 136,  221 => 135,  217 => 134,  212 => 133,  209 => 131,  205 => 130,  79 => 6,  70 => 5,  52 => 3,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"front/base.html.twig\" %}

{% block title %}GestionAgeeBundle:Planning_visite_medicale:Afficher{% endblock %}

{% block content %}







    <!-- banner -->
    <div class=\"banner_top\">
        <div id=\"myCarousel\" class=\"carousel slide\" data-ride=\"carousel\">
            <!-- Indicators -->
            <ol class=\"carousel-indicators\">
                <li data-target=\"#myCarousel\" data-slide-to=\"0\" class=\"active\"></li>
                <li data-target=\"#myCarousel\" data-slide-to=\"1\" class=\"\"></li>
                <li data-target=\"#myCarousel\" data-slide-to=\"2\" class=\"\"></li>
                <li data-target=\"#myCarousel\" data-slide-to=\"3\" class=\"\"></li>
            </ol>
            <div class=\"carousel-inner\" role=\"listbox\">
                <div class=\"item active\">
                    <div class=\"container\">
                        <div class=\"carousel-caption\">
                            <h3>Change Begins With You.</h3>


                        </div>
                    </div>
                </div>
                <div class=\"item item2\">
                    <div class=\"container\">
                        <div class=\"carousel-caption\">
                            <h3>Make good things happen</h3>


                        </div>
                    </div>
                </div>
                <div class=\"item item3\">
                    <div class=\"container\">
                        <div class=\"carousel-caption\">
                            <h3>Change Begins With You.</h3>


                        </div>
                    </div>
                </div>
                <div class=\"item item4\">
                    <div class=\"container\">
                        <div class=\"carousel-caption\">


                        </div>
                    </div>
                </div>
            </div>
            <a class=\"left carousel-control\" href=\"#myCarousel\" role=\"button\" data-slide=\"prev\">
                <span class=\"fa fa-chevron-left\" aria-hidden=\"true\"></span>
                <span class=\"sr-only\">Previous</span>
            </a>
            <a class=\"right carousel-control\" href=\"#myCarousel\" role=\"button\" data-slide=\"next\">
                <span class=\"fa fa-chevron-right\" aria-hidden=\"true\"></span>
                <span class=\"sr-only\">Next</span>
            </a>
            <!-- The Modal -->
        </div>
    </div>












    <style>

        body {
            background-color: #F1F1F1;
        }

        #planning {
            background-color: #FAFAFA;
            width: 90%;
            margin-left: 5%;
            padding: 15px;
        }

        .table {
            border: 2px solid #629aa9 !important;

        }

        .table th {
            border: 2px solid #629aa9 !important;
        }

        .table td {
            border: 2px solid #629aa9 !important;
        }

    </style>


    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <div id=\"planning\">
        <br>
        <h1 style=\"text-align: center ; font-size: 30px\"  >Planning des visites medicales</h1>
        <br>
        <br>
        <table class=\"table table-bordered\" >

            <thead>
            <th>Nom Agee</th>
            <th>Prenom Agee</th>
            <th>CIN</th>
            <th>Date du RDV</th>
            <th>Heure du RDV</th>
            </thead>


            <tbody>
            {% for planning in plannings %}
                <tr>
                    {#<td align=center>  {{ planning.id }} </td>#}
                    <td align=center>  {{ planning.idAgee.nom }} </td>
                    <td align=center>  {{ planning.idAgee.prenom }} </td>
                    <td align=center>  {{ planning.idAgee.cin }} </td>
                    <td align=center>  {{ planning.dateRdv|date('d-m-Y') }} </td>
                    <td align=center>  {{ planning.dateRdv|date('H:i:s') }} </td>



                </tr>
            {% endfor %}
            </tbody>
        </table>
    </div>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>


    {% include '@FullCalendar/Calendar/calendar.html.twig' %}

{% endblock %}
", "@GestionAgee/Agee/afficherVisiteAgee.html.twig", "/var/www/html/3A11/happy_olds/src/GestionAgeeBundle/Resources/views/Agee/afficherVisiteAgee.html.twig");
    }
}
