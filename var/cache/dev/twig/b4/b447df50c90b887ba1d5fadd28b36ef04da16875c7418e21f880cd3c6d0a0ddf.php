<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* back/_top_header.html.twig */
class __TwigTemplate_e0e36f95ac8fca9a7d71482295af0978c3c370914ff8c4453f45c5bfca9a6e8c extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "back/_top_header.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "back/_top_header.html.twig"));

        // line 1
        echo "<!-- header-starts -->
<div class=\"sticky-header header-section \">
\t<div class=\"header-left\">
\t\t<!--toggle button start-->
\t\t<button id=\"showLeftPush\"><i class=\"fa fa-bars\"></i></button>
\t\t<!--toggle button end-->
\t\t<div class=\"profile_details_left\"><!--notifications of menu start -->
\t\t\t<ul class=\"nofitications-dropdown\">
\t\t\t\t<li class=\"dropdown head-dpdn\">
\t\t\t\t\t<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"fa fa-envelope\"></i><span class=\"badge\">4</span></a>
\t\t\t\t\t<ul class=\"dropdown-menu\">
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<div class=\"notification_header\">
\t\t\t\t\t\t\t\t<h3>You have 3 new messages</h3>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li><a href=\"#\">
\t\t\t\t\t\t\t\t<div class=\"user_img\"><img src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("imagesB/1.jpg"), "html", null, true);
        echo "\" alt=\"\"></div>
\t\t\t\t\t\t\t\t<div class=\"notification_desc\">
\t\t\t\t\t\t\t\t\t<p>Lorem ipsum dolor amet</p>
\t\t\t\t\t\t\t\t\t<p><span>1 hour ago</span></p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t</a></li>
\t\t\t\t\t\t<li class=\"odd\"><a href=\"#\">
\t\t\t\t\t\t\t\t<div class=\"user_img\"><img src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("imagesB/4.jpg"), "html", null, true);
        echo "\" alt=\"\"></div>
\t\t\t\t\t\t\t\t<div class=\"notification_desc\">
\t\t\t\t\t\t\t\t\t<p>Lorem ipsum dolor amet </p>
\t\t\t\t\t\t\t\t\t<p><span>1 hour ago</span></p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t</a></li>
\t\t\t\t\t\t<li><a href=\"#\">
\t\t\t\t\t\t\t\t<div class=\"user_img\"><img src=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("imagesB/3.jpg"), "html", null, true);
        echo "\" alt=\"\"></div>
\t\t\t\t\t\t\t\t<div class=\"notification_desc\">
\t\t\t\t\t\t\t\t\t<p>Lorem ipsum dolor amet </p>
\t\t\t\t\t\t\t\t\t<p><span>1 hour ago</span></p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t</a></li>
\t\t\t\t\t\t<li><a href=\"#\">
\t\t\t\t\t\t\t\t<div class=\"user_img\"><img src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("imagesB/2.jpg"), "html", null, true);
        echo "\" alt=\"\"></div>
\t\t\t\t\t\t\t\t<div class=\"notification_desc\">
\t\t\t\t\t\t\t\t\t<p>Lorem ipsum dolor amet </p>
\t\t\t\t\t\t\t\t\t<p><span>1 hour ago</span></p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t</a></li>
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<div class=\"notification_bottom\">
\t\t\t\t\t\t\t\t<a href=\"#\">See all messages</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</li>
\t\t\t\t\t</ul>
\t\t\t\t</li>
\t\t\t\t<li class=\"dropdown head-dpdn\">
\t\t\t\t\t<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"fa fa-bell\"></i><span class=\"badge blue\">4</span></a>
\t\t\t\t\t<ul class=\"dropdown-menu\">
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<div class=\"notification_header\">
\t\t\t\t\t\t\t\t<h3>You have 3 new notification</h3>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li><a href=\"#\">
\t\t\t\t\t\t\t\t<div class=\"user_img\"><img src=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("imagesB/4.jpg"), "html", null, true);
        echo "\" alt=\"\"></div>
\t\t\t\t\t\t\t\t<div class=\"notification_desc\">
\t\t\t\t\t\t\t\t\t<p>Lorem ipsum dolor amet</p>
\t\t\t\t\t\t\t\t\t<p><span>1 hour ago</span></p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t</a></li>
\t\t\t\t\t\t<li class=\"odd\"><a href=\"#\">
\t\t\t\t\t\t\t\t<div class=\"user_img\"><img src=\"";
        // line 73
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("imagesB/1.jpg"), "html", null, true);
        echo "\" alt=\"\"></div>
\t\t\t\t\t\t\t\t<div class=\"notification_desc\">
\t\t\t\t\t\t\t\t\t<p>Lorem ipsum dolor amet </p>
\t\t\t\t\t\t\t\t\t<p><span>1 hour ago</span></p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t</a></li>
\t\t\t\t\t\t<li><a href=\"#\">
\t\t\t\t\t\t\t\t<div class=\"user_img\"><img src=\"";
        // line 81
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("imagesB/3.jpg"), "html", null, true);
        echo "\" alt=\"\"></div>
\t\t\t\t\t\t\t\t<div class=\"notification_desc\">
\t\t\t\t\t\t\t\t\t<p>Lorem ipsum dolor amet </p>
\t\t\t\t\t\t\t\t\t<p><span>1 hour ago</span></p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t</a></li>
\t\t\t\t\t\t<li><a href=\"#\">
\t\t\t\t\t\t\t\t<div class=\"user_img\"><img src=\"";
        // line 89
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("imagesB/2.jpg"), "html", null, true);
        echo "\" alt=\"\"></div>
\t\t\t\t\t\t\t\t<div class=\"notification_desc\">
\t\t\t\t\t\t\t\t\t<p>Lorem ipsum dolor amet </p>
\t\t\t\t\t\t\t\t\t<p><span>1 hour ago</span></p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t</a></li>
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<div class=\"notification_bottom\">
\t\t\t\t\t\t\t\t<a href=\"#\">See all notifications</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</li>
\t\t\t\t\t</ul>
\t\t\t\t</li>
\t\t\t\t<li class=\"dropdown head-dpdn\">
\t\t\t\t\t<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"fa fa-tasks\"></i><span class=\"badge blue1\">8</span></a>
\t\t\t\t\t<ul class=\"dropdown-menu\">
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<div class=\"notification_header\">
\t\t\t\t\t\t\t\t<h3>You have 8 pending task</h3>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li><a href=\"#\">
\t\t\t\t\t\t\t\t<div class=\"task-info\">
\t\t\t\t\t\t\t\t\t<span class=\"task-desc\">Database update</span><span class=\"percentage\">40%</span>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"progress progress-striped active\">
\t\t\t\t\t\t\t\t\t<div class=\"bar yellow\" style=\"width:40%;\"></div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</a></li>
\t\t\t\t\t\t<li><a href=\"#\">
\t\t\t\t\t\t\t\t<div class=\"task-info\">
\t\t\t\t\t\t\t\t\t<span class=\"task-desc\">Dashboard done</span><span class=\"percentage\">90%</span>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"progress progress-striped active\">
\t\t\t\t\t\t\t\t\t<div class=\"bar green\" style=\"width:90%;\"></div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</a></li>
\t\t\t\t\t\t<li><a href=\"#\">
\t\t\t\t\t\t\t\t<div class=\"task-info\">
\t\t\t\t\t\t\t\t\t<span class=\"task-desc\">Mobile App</span><span class=\"percentage\">33%</span>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"progress progress-striped active\">
\t\t\t\t\t\t\t\t\t<div class=\"bar red\" style=\"width: 33%;\"></div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</a></li>
\t\t\t\t\t\t<li><a href=\"#\">
\t\t\t\t\t\t\t\t<div class=\"task-info\">
\t\t\t\t\t\t\t\t\t<span class=\"task-desc\">Issues fixed</span><span class=\"percentage\">80%</span>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"progress progress-striped active\">
\t\t\t\t\t\t\t\t\t<div class=\"bar  blue\" style=\"width: 80%;\"></div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</a></li>
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<div class=\"notification_bottom\">
\t\t\t\t\t\t\t\t<a href=\"#\">See all pending tasks</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</li>
\t\t\t\t\t</ul>
\t\t\t\t</li>
\t\t\t</ul>
\t\t\t<div class=\"clearfix\"> </div>
\t\t</div>
\t\t<!--notification menu end -->
\t\t<div class=\"clearfix\"> </div>
\t</div>
\t<div class=\"header-right\">


\t\t<!--search-box-->
\t\t<div class=\"search-box\">
\t\t\t<form class=\"input\">
\t\t\t\t<input class=\"sb-search-input input__field--madoka\" placeholder=\"Search...\" type=\"search\" id=\"input-31\" />
\t\t\t\t<label class=\"input__label\" for=\"input-31\">
\t\t\t\t\t<svg class=\"graphic\" width=\"100%\" height=\"100%\" viewBox=\"0 0 404 77\" preserveAspectRatio=\"none\">
\t\t\t\t\t\t<path d=\"m0,0l404,0l0,77l-404,0l0,-77z\"/>
\t\t\t\t\t</svg>
\t\t\t\t</label>
\t\t\t</form>
\t\t</div><!--//end-search-box-->

\t\t<div class=\"profile_details\">
\t\t\t<ul>
\t\t\t\t<li class=\"dropdown profile_details_drop\">
\t\t\t\t\t<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">
\t\t\t\t\t\t<div class=\"profile_img\">
\t\t\t\t\t\t\t<span class=\"prfil-img\"><img src=\"";
        // line 180
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("imagesB/2.jpg"), "html", null, true);
        echo "\" alt=\"\"> </span>
\t\t\t\t\t\t\t<div class=\"user-name\">
\t\t\t\t\t\t\t\t<p>Docteur</p>
\t\t\t\t\t\t\t\t";
        // line 184
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<i class=\"fa fa-angle-down lnr\"></i>
\t\t\t\t\t\t\t<i class=\"fa fa-angle-up lnr\"></i>
\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</a>
\t\t\t\t\t<ul class=\"dropdown-menu drp-mnu\">
\t\t\t\t\t\t<li> <a href=\"#\"><i class=\"fa fa-cog\"></i> Settings</a> </li>
\t\t\t\t\t\t<li> <a href=\"#\"><i class=\"fa fa-user\"></i> My Account</a> </li>
\t\t\t\t\t\t<li> <a href=\"#\"><i class=\"fa fa-suitcase\"></i> Profile</a> </li>
\t\t\t\t\t\t<li> <a href=\"";
        // line 194
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_logout");
        echo "\" > <i class=\"fa fa-sign-out\"></i> Logout</a> </li>
\t\t\t\t\t</ul>
\t\t\t\t</li>
\t\t\t</ul>
\t\t</div>
\t\t<div class=\"clearfix\"> </div>
\t</div>
\t<div class=\"clearfix\"> </div>
</div>
<!-- //header-ends -->";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "back/_top_header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  259 => 194,  247 => 184,  241 => 180,  147 => 89,  136 => 81,  125 => 73,  114 => 65,  88 => 42,  77 => 34,  66 => 26,  55 => 18,  36 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<!-- header-starts -->
<div class=\"sticky-header header-section \">
\t<div class=\"header-left\">
\t\t<!--toggle button start-->
\t\t<button id=\"showLeftPush\"><i class=\"fa fa-bars\"></i></button>
\t\t<!--toggle button end-->
\t\t<div class=\"profile_details_left\"><!--notifications of menu start -->
\t\t\t<ul class=\"nofitications-dropdown\">
\t\t\t\t<li class=\"dropdown head-dpdn\">
\t\t\t\t\t<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"fa fa-envelope\"></i><span class=\"badge\">4</span></a>
\t\t\t\t\t<ul class=\"dropdown-menu\">
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<div class=\"notification_header\">
\t\t\t\t\t\t\t\t<h3>You have 3 new messages</h3>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li><a href=\"#\">
\t\t\t\t\t\t\t\t<div class=\"user_img\"><img src=\"{{ asset('imagesB/1.jpg') }}\" alt=\"\"></div>
\t\t\t\t\t\t\t\t<div class=\"notification_desc\">
\t\t\t\t\t\t\t\t\t<p>Lorem ipsum dolor amet</p>
\t\t\t\t\t\t\t\t\t<p><span>1 hour ago</span></p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t</a></li>
\t\t\t\t\t\t<li class=\"odd\"><a href=\"#\">
\t\t\t\t\t\t\t\t<div class=\"user_img\"><img src=\"{{ asset('imagesB/4.jpg') }}\" alt=\"\"></div>
\t\t\t\t\t\t\t\t<div class=\"notification_desc\">
\t\t\t\t\t\t\t\t\t<p>Lorem ipsum dolor amet </p>
\t\t\t\t\t\t\t\t\t<p><span>1 hour ago</span></p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t</a></li>
\t\t\t\t\t\t<li><a href=\"#\">
\t\t\t\t\t\t\t\t<div class=\"user_img\"><img src=\"{{ asset('imagesB/3.jpg') }}\" alt=\"\"></div>
\t\t\t\t\t\t\t\t<div class=\"notification_desc\">
\t\t\t\t\t\t\t\t\t<p>Lorem ipsum dolor amet </p>
\t\t\t\t\t\t\t\t\t<p><span>1 hour ago</span></p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t</a></li>
\t\t\t\t\t\t<li><a href=\"#\">
\t\t\t\t\t\t\t\t<div class=\"user_img\"><img src=\"{{ asset('imagesB/2.jpg') }}\" alt=\"\"></div>
\t\t\t\t\t\t\t\t<div class=\"notification_desc\">
\t\t\t\t\t\t\t\t\t<p>Lorem ipsum dolor amet </p>
\t\t\t\t\t\t\t\t\t<p><span>1 hour ago</span></p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t</a></li>
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<div class=\"notification_bottom\">
\t\t\t\t\t\t\t\t<a href=\"#\">See all messages</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</li>
\t\t\t\t\t</ul>
\t\t\t\t</li>
\t\t\t\t<li class=\"dropdown head-dpdn\">
\t\t\t\t\t<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"fa fa-bell\"></i><span class=\"badge blue\">4</span></a>
\t\t\t\t\t<ul class=\"dropdown-menu\">
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<div class=\"notification_header\">
\t\t\t\t\t\t\t\t<h3>You have 3 new notification</h3>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li><a href=\"#\">
\t\t\t\t\t\t\t\t<div class=\"user_img\"><img src=\"{{ asset('imagesB/4.jpg') }}\" alt=\"\"></div>
\t\t\t\t\t\t\t\t<div class=\"notification_desc\">
\t\t\t\t\t\t\t\t\t<p>Lorem ipsum dolor amet</p>
\t\t\t\t\t\t\t\t\t<p><span>1 hour ago</span></p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t</a></li>
\t\t\t\t\t\t<li class=\"odd\"><a href=\"#\">
\t\t\t\t\t\t\t\t<div class=\"user_img\"><img src=\"{{ asset('imagesB/1.jpg') }}\" alt=\"\"></div>
\t\t\t\t\t\t\t\t<div class=\"notification_desc\">
\t\t\t\t\t\t\t\t\t<p>Lorem ipsum dolor amet </p>
\t\t\t\t\t\t\t\t\t<p><span>1 hour ago</span></p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t</a></li>
\t\t\t\t\t\t<li><a href=\"#\">
\t\t\t\t\t\t\t\t<div class=\"user_img\"><img src=\"{{ asset('imagesB/3.jpg') }}\" alt=\"\"></div>
\t\t\t\t\t\t\t\t<div class=\"notification_desc\">
\t\t\t\t\t\t\t\t\t<p>Lorem ipsum dolor amet </p>
\t\t\t\t\t\t\t\t\t<p><span>1 hour ago</span></p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t</a></li>
\t\t\t\t\t\t<li><a href=\"#\">
\t\t\t\t\t\t\t\t<div class=\"user_img\"><img src=\"{{ asset('imagesB/2.jpg') }}\" alt=\"\"></div>
\t\t\t\t\t\t\t\t<div class=\"notification_desc\">
\t\t\t\t\t\t\t\t\t<p>Lorem ipsum dolor amet </p>
\t\t\t\t\t\t\t\t\t<p><span>1 hour ago</span></p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t</a></li>
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<div class=\"notification_bottom\">
\t\t\t\t\t\t\t\t<a href=\"#\">See all notifications</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</li>
\t\t\t\t\t</ul>
\t\t\t\t</li>
\t\t\t\t<li class=\"dropdown head-dpdn\">
\t\t\t\t\t<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"fa fa-tasks\"></i><span class=\"badge blue1\">8</span></a>
\t\t\t\t\t<ul class=\"dropdown-menu\">
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<div class=\"notification_header\">
\t\t\t\t\t\t\t\t<h3>You have 8 pending task</h3>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li><a href=\"#\">
\t\t\t\t\t\t\t\t<div class=\"task-info\">
\t\t\t\t\t\t\t\t\t<span class=\"task-desc\">Database update</span><span class=\"percentage\">40%</span>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"progress progress-striped active\">
\t\t\t\t\t\t\t\t\t<div class=\"bar yellow\" style=\"width:40%;\"></div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</a></li>
\t\t\t\t\t\t<li><a href=\"#\">
\t\t\t\t\t\t\t\t<div class=\"task-info\">
\t\t\t\t\t\t\t\t\t<span class=\"task-desc\">Dashboard done</span><span class=\"percentage\">90%</span>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"progress progress-striped active\">
\t\t\t\t\t\t\t\t\t<div class=\"bar green\" style=\"width:90%;\"></div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</a></li>
\t\t\t\t\t\t<li><a href=\"#\">
\t\t\t\t\t\t\t\t<div class=\"task-info\">
\t\t\t\t\t\t\t\t\t<span class=\"task-desc\">Mobile App</span><span class=\"percentage\">33%</span>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"progress progress-striped active\">
\t\t\t\t\t\t\t\t\t<div class=\"bar red\" style=\"width: 33%;\"></div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</a></li>
\t\t\t\t\t\t<li><a href=\"#\">
\t\t\t\t\t\t\t\t<div class=\"task-info\">
\t\t\t\t\t\t\t\t\t<span class=\"task-desc\">Issues fixed</span><span class=\"percentage\">80%</span>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"progress progress-striped active\">
\t\t\t\t\t\t\t\t\t<div class=\"bar  blue\" style=\"width: 80%;\"></div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</a></li>
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<div class=\"notification_bottom\">
\t\t\t\t\t\t\t\t<a href=\"#\">See all pending tasks</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</li>
\t\t\t\t\t</ul>
\t\t\t\t</li>
\t\t\t</ul>
\t\t\t<div class=\"clearfix\"> </div>
\t\t</div>
\t\t<!--notification menu end -->
\t\t<div class=\"clearfix\"> </div>
\t</div>
\t<div class=\"header-right\">


\t\t<!--search-box-->
\t\t<div class=\"search-box\">
\t\t\t<form class=\"input\">
\t\t\t\t<input class=\"sb-search-input input__field--madoka\" placeholder=\"Search...\" type=\"search\" id=\"input-31\" />
\t\t\t\t<label class=\"input__label\" for=\"input-31\">
\t\t\t\t\t<svg class=\"graphic\" width=\"100%\" height=\"100%\" viewBox=\"0 0 404 77\" preserveAspectRatio=\"none\">
\t\t\t\t\t\t<path d=\"m0,0l404,0l0,77l-404,0l0,-77z\"/>
\t\t\t\t\t</svg>
\t\t\t\t</label>
\t\t\t</form>
\t\t</div><!--//end-search-box-->

\t\t<div class=\"profile_details\">
\t\t\t<ul>
\t\t\t\t<li class=\"dropdown profile_details_drop\">
\t\t\t\t\t<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">
\t\t\t\t\t\t<div class=\"profile_img\">
\t\t\t\t\t\t\t<span class=\"prfil-img\"><img src=\"{{ asset('imagesB/2.jpg') }}\" alt=\"\"> </span>
\t\t\t\t\t\t\t<div class=\"user-name\">
\t\t\t\t\t\t\t\t<p>Docteur</p>
\t\t\t\t\t\t\t\t{#<span>Administrator</span>#}
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<i class=\"fa fa-angle-down lnr\"></i>
\t\t\t\t\t\t\t<i class=\"fa fa-angle-up lnr\"></i>
\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</a>
\t\t\t\t\t<ul class=\"dropdown-menu drp-mnu\">
\t\t\t\t\t\t<li> <a href=\"#\"><i class=\"fa fa-cog\"></i> Settings</a> </li>
\t\t\t\t\t\t<li> <a href=\"#\"><i class=\"fa fa-user\"></i> My Account</a> </li>
\t\t\t\t\t\t<li> <a href=\"#\"><i class=\"fa fa-suitcase\"></i> Profile</a> </li>
\t\t\t\t\t\t<li> <a href=\"{{ path('fos_user_security_logout') }}\" > <i class=\"fa fa-sign-out\"></i> Logout</a> </li>
\t\t\t\t\t</ul>
\t\t\t\t</li>
\t\t\t</ul>
\t\t</div>
\t\t<div class=\"clearfix\"> </div>
\t</div>
\t<div class=\"clearfix\"> </div>
</div>
<!-- //header-ends -->", "back/_top_header.html.twig", "/var/www/html/3A11/happy_olds/app/Resources/views/back/_top_header.html.twig");
    }
}
