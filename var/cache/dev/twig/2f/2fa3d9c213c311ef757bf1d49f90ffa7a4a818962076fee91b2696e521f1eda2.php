<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* :front:_notification.html.twig */
class __TwigTemplate_49e3203d7efe701c13fa32449e000b69b3dca08b0440d8f6727051d1a10cd745 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", ":front:_notification.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", ":front:_notification.html.twig"));

        // line 1
        echo "<ul id=\"notifications\" class=\"dropdown-menu notification-div\"
    style=\"width: 350px; padding: 5px 0px 0px 0px; max-height: 300px; height: 300px; overflow-y: auto;\">

    <div class=\"pull-left\" style=\"font-weight: bold; margin-top: 5px; margin-left: 10px;\">Notifications :</div>
    ";
        // line 5
        if ((twig_length_filter($this->env, ($context["notificationList"] ?? $this->getContext($context, "notificationList"))) > 0)) {
            // line 6
            echo "        <div class=\"pull-right\">
            <button onclick=\"mark_all_as_seen();\" class=\"btn customButton\" type=\"button\"
                    style=\"font-size: 10px; margin-right: 5px;\"><span
                        class=\"fa fa-check\"></span> Mark all as seen
            </button>
        </div>
    ";
        }
        // line 13
        echo "    <div class=\"clearfix\"></div>
    <hr style=\"margin: 4px !important;\">

    ";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["notificationList"] ?? $this->getContext($context, "notificationList")));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 17
            echo "        <li class=\"";
            if ( !$this->getAttribute($context["item"], "seen", [])) {
                echo "active";
            }
            echo "\">
            <div class=\"pull-right\">
                ";
            // line 19
            if ( !$this->getAttribute($context["item"], "seen", [])) {
                // line 20
                echo "                <button onclick=\"mark_as_seen";
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "id", []), "html", null, true);
                echo "()\" style=\"font-size: 10px;\" class=\"btn customButton\"
                        type=\"button\">
                    <span class=\"fa fa-check\"></span>
                </button>
                ";
            }
            // line 25
            echo "            </div>
            <div class=\"pull-left\" style=\"width: 85%;\"><span>";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "notification", []), "html", null, true);
            echo "</span></div>
            <br>

            <div class=\"clearfix\"></div>
            <script>
                function mark_as_seen";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "id", []), "html", null, true);
            echo "() {
                    \$.ajax({
                        url: \"";
            // line 33
            echo $this->env->getExtension('Mgilet\NotificationBundle\Twig\NotificationExtension')->generatePath("notification_mark_as_seen", $this->getAttribute($context["item"], "notifiableEntity", []), $this->getAttribute($context["item"], "notification", []));
            echo "\",
                        method: \"post\",
                        success: function (data) {
                            \$(\"#notifications\").load(location.href+\" #notifications>*\",\"\");
                            \$(\"#notification-number-wrapper\").load(location.href+\" #notification-number\");
                        }
                    });
                }
            </script>
        </li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 44
        echo "</ul>

";
        // line 46
        if ((twig_length_filter($this->env, ($context["notificationList"] ?? $this->getContext($context, "notificationList"))) > 0)) {
            // line 47
            echo "<script>
    function mark_all_as_seen() {
        \$.ajax({
            url: \"";
            // line 50
            echo $this->env->getExtension('Mgilet\NotificationBundle\Twig\NotificationExtension')->generatePath("notification_mark_all_as_seen", $this->getAttribute(twig_first($this->env, ($context["notificationList"] ?? $this->getContext($context, "notificationList"))), "notifiableEntity", []));
            echo "\",
            method: \"post\",
            success: function (data) {
                \$(\"#notifications\").load(location.href+\" #notifications>*\",\"\");
                \$(\"#notification-number-wrapper\").load(location.href+\" #notification-number\");
            }
        });
    }
</script>
";
        }
        // line 60
        echo "
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return ":front:_notification.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 60,  125 => 50,  120 => 47,  118 => 46,  114 => 44,  97 => 33,  92 => 31,  84 => 26,  81 => 25,  72 => 20,  70 => 19,  62 => 17,  58 => 16,  53 => 13,  44 => 6,  42 => 5,  36 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<ul id=\"notifications\" class=\"dropdown-menu notification-div\"
    style=\"width: 350px; padding: 5px 0px 0px 0px; max-height: 300px; height: 300px; overflow-y: auto;\">

    <div class=\"pull-left\" style=\"font-weight: bold; margin-top: 5px; margin-left: 10px;\">Notifications :</div>
    {% if notificationList|length > 0 %}
        <div class=\"pull-right\">
            <button onclick=\"mark_all_as_seen();\" class=\"btn customButton\" type=\"button\"
                    style=\"font-size: 10px; margin-right: 5px;\"><span
                        class=\"fa fa-check\"></span> Mark all as seen
            </button>
        </div>
    {% endif %}
    <div class=\"clearfix\"></div>
    <hr style=\"margin: 4px !important;\">

    {% for item in notificationList %}
        <li class=\"{% if not item.seen %}active{% endif %}\">
            <div class=\"pull-right\">
                {% if not item.seen %}
                <button onclick=\"mark_as_seen{{ item.id }}()\" style=\"font-size: 10px;\" class=\"btn customButton\"
                        type=\"button\">
                    <span class=\"fa fa-check\"></span>
                </button>
                {% endif %}
            </div>
            <div class=\"pull-left\" style=\"width: 85%;\"><span>{{ item.notification }}</span></div>
            <br>

            <div class=\"clearfix\"></div>
            <script>
                function mark_as_seen{{ item.id }}() {
                    \$.ajax({
                        url: \"{{ mgilet_notification_generate_path('notification_mark_as_seen', item.notifiableEntity, item.notification) }}\",
                        method: \"post\",
                        success: function (data) {
                            \$(\"#notifications\").load(location.href+\" #notifications>*\",\"\");
                            \$(\"#notification-number-wrapper\").load(location.href+\" #notification-number\");
                        }
                    });
                }
            </script>
        </li>
    {% endfor %}
</ul>

{% if notificationList|length > 0 %}
<script>
    function mark_all_as_seen() {
        \$.ajax({
            url: \"{{ mgilet_notification_generate_path('notification_mark_all_as_seen', notificationList|first.notifiableEntity) }}\",
            method: \"post\",
            success: function (data) {
                \$(\"#notifications\").load(location.href+\" #notifications>*\",\"\");
                \$(\"#notification-number-wrapper\").load(location.href+\" #notification-number\");
            }
        });
    }
</script>
{% endif %}

", ":front:_notification.html.twig", "/var/www/html/3A11/happy_olds/app/Resources/views/front/_notification.html.twig");
    }
}
