<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = [];
        $pathinfo = rawurldecode($rawPathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request ?: $this->createRequest($pathinfo);
        $requestMethod = $canonicalMethod = $context->getMethod();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => '_wdt']), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if ('/_profiler' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif ('GET' !== $canonicalMethod) {
                        goto not__profiler_home;
                    } else {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', '_profiler_home'));
                    }

                    return $ret;
                }
                not__profiler_home:

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ('/_profiler/search' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ('/_profiler/search_bar' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_phpinfo
                if ('/_profiler/phpinfo' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler_search_results']), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler_open_file
                if ('/_profiler/open' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:openAction',  '_route' => '_profiler_open_file',);
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler']), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler_router']), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler_exception']), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler_exception_css']), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => '_twig_error_test']), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        elseif (0 === strpos($pathinfo, '/a')) {
            if (0 === strpos($pathinfo, '/afficher')) {
                // afficher_agee
                if ('/afficher_agee' === $pathinfo) {
                    return array (  '_controller' => 'GestionAgeeBundle\\Controller\\AgeeController::AfficherAction',  '_route' => 'afficher_agee',);
                }

                if (0 === strpos($pathinfo, '/afficherTraitement')) {
                    // afficherTraitement
                    if ('/afficherTraitement' === $pathinfo) {
                        return array (  '_controller' => 'GestionAgeeBundle\\Controller\\Planning_traitement_medicalController::afficherTraitementAction',  '_route' => 'afficherTraitement',);
                    }

                    // afficherTraitementAgee
                    if ('/afficherTraitementAgee' === $pathinfo) {
                        return array (  '_controller' => 'GestionAgeeBundle\\Controller\\Planning_traitement_medicalController::afficherTraitementAgeection',  '_route' => 'afficherTraitementAgee',);
                    }

                }

                // afficher
                if ('/afficher' === $pathinfo) {
                    return array (  '_controller' => 'GestionAgeeBundle\\Controller\\Planning_visite_medicaleController::AfficherAction',  '_route' => 'afficher',);
                }

                // afficherVisiteAgee
                if ('/afficherVisiteAgee' === $pathinfo) {
                    return array (  '_controller' => 'GestionAgeeBundle\\Controller\\Planning_visite_medicaleController::AfficherAgeeAction',  '_route' => 'afficherVisiteAgee',);
                }

            }

            elseif (0 === strpos($pathinfo, '/ajouter')) {
                // ajouterTraitement
                if ('/ajouterTraitement' === $pathinfo) {
                    return array (  '_controller' => 'GestionAgeeBundle\\Controller\\Planning_traitement_medicalController::ajouterTraitementAction',  '_route' => 'ajouterTraitement',);
                }

                // ajouter
                if ('/ajouter' === $pathinfo) {
                    return array (  '_controller' => 'GestionAgeeBundle\\Controller\\Planning_visite_medicaleController::AjouterAction',  '_route' => 'ajouter',);
                }

            }

            // about
            if ('/about' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::aboutAction',  '_route' => 'about',);
            }

            if (0 === strpos($pathinfo, '/api/tasks')) {
                // all
                if ('/api/tasks/all' === $pathinfo) {
                    return array (  '_controller' => 'EspritApiBundle\\Controller\\TaskController::allAction',  '_route' => 'all',);
                }

                // find
                if (0 === strpos($pathinfo, '/api/tasks/find') && preg_match('#^/api/tasks/find/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'find']), array (  '_controller' => 'EspritApiBundle\\Controller\\TaskController::findAction',));
                }

                // new
                if ('/api/tasks/new' === $pathinfo) {
                    return array (  '_controller' => 'EspritApiBundle\\Controller\\TaskController::newAction',  '_route' => 'new',);
                }

            }

        }

        elseif (0 === strpos($pathinfo, '/mobile')) {
            if (0 === strpos($pathinfo, '/mobile/MapLocation')) {
                // mobileMapLocationAdd
                if ('/mobile/MapLocation/add' === $pathinfo) {
                    return array (  '_controller' => 'GestionAgeeBundle\\Controller\\MapLocationController::newAction',  '_route' => 'mobileMapLocationAdd',);
                }

                // mobileMapLocationAll
                if ('/mobile/MapLocation/all' === $pathinfo) {
                    return array (  '_controller' => 'GestionAgeeBundle\\Controller\\MapLocationController::allAction',  '_route' => 'mobileMapLocationAll',);
                }

                // mobileMapLocationDelete
                if ('/mobile/MapLocation/delete' === $pathinfo) {
                    return array (  '_controller' => 'GestionAgeeBundle\\Controller\\MapLocationController::deleteLocationAction',  '_route' => 'mobileMapLocationDelete',);
                }

                // mobileMapLocationFind
                if (0 === strpos($pathinfo, '/mobile/MapLocation/find') && preg_match('#^/mobile/MapLocation/find/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'mobileMapLocationFind']), array (  '_controller' => 'GestionAgeeBundle\\Controller\\MapLocationController::findLocationAction',));
                }

            }

            elseif (0 === strpos($pathinfo, '/mobile/traitement')) {
                // mobileTraitementAfficher
                if ('/mobile/traitement/afficher' === $pathinfo) {
                    return array (  '_controller' => 'GestionAgeeBundle\\Controller\\MobileTraitementMedicalController::allAction',  '_route' => 'mobileTraitementAfficher',);
                }

                // mobileTraitementAjouter
                if ('/mobile/traitement/ajouter' === $pathinfo) {
                    return array (  '_controller' => 'GestionAgeeBundle\\Controller\\MobileTraitementMedicalController::newAction',  '_route' => 'mobileTraitementAjouter',);
                }

                // mobileTraitementRecherche
                if (0 === strpos($pathinfo, '/mobile/traitement/recherche/idAgee') && preg_match('#^/mobile/traitement/recherche/idAgee/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'mobileTraitementRecherche']), array (  '_controller' => 'GestionAgeeBundle\\Controller\\MobileTraitementMedicalController::findAgeeTraitementAction',));
                }

                // mobileTraitementModif
                if ('/mobile/traitement/update' === $pathinfo) {
                    return array (  '_controller' => 'GestionAgeeBundle\\Controller\\MobileTraitementMedicalController::modifierTraitementAction',  '_route' => 'mobileTraitementModif',);
                }

            }

            // mobileAgeeAfficher
            if ('/mobile/Agee/afficher' === $pathinfo) {
                return array (  '_controller' => 'GestionAgeeBundle\\Controller\\MobileTraitementMedicalController::AfficherAgee',  '_route' => 'mobileAgeeAfficher',);
            }

            // mobileSupprimerTraitement
            if ('/mobile/supprimerTraitement' === $pathinfo) {
                return array (  '_controller' => 'GestionAgeeBundle\\Controller\\MobileTraitementMedicalController::supprimerTraitementAction',  '_route' => 'mobileSupprimerTraitement',);
            }

        }

        elseif (0 === strpos($pathinfo, '/modifier')) {
            // modifierTraitement
            if (0 === strpos($pathinfo, '/modifierTraitement') && preg_match('#^/modifierTraitement/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'modifierTraitement']), array (  '_controller' => 'GestionAgeeBundle\\Controller\\Planning_traitement_medicalController::modifierTraitementAction',));
            }

            // modifier
            if (preg_match('#^/modifier/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'modifier']), array (  '_controller' => 'GestionAgeeBundle\\Controller\\Planning_visite_medicaleController::ModifierAction',));
            }

        }

        elseif (0 === strpos($pathinfo, '/supprimer')) {
            // supprimerTraitement
            if (0 === strpos($pathinfo, '/supprimerTraitement') && preg_match('#^/supprimerTraitement/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'supprimerTraitement']), array (  '_controller' => 'GestionAgeeBundle\\Controller\\Planning_traitement_medicalController::supprimerTraitementAction',));
            }

            // supprimer
            if (preg_match('#^/supprimer/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'supprimer']), array (  '_controller' => 'GestionAgeeBundle\\Controller\\Planning_visite_medicaleController::SupprimerAction',));
            }

        }

        // send_notification
        if ('/send-notification' === $pathinfo) {
            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::sendNotification',  '_route' => 'send_notification',);
        }

        if (0 === strpos($pathinfo, '/pdf')) {
            // pdf3
            if (0 === strpos($pathinfo, '/pdf3') && preg_match('#^/pdf3/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'pdf3']), array (  '_controller' => 'GestionAgeeBundle\\Controller\\Planning_visite_medicaleController::pdf3Action',));
            }

            // pdf
            if ('/pdf' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::pdfAction',  '_route' => 'pdf',);
            }

            // pdf2
            if ('/pdf2' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::pdf2Action',  '_route' => 'pdf2',);
            }

        }

        elseif (0 === strpos($pathinfo, '/profile')) {
            // fos_user_profile_show
            if ('/profile' === $trimmedPathinfo) {
                $ret = array (  '_controller' => 'fos_user.profile.controller:showAction',  '_route' => 'fos_user_profile_show',);
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif ('GET' !== $canonicalMethod) {
                    goto not_fos_user_profile_show;
                } else {
                    return array_replace($ret, $this->redirect($rawPathinfo.'/', 'fos_user_profile_show'));
                }

                if (!in_array($canonicalMethod, ['GET'])) {
                    $allow = array_merge($allow, ['GET']);
                    goto not_fos_user_profile_show;
                }

                return $ret;
            }
            not_fos_user_profile_show:

            // fos_user_profile_edit
            if ('/profile/edit' === $pathinfo) {
                $ret = array (  '_controller' => 'fos_user.profile.controller:editAction',  '_route' => 'fos_user_profile_edit',);
                if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                    $allow = array_merge($allow, ['GET', 'POST']);
                    goto not_fos_user_profile_edit;
                }

                return $ret;
            }
            not_fos_user_profile_edit:

            // fos_user_change_password
            if ('/profile/change-password' === $pathinfo) {
                $ret = array (  '_controller' => 'fos_user.change_password.controller:changePasswordAction',  '_route' => 'fos_user_change_password',);
                if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                    $allow = array_merge($allow, ['GET', 'POST']);
                    goto not_fos_user_change_password;
                }

                return $ret;
            }
            not_fos_user_change_password:

        }

        // homepage
        if ('' === $trimmedPathinfo) {
            $ret = array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif ('GET' !== $canonicalMethod) {
                goto not_homepage;
            } else {
                return array_replace($ret, $this->redirect($rawPathinfo.'/', 'homepage'));
            }

            return $ret;
        }
        not_homepage:

        // back
        if ('/back' === $pathinfo) {
            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::backAction',  '_route' => 'back',);
        }

        if (0 === strpos($pathinfo, '/login')) {
            // fos_user_security_login
            if ('/login' === $pathinfo) {
                $ret = array (  '_controller' => 'fos_user.security.controller:loginAction',  '_route' => 'fos_user_security_login',);
                if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                    $allow = array_merge($allow, ['GET', 'POST']);
                    goto not_fos_user_security_login;
                }

                return $ret;
            }
            not_fos_user_security_login:

            // fos_user_security_check
            if ('/login_check' === $pathinfo) {
                $ret = array (  '_controller' => 'fos_user.security.controller:checkAction',  '_route' => 'fos_user_security_check',);
                if (!in_array($requestMethod, ['POST'])) {
                    $allow = array_merge($allow, ['POST']);
                    goto not_fos_user_security_check;
                }

                return $ret;
            }
            not_fos_user_security_check:

        }

        // fos_user_security_logout
        if ('/logout' === $pathinfo) {
            $ret = array (  '_controller' => 'fos_user.security.controller:logoutAction',  '_route' => 'fos_user_security_logout',);
            if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                $allow = array_merge($allow, ['GET', 'POST']);
                goto not_fos_user_security_logout;
            }

            return $ret;
        }
        not_fos_user_security_logout:

        if (0 === strpos($pathinfo, '/register')) {
            // fos_user_registration_register
            if ('/register' === $trimmedPathinfo) {
                $ret = array (  '_controller' => 'fos_user.registration.controller:registerAction',  '_route' => 'fos_user_registration_register',);
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif ('GET' !== $canonicalMethod) {
                    goto not_fos_user_registration_register;
                } else {
                    return array_replace($ret, $this->redirect($rawPathinfo.'/', 'fos_user_registration_register'));
                }

                if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                    $allow = array_merge($allow, ['GET', 'POST']);
                    goto not_fos_user_registration_register;
                }

                return $ret;
            }
            not_fos_user_registration_register:

            // fos_user_registration_check_email
            if ('/register/check-email' === $pathinfo) {
                $ret = array (  '_controller' => 'fos_user.registration.controller:checkEmailAction',  '_route' => 'fos_user_registration_check_email',);
                if (!in_array($canonicalMethod, ['GET'])) {
                    $allow = array_merge($allow, ['GET']);
                    goto not_fos_user_registration_check_email;
                }

                return $ret;
            }
            not_fos_user_registration_check_email:

            if (0 === strpos($pathinfo, '/register/confirm')) {
                // fos_user_registration_confirm
                if (preg_match('#^/register/confirm/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'fos_user_registration_confirm']), array (  '_controller' => 'fos_user.registration.controller:confirmAction',));
                    if (!in_array($canonicalMethod, ['GET'])) {
                        $allow = array_merge($allow, ['GET']);
                        goto not_fos_user_registration_confirm;
                    }

                    return $ret;
                }
                not_fos_user_registration_confirm:

                // fos_user_registration_confirmed
                if ('/register/confirmed' === $pathinfo) {
                    $ret = array (  '_controller' => 'fos_user.registration.controller:confirmedAction',  '_route' => 'fos_user_registration_confirmed',);
                    if (!in_array($canonicalMethod, ['GET'])) {
                        $allow = array_merge($allow, ['GET']);
                        goto not_fos_user_registration_confirmed;
                    }

                    return $ret;
                }
                not_fos_user_registration_confirmed:

            }

        }

        elseif (0 === strpos($pathinfo, '/resetting')) {
            // fos_user_resetting_request
            if ('/resetting/request' === $pathinfo) {
                $ret = array (  '_controller' => 'fos_user.resetting.controller:requestAction',  '_route' => 'fos_user_resetting_request',);
                if (!in_array($canonicalMethod, ['GET'])) {
                    $allow = array_merge($allow, ['GET']);
                    goto not_fos_user_resetting_request;
                }

                return $ret;
            }
            not_fos_user_resetting_request:

            // fos_user_resetting_reset
            if (0 === strpos($pathinfo, '/resetting/reset') && preg_match('#^/resetting/reset/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'fos_user_resetting_reset']), array (  '_controller' => 'fos_user.resetting.controller:resetAction',));
                if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                    $allow = array_merge($allow, ['GET', 'POST']);
                    goto not_fos_user_resetting_reset;
                }

                return $ret;
            }
            not_fos_user_resetting_reset:

            // fos_user_resetting_send_email
            if ('/resetting/send-email' === $pathinfo) {
                $ret = array (  '_controller' => 'fos_user.resetting.controller:sendEmailAction',  '_route' => 'fos_user_resetting_send_email',);
                if (!in_array($requestMethod, ['POST'])) {
                    $allow = array_merge($allow, ['POST']);
                    goto not_fos_user_resetting_send_email;
                }

                return $ret;
            }
            not_fos_user_resetting_send_email:

            // fos_user_resetting_check_email
            if ('/resetting/check-email' === $pathinfo) {
                $ret = array (  '_controller' => 'fos_user.resetting.controller:checkEmailAction',  '_route' => 'fos_user_resetting_check_email',);
                if (!in_array($canonicalMethod, ['GET'])) {
                    $allow = array_merge($allow, ['GET']);
                    goto not_fos_user_resetting_check_email;
                }

                return $ret;
            }
            not_fos_user_resetting_check_email:

        }

        // ancarebeca_full_calendar_load
        if ('/full-calendar/load' === $pathinfo) {
            return array (  '_controller' => 'AncaRebeca\\FullCalendarBundle\\Controller\\CalendarController::loadAction',  '_route' => 'ancarebeca_full_calendar_load',);
        }

        if (0 === strpos($pathinfo, '/notifications')) {
            // notification_list
            if (preg_match('#^/notifications/(?P<notifiable>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'notification_list']), array (  '_controller' => 'Mgilet\\NotificationBundle\\Controller\\NotificationController::listAction',));
            }

            // notification_mark_as_seen
            if (preg_match('#^/notifications/(?P<notifiable>[^/]++)/mark_as_seen/(?P<notification>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'notification_mark_as_seen']), array (  '_controller' => 'Mgilet\\NotificationBundle\\Controller\\NotificationController::markAsSeenAction',));
            }

            // notification_mark_as_unseen
            if (preg_match('#^/notifications/(?P<notifiable>[^/]++)/mark_as_unseen/(?P<notification>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'notification_mark_as_unseen']), array (  '_controller' => 'Mgilet\\NotificationBundle\\Controller\\NotificationController::markAsUnSeenAction',));
            }

            // notification_mark_all_as_seen
            if (preg_match('#^/notifications/(?P<notifiable>[^/]++)/markAllAsSeen$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'notification_mark_all_as_seen']), array (  '_controller' => 'Mgilet\\NotificationBundle\\Controller\\NotificationController::markAllAsSeenAction',));
            }

        }

        if ('/' === $pathinfo && !$allow) {
            throw new Symfony\Component\Routing\Exception\NoConfigurationException();
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
