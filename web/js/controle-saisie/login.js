function verif_username() {
    var test = true;
    var username = $('#username').val().trim();
    if (username == '') {
        $('#block-erreur-username').html('Le username ne doit pas être vide');
        $('#block-erreur-username').removeClass('hide');
        $('#username').css({
            'border': '1px solid red'
        });
        $('#username').focus();
        test = false;
    }
    else {
        $('#block-erreur-username').addClass('hide');
        $('#username').css({
            'border': 'none'
        });
    }
    return test;
}

function verif_plainPassword_first() {
    var test = true;
    var password = $('#password').val();
    if (password == '') {
        $('#block-erreur-password').html('Le mot de passe ne doit pas être vide');
        $('#block-erreur-password').removeClass('hide');
        $('#password').css({
            'border': '1px solid red'
        });
        $('#password').focus();
        test = false;
    }
    else {
        $('#block-erreur-password').addClass('hide');
        $('#password').css({
            'border': 'none'
        });
    }
    return test;
}


function verif_form() {
    var test = true;

    verif_username();
    verif_plainPassword_first();
    test = test
        && verif_username()
        && verif_plainPassword_first()
    return test;
}


$('#username').on('keyup', function () {
    verif_username();
});

$('#password').on('keyup', function () {
    verif_plainPassword_first();
});



$('.submitLogin').on('click', function (e) {
    e.preventDefault();
    if(verif_form()){
        $('#loginForm').submit();
    }
});



















