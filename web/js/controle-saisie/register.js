function verif_nom() {
    var test = true;
    var nom = $('#fos_user_registration_form_nom').val().trim();
    if (nom == '') {
        $('#block-erreur-nom').html('Le nom ne doit pas être vide');
        $('#block-erreur-nom').removeClass('hide');
        $('#fos_user_registration_form_nom').css({
            'border': '1px solid red'
        });
        $('#fos_user_registration_form_nom').focus();
        test = false;
    } else if (nom.length < 3) {
        $('#block-erreur-nom').html('Le nom doit être supérieur à 3 caractères');
        $('#block-erreur-nom').removeClass('hide');
        $('#fos_user_registration_form_nom').css({
            'border': '1px solid red'
        });
        $('#fos_user_registration_form_nom').focus();
        test = false;
    } else {
        $('#block-erreur-nom').addClass('hide');
        $('#fos_user_registration_form_nom').css({
            'border': 'none'
        });
    }
    return test;
}

function verif_prenom() {
    var test = true;
    var prenom = $('#fos_user_registration_form_prenom').val().trim();
    if (prenom == '') {
        $('#block-erreur-prenom').html('Le prénom ne doit pas être vide');
        $('#block-erreur-prenom').removeClass('hide');
        $('#fos_user_registration_form_prenom').css({
            'border': '1px solid red'
        });
        $('#fos_user_registration_form_prenom').focus();
        test = false;
    } else if (prenom.length < 3) {
        $('#block-erreur-prenom').html('Le prénom doit être supérieur à 3 caractères');
        $('#block-erreur-prenom').removeClass('hide');
        $('#fos_user_registration_form_prenom').css({
            'border': '1px solid red'
        });
        $('#fos_user_registration_form_prenom').focus();
        test = false;
    } else {
        $('#block-erreur-prenom').addClass('hide');
        $('#fos_user_registration_form_prenom').css({
            'border': 'none'
        });
    }
    return test;
}

function verif_email() {
    var test = true;
    var email = $('#fos_user_registration_form_email').val().trim();
    var reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (email == '') {
        $('#block-erreur-email').html('L\'émail ne doit pas être vide');
        $('#block-erreur-email').removeClass('hide');
        $('#fos_user_registration_form_email').css({
            'border': '1px solid red'
        });
        $('#fos_user_registration_form_email').focus();
        test = false;
    } else if (!reg.test(email)) {
        $('#block-erreur-email').html('Email non valide');
        $('#block-erreur-email').removeClass('hide');
        $('#fos_user_registration_form_email').css({
            'border': '1px solid red'
        });
        $('#fos_user_registration_form_email').focus();
        test = false;
    } else {
        $('#block-erreur-email').addClass('hide');
        $('#fos_user_registration_form_email').css({
            'border': 'none'
        });
    }
    return test;
}

function verif_username() {
    var test = true;
    var username = $('#fos_user_registration_form_username').val().trim();
    if (username == '') {
        $('#block-erreur-username').html('Le username ne doit pas être vide');
        $('#block-erreur-username').removeClass('hide');
        $('#fos_user_registration_form_username').css({
            'border': '1px solid red'
        });
        $('#fos_user_registration_form_username').focus();
        test = false;
    } else if (username.length < 3) {
        $('#block-erreur-username').html('Le username doit être supérieur à 3 caractères');
        $('#block-erreur-username').removeClass('hide');
        $('#fos_user_registration_form_username').css({
            'border': '1px solid red'
        });
        $('#fos_user_registration_form_username').focus();
        test = false;
    } else {
        $('#block-erreur-username').addClass('hide');
        $('#fos_user_registration_form_username').css({
            'border': 'none'
        });
    }
    return test;
}

function verif_cin() {
    var test = true;
    var cin = $('#fos_user_registration_form_cin').val().trim();
    var reg = /^\d+$/;

    if (cin == '') {
        $('#block-erreur-cin').html('Le CIN ne doit pas être vide');
        $('#block-erreur-cin').removeClass('hide');
        $('#fos_user_registration_form_cin').css({
            'border': '1px solid red'
        });
        $('#fos_user_registration_form_cin').focus();
        test = false;
    } else if (cin.length != 8) {
        $('#block-erreur-cin').html('CIN non valide');
        $('#block-erreur-cin').removeClass('hide');
        $('#fos_user_registration_form_cin').css({
            'border': '1px solid red'
        });
        $('#fos_user_registration_form_cin').focus();
        test = false;
    } else if (!reg.test(cin)) {
        $('#block-erreur-cin').html('CIN non valide');
        $('#block-erreur-cin').removeClass('hide');
        $('#fos_user_registration_form_cin').css({
            'border': '1px solid red'
        });
        $('#fos_user_registration_form_cin').focus();
        test = false;
    } else {
        $('#block-erreur-cin').addClass('hide');
        $('#fos_user_registration_form_cin').css({
            'border': 'none'
        });
    }
    return test;
}

function verif_Region() {
    var test = true;
    var nom = $('#fos_user_registration_form_region').val().trim();
    if (nom == '') {
        $('#block-erreur-region').html('La région ne doit pas être vide');
        $('#block-erreur-region').removeClass('hide');
        $('#fos_user_registration_form_region').css({
            'border': '1px solid red'
        });
        $('#fos_user_registration_form_region').focus();
        test = false;
    } else if (nom.length < 3) {
        $('#block-erreur-region').html('La région doit être supérieur à 3 caractères');
        $('#block-erreur-region').removeClass('hide');
        $('#fos_user_registration_form_region').css({
            'border': '1px solid red'
        });
        $('#fos_user_registration_form_region').focus();
        test = false;
    } else {
        $('#block-erreur-region').addClass('hide');
        $('#fos_user_registration_form_region').css({
            'border': 'none'
        });
    }
    return test;
}

function verif_rib() {
    var test = true;
    var reg = /^\d+$/;
    var nom = $('#fos_user_registration_form_rib').val().trim();
    if (nom == '') {
        $('#block-erreur-rib').html('Le Rib ne doit pas être vide');
        $('#block-erreur-rib').removeClass('hide');
        $('#fos_user_registration_form_rib').css({
            'border': '1px solid red'
        });
        $('#fos_user_registration_form_rib').focus();
        test = false;
    }
    else if (!reg.test(nom)) {
        $('#block-erreur-rib').html('RIB non valide');
        $('#block-erreur-rib').removeClass('hide');
        $('#fos_user_registration_form_rib').css({
            'border': '1px solid red'
        });
        $('#fos_user_registration_form_rib').focus();
        test = false;
    }
    else if (nom.length != 8) {
        $('#block-erreur-rib').html('Rib non valide il doit avoir 8 chiffres');
        $('#block-erreur-rib').removeClass('hide');
        $('#fos_user_registration_form_rib').css({
            'border': '1px solid red'
        });
        $('#fos_user_registration_form_rib').focus();
        test = false;
    }
    else {
        $('#block-erreur-rib').addClass('hide');
        $('#fos_user_registration_form_rib').css({
            'border': 'none'
        });
    }
    return test;
}


function verif_adresse() {
    var test = true;
    var nom = $('#fos_user_registration_form_adresse').val().trim();
    if (nom == '') {
        $('#block-erreur-adresse').html('L\'adresse ne doit pas être vide');
        $('#block-erreur-adresse').removeClass('hide');
        $('#fos_user_registration_form_adresse').css({
            'border': '1px solid red'
        });
        $('#fos_user_registration_form_adresse').focus();
        test = false;
    } else if (nom.length < 3) {
        $('#block-erreur-adresse').html('L\'adresse doit être supérieur à 3 caractères');
        $('#block-erreur-adresse').removeClass('hide');
        $('#fos_user_registration_form_adresse').css({
            'border': '1px solid red'
        });
        $('#fos_user_registration_form_adresse').focus();
        test = false;
    } else {
        $('#block-erreur-adresse').addClass('hide');
        $('#fos_user_registration_form_adresse').css({
            'border': 'none'
        });
    }
    return test;
}

function verif_telephone() {
    var test = true;
    var telephone = $('#fos_user_registration_form_telephone').val().trim();
    var reg = /^\d+$/;

    if (telephone == '') {
        $('#block-erreur-telephone').html('Le numéro du téléphone ne doit pas être vide');
        $('#block-erreur-telephone').removeClass('hide');
        $('#fos_user_registration_form_telephone').css({
            'border': '1px solid red'
        });
        $('#fos_user_registration_form_telephone').focus();
        test = false;
    } else if (telephone.length != 8) {
        $('#block-erreur-telephone').html('telephone non valide');
        $('#block-erreur-telephone').removeClass('hide');
        $('#fos_user_registration_form_telephone').css({
            'border': '1px solid red'
        });
        $('#fos_user_registration_form_telephone').focus();
        test = false;
    } else if (!reg.test(telephone)) {
        $('#block-erreur-telephone').html('telephone non valide');
        $('#block-erreur-telephone').removeClass('hide');
        $('#fos_user_registration_form_telephone').css({
            'border': '1px solid red'
        });
        $('#fos_user_registration_form_telephone').focus();
        test = false;
    } else {
        $('#block-erreur-telephone').addClass('hide');
        $('#fos_user_registration_form_telephone').css({
            'border': 'none'
        });
    }
    return test;
}

function verif_plainPassword_first() {
    var test = true;
    var password = $('#fos_user_registration_form_plainPassword_first').val();
    if (password == '') {
        $('#block-erreur-password').html('Le mot de passe ne doit pas être vide');
        $('#block-erreur-password').removeClass('hide');
        $('#fos_user_registration_form_plainPassword_first').css({
            'border': '1px solid red'
        });
        $('#fos_user_registration_form_plainPassword_first').focus();
        test = false;
    } else if (password.length < 8) {
        $('#block-erreur-password').html('Le mot de passe doit être supérieur à 8 caractères');
        $('#block-erreur-password').removeClass('hide');
        $('#fos_user_registration_form_plainPassword_first').css({
            'border': '1px solid red'
        });
        $('#fos_user_registration_form_plainPassword_first').focus();
        test = false;
    }
    else {
        $('#block-erreur-password').addClass('hide');
        $('#fos_user_registration_form_plainPassword_first').css({
            'border': 'none'
        });
    }
    return test;
}

function verif_plainPassword_second() {
    var test = true;
    var password = $('#fos_user_registration_form_plainPassword_second').val();
    if (password == '') {
        $('#block-erreur-password').html('Le mot de passe ne doit pas être vide');
        $('#block-erreur-password').removeClass('hide');
        $('#fos_user_registration_form_plainPassword_second').css({
            'border': '1px solid red'
        });
        $('#fos_user_registration_form_plainPassword_second').focus();
        test = false;
    } else if (password.length < 8) {
        $('#block-erreur-password').html('Le mot de passe doit être supérieur à 8 caractères');
        $('#block-erreur-password').removeClass('hide');
        $('#fos_user_registration_form_plainPassword_second').css({
            'border': '1px solid red'
        });
        $('#fos_user_registration_form_plainPassword_second').focus();
        test = false;
    }
    else if (password != $('#fos_user_registration_form_plainPassword_first').val()){
        $('#block-erreur-password').html('Les mots de passe doivent être identiques');
        $('#block-erreur-password').removeClass('hide');
        $('#fos_user_registration_form_plainPassword_second').css({
            'border': '1px solid red'
        });
        $('#fos_user_registration_form_plainPassword_second').focus();
        test = false;
    }
    else {
        $('#block-erreur-password').addClass('hide');
        $('#fos_user_registration_form_plainPassword_second').css({
            'border': 'none'
        });
    }
    return test;
}







function verif_form() {
    var test = true;
    verif_nom();
    verif_prenom();
    verif_email();
    verif_username();
    verif_cin();
    verif_Region();
    verif_adresse();
    verif_rib();
    verif_telephone();
    verif_plainPassword_first();
    verif_plainPassword_second();
    test = test
        && verif_nom()
        && verif_prenom()
        && verif_email()
        && verif_username()
        && verif_cin()
        && verif_Region()
        && verif_adresse()
        && verif_rib()
        && verif_telephone()
        && verif_plainPassword_first()
        && verif_plainPassword_second();
    return test;
}

$('#fos_user_registration_form_nom').on('keyup', function () {
    verif_nom();
});

$('#fos_user_registration_form_prenom').on('keyup', function () {
    verif_prenom();
});

$('#fos_user_registration_form_email').on('keyup', function () {
    verif_email();
});

$('#fos_user_registration_form_username').on('keyup', function () {
    verif_username();
});

$('#fos_user_registration_form_cin').on('keyup', function () {
    verif_cin();
});

$('#fos_user_registration_form_region').on('keyup', function () {
    verif_Region();
});

$('#fos_user_registration_form_adresse').on('keyup', function () {
    verif_adresse();
});

$('#fos_user_registration_form_rib').on('keyup', function () {
    verif_rib();
});
$('#fos_user_registration_form_telephone').on('keyup', function () {
    verif_telephone();
});
$('#fos_user_registration_form_plainPassword_first').on('keyup', function () {
    verif_plainPassword_first();
});
$('#fos_user_registration_form_plainPassword_second').on('keyup', function () {
    verif_plainPassword_second();
});


$('#submitRegister').on('click', function (e) {
   e.preventDefault();
   if(verif_form()){
       $('#registerForm').submit();
   }
});